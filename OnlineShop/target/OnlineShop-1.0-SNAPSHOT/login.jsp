<%-- 
    Document   : login
    Created on : Jul 20, 2015, 6:14:02 PM
    Author     : notepad
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <fmt:setLocale value="${sessionScope.locale}" />
        <fmt:setBundle basename="locale" var="locale"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="main.page.title" bundle="${locale}" /></title>
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
        <link href="css/bootstrap-theme.min.css" type="text/css" rel="stylesheet">
        <link href="css/signin.css" rel="stylesheet" type="text/css">
        <link href="css/styles.css" rel="stylesheet" type="text/css">
        <style>input{margin-top: 10px;}</style>
    </head>
    <body>
        <h2 style="text-align: center;"  class="form-signin-heading"><fmt:message key="login.title" bundle="${locale}"/></h2>
        <div class="container">
            <div class="form-signin">
                <form action="catalog" method="POST">

                    <input class="form-control" type="text" name="login" placeholder="<fmt:message key="login.label" bundle="${locale}"/>" required autofocus pattern="[A-Za-z0-9]+" />
                    <input  class="form-control" type="password" name="password" placeholder="<fmt:message key="password.label" bundle="${locale}"/>" required />
                    <input type="hidden" name="command" value="log_in" />
                    <input  class="btn btn-lg btn-primary btn-block" type="submit" value="<fmt:message key="login.button" bundle="${locale}"/>" />
                </form>
                <form action="signup" method="GET">
                    <input type="hidden" name="command" value="redirect" />
                    <input type="hidden" name="way_of_redirecting" value="registration_way" />
                    <input class="btn btn-lg btn-primary btn-block"   type="submit" value="<fmt:message key="registration.button" bundle="${locale}" />" />
                </form>
            </div>
        </div>
        <footer>
            <div style="width: 11%; margin: 0 auto;" class="footer-class">
                <form action="login" method="GET">
                    <input type="hidden" name="way_of_redirecting" value="change_locale_way" />
                    <input type="hidden" name="command" value="redirect" />
                    <input style="width: 45%;" class="btn btn-primary" type="submit" name="locale_button" value="En" />
                    <input style="width: 45%;" class="btn btn-primary" type="submit" name="locale_button" value="Ru" />
                </form>
        </footer>
    </body>
</html>
