<%-- 
    Document   : add_product
    Created on : Aug 5, 2015, 12:43:14 PM
    Author     : notepad
--%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <fmt:setLocale value="${sessionScope.locale}" />
        <fmt:setBundle basename="locale" var="locale" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
        <link href="css/bootstrap-theme.min.css" type="text/css" rel="stylesheet">
        <link href="css/signin.css" rel="stylesheet" type="text/css">
        <link href="css/styles.css" rel="stylesheet" type="text/css">
        <script>
            window.onload = function () {
                localeName = '<fmt:message key="param.name.label" bundle="${locale}" />';
                localeValue = '<fmt:message key="param.value.label" bundle="${locale}" />';
            }
        </script>
        <script src="js/scripts.js"></script>
        <title><fmt:message key="product.page.title" bundle="${locale}" /></title>
    </head>
    <body  style="margin-top: 0.01%;">
        <div class="container">
            <div class="form-signin">
                <form action="product" method="POST">
                    <select class="form-control"  name="type">
                        <c:forEach var="type" items="${types}">
                            <option>${type.name}</option>
                        </c:forEach>
                    </select>
                    <input class="form-control" type="text" name="product_name" value="${product.name}" placeholder="<fmt:message key="product.name.label" bundle="${locale}" />" required pattern="[a-zа-я\sА-ЯA-Z0-9]{1,50}" />
                    <input class="form-control" type="text" name="price" value="${product.price}"  placeholder="<fmt:message key="price.label" bundle="${locale}" />" required pattern="[0-9]{1,9}"/>
                    <h3 style="text-align: center;"><fmt:message key="params.label" bundle="${locale}" /></h3>
                    <c:forEach var="item" items="${params}">
                        <input class="form-control" type="text" name="static_param_name" required pattern="[A-ZА-Яa-zа-я\s][a-zа-я\s]{1,50}" value="${item.name}" placeholder="<fmt:message key="param.name.label" bundle="${locale}" />" />
                        <input class="form-control" type="text" name="static_param_value" required pattern="[А-ЯA-Za-zа-я0-9\s\.]{1,20}" value="${item.value}" placeholder="<fmt:message key="param.value.label" bundle="${locale}" />" />
                    </c:forEach>
                    <div id="more-params" onclick="deleteParam(event)"> </div>
                    <div>
                        <input type="button" value="<fmt:message key="new.param.button" bundle="${locale}" />"  class="btn btn-primary"  onclick="addParam()" style="width: 50%; display: block; margin: 5px auto;"/>
                    </div>
                    <input  type="hidden" name="command" value="save_edited_product" />
                    <input type="hidden" name="way_of_redirecting" value="product_way" />
                    <input class="btn btn-lg btn-primary btn-block" type="submit" value="<fmt:message key="save.button" bundle="${locale}" />" />
                </form>
                <form action="product" method="GET">
                    <input type="hidden" name="way_of_redirecting" value="back_way" />
                    <input type="hidden" name="way_attribute" value="to_product" />
                    <input type="hidden" name="command" value="redirect" />
                    <input class="btn btn-lg btn-primary btn-block" type="submit" value="<fmt:message key="back.way.button" bundle="${locale}" />">
                </form>
            </div>
        </div>
        <div style="text-align: center;">
            <label>${completeMessage}</label>
            <label>${warningMessage}</label>
        </div>
    </body>
</html>
