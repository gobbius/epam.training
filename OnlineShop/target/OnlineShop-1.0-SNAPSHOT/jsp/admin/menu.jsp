<%-- 
    Document   : welcomAdmin
    Created on : Jul 22, 2015, 11:32:26 AM
    Author     : notepad
--%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <fmt:setLocale value="${sessionScope.locale}" />
        <fmt:setBundle basename="locale" var="locale"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/styles.css" rel="stylesheet" type="text/css">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
        <link href="css/signin.css" rel="stylesheet" type="text/css">
        <title><fmt:message key="welcome.page.title" bundle="${locale}" /></title>
    </head>
    <body style="margin-top: 0.01%;">
        <h3 style="text-align: center; margin-right: 2.5%"><fmt:message key="admin.menu.label" bundle="${locale}" /></h3>
        <div class="container">
            <div class="form-signin">
                <form class="admin-menu" action="product" method="GET">
                    <input type="hidden" name="command" value="redirect" />
                    <input type="hidden" name="way_of_redirecting" value="add_product_way" />
                    <input class="btn btn-primary" type="submit" value="<fmt:message key="add.product.button" bundle="${locale}" />"/>
                </form>
                <form class="admin-menu" action="catalog" method="GET">
                    <input type="hidden" name="command" value="redirect" />
                    <input type="hidden" name="way_of_redirecting" value="catalog_way" />
                    <input class="btn btn-primary" type="submit" value="<fmt:message key="catalog.button" bundle="${locale}" />"/>
                </form>
                <form class="admin-menu" action="login" method="GET">
                    <input type="hidden" name="way_of_redirecting" value="index_way" />
                    <input type="hidden" name="command" value="redirect" />
                    <input class="btn btn-primary" type="submit" value="<fmt:message key="log.out.button" bundle="${locale}" />">
                </form>
            </div>
        </div>
        <footer><div class="footer-class"><h3><fmt:message key="welcom.label" bundle="${locale}" />${sessionScope.userName}</h3></div></footer>
    </body>
</html>
