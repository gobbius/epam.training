<%-- 
    Document   : payment
    Created on : Aug 18, 2015, 9:25:14 AM
    Author     : notepad
--%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <fmt:setLocale value="${sessionScope.locale}" />
        <fmt:setBundle basename="locale" var="locale"/>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="css/dashboard.css" rel="stylesheet" type="text/css">
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
        <link href="css/styles.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="orders.title" bundle="${locale}" /></title>
    </head>
    <body  style="margin-top: 0.01%;">
        <nav style="margin-top: 0.01%;" class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div style="margin-top: 0.4%; float: left;width: 4%;height: 80%;" class="navbar-header">
                    <form action="login" method="GET">
                        <input type="hidden" name="command" value="redirect" />
                        <input type="hidden" name="way_of_redirecting" value="log_out_way" />
                        <button title="<fmt:message key="log.out.title" bundle="${locale}" />" style="width:100%;" class="form-control" type="submit">
                            <span class="glyphicon glyphicon-log-out"></span>
                        </button>
                    </form>
                </div>
                <div style="margin-top: 0.4%;margin-left: 2%;width: 4%;height: 80%;" class="navbar-header">
                    <form action="cart" method="GET">
                        <input type="hidden" name="command" value="redirect" />
                        <input type="hidden" name="way_of_redirecting" value="cart_way" />
                        <button title="<fmt:message key="cart.title" bundle="${locale}" />" type="submit" style="width: 100%;" class="form-control">
                            <span class="glyphicon glyphicon-shopping-cart"></span>
                        </button>
                    </form>
                </div>
                <div style="margin-top: 0.4%;margin-left: 2%;width: 4%;height: 80%;" class="navbar-header">
                    <form action="payment" method="GET">
                        <input type="hidden" name="command" value="redirect" />
                        <input type="hidden" name="way_of_redirecting" value="payment_way" />
                        <button title="<fmt:message key="payment.title" bundle="${locale}" />" type="submit" style="width: 100%;" class="form-control">
                            <span class="glyphicon glyphicon-thumbs-up"></span>
                        </button>
                    </form>
                </div>  
            </div>
        </nav>
        <div style="text-align: center;" class="container-fluid">
            <h3><fmt:message key="orders.title" bundle="${locale}" /></h3>            
            <div class="row">
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <div style="width: 700px;margin-left: 15%;" class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th style="width: 50%">#</th>
                                    <th style="width: 20%; text-align: center;"><fmt:message key="date.label" bundle="${locale}" /></th>
                                    <th style="width: 20%; text-align: right;"><fmt:message key="total.price.label" bundle="${locale}" /></th>
                                    <th style="width: 5%;"></th>
                                    <th style="width: 5%;"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="order" items="${orders}">
                                    <tr style="text-align: left;">
                                        <td>${order.id}</td>
                                        <td style="text-align: center;">${order.date}</td>
                                        <td style="text-align: right;">${order.totalPrice} <fmt:message key="money.index" bundle="${locale}" /></td>
                                        <td>
                                            <div style="margin-top: -7%;width: 100%;margin-left: 15%;">
                                                <form action="payment" method="POST">
                                                    <input type="hidden" name="command" value="check_out" />
                                                    <input type="hidden" name="order_id" value="${order.id}" />
                                                    <button style="border: 0;height: 50%;" class="form-control">
                                                        <span  class="glyphicon glyphicon-ok"></span>
                                                    </button>
                                                </form>
                                            </div>
                                        </td>
                                        <td>
                                            <div style="margin-top: -7%;width: 100%;margin-left: 15%;">
                                                <form action="payment" method="POST">
                                                    <input type="hidden" name="command" value="remove_order" />
                                                    <input type="hidden" name="order_id" value="${order.id}" />
                                                    <button style="border: 0;height: 50%;" class="form-control">
                                                        <span  class="glyphicon glyphicon-remove"></span>
                                                    </button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                        <div>
                            <div>
                                <form action="payment" method="GET">
                                    <input type="hidden" name="command" value="search_order" />
                                    <input style="margin-left: 70%;margin-top: -0.3%;width: 23%;" class="form-control" type="date" name="date" value="2015-06-01" max="2015-12-31" min="2015-01-01" />
                                    <button style="width: 5%;float: right;margin-top: -4.5%;" class="form-control">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                </form>
                            </div>
                            <div style="width: 20%;margin-left: 53%;margin-top: 10px;">
                                <form action="payment" method="GET">
                                    <input type="hidden" name="command" value="redirect" />
                                    <input type="hidden" name="way_of_redirecting" value="payment_way" />
                                    <input style="width: 100%;" class="btn btn-lg" type="submit" value="<fmt:message key="show.all.orders.button" bundle="${locale}" />" />
                                </form>
                            </div>
                            <div style="width: 25%;margin-left: 75%;margin-top: -56px;">
                                <form action="catalog" method="GET">
                                    <input type="hidden" name="command" value="redirect" />
                                    <input type="hidden" name="way_of_redirecting" value="catalog_way" />
                                    <input style="width: 100%;" class="btn btn-lg" type="submit" value="<fmt:message key="back.button" bundle="${locale}" />" />
                                </form>
                            </div>
                            <div style="margin-top: 3%;">
                                <label>${resultOfPayment}</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </body>
</html>
