<%-- 
    Document   : user_product
    Created on : Aug 12, 2015, 12:50:17 PM
    Author     : notepad
--%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <fmt:setLocale value="${sessionScope.locale}" />
        <fmt:setBundle basename="locale" var="locale"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="css/dashboard.css" rel="stylesheet" type="text/css">
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
        <link href="css/styles.css" rel="stylesheet" type="text/css">
        <title>${product.name}</title>
    </head>
    <body  style="margin-top: 0.01%;"><nav style="margin-top: 0.01%;" class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div style="margin-top: 0.4%; float: left;width: 4%;height: 80%;" class="navbar-header">
                    <form action="login" method="GET">
                        <input type="hidden" name="command" value="redirect" />
                        <input type="hidden" name="way_of_redirecting" value="log_out_way" />
                        <button title="<fmt:message key="log.out.title" bundle="${locale}" />" style="width:100%;" class="form-control" type="submit">
                            <span class="glyphicon glyphicon-log-out"></span>
                        </button>
                    </form>
                </div>
                <div style="margin-top: 0.4%;margin-left: 2%;width: 4%;height: 80%;" class="navbar-header">
                    <form action="cart" method="GET">
                        <input type="hidden" name="command" value="redirect" />
                        <input type="hidden" name="way_of_redirecting" value="cart_way" />
                        <button title="<fmt:message key="cart.title" bundle="${locale}" />" type="submit" style="width: 100%;" class="form-control">
                            <span class="glyphicon glyphicon-shopping-cart"></span>
                        </button>
                    </form>
                </div>
                <div style="margin-top: 0.4%;margin-left: 2%;width: 4%;height: 80%;" class="navbar-header">
                    <form action="payment" method="GET">
                        <input type="hidden" name="command" value="redirect" />
                        <input type="hidden" name="way_of_redirecting" value="payment_way" />
                        <button title="<fmt:message key="payment.title" bundle="${locale}" />" type="submit" style="width: 100%;" class="form-control">
                            <span class="glyphicon glyphicon-thumbs-up"></span>
                        </button>
                    </form>
                </div>  
            </div>
        </nav>
        <div style="width: 700px; margin: 0 auto; text-align: center;">
            <h3><fmt:message key="params.label" bundle="${locale}" /></h3>
            <table style="width: 100%;" class="table table-striped">
                <thead>
                    <tr>
                        <th style="text-align: center;"><fmt:message key="param.name.label" bundle="${locale}" /></th>
                        <th style="text-align: center;"><fmt:message key="param.value.label" bundle="${locale}" /></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="item" items="${params}">
                        <tr>
                            <td style="text-align: left;">${item.name}</td>
                            <td>${item.value}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <div style="text-align: right;">
                <h3><fmt:message key="price.label" bundle="${locale}" />: <fmt:formatNumber value="${product.price}" type="CURRENCY" currencySymbol="" /> <fmt:message key="money.index" bundle="${locale}" /></h3>
            </div>
            <div>
                <div style="margin-left: 330px;width: 180px;">
                    <form action="product" method="POST">
                        <input type="hidden" name="command" value="add_product_to_cart" />
                        <input type="hidden" name="price" value="${product.price}" />
                        <input type="hidden" name="product_id" value="${product.id}" />
                        <input type="hidden" name="product_name" value="${product.name}" />
                        <input type="hidden" name="product_type_id" value="${product.idType}" />
                        <input style="width: 100%;" class="btn btn-lg" type="submit" value="<fmt:message key="add.to.cart.button" bundle="${locale}" />" />
                    </form>
                </div>
                <div style="margin-left: 528px;margin-top: -55px;width: 180px;">
                    <form action="catalog" method="GET">
                        <input type="hidden" name="command" value="redirect" />
                        <input type="hidden" name="way_of_redirecting" value="catalog_way" />
                        <input style="width: 100%;" class="btn btn-lg" type="submit" value="<fmt:message key="back.button" bundle="${locale}" />" />
                    </form>
                </div>
            </div>
            <div style="margin-top: 3%;">
                <label>${resultOfAdding}</label>
            </div>
        </div>
    </body>
</html>
