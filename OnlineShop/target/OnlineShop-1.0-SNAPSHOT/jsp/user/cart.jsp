<%-- 
    Document   : cart
    Created on : Aug 13, 2015, 4:31:48 PM
    Author     : notepad
--%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <fmt:setLocale value="${sessionScope.locale}" />
        <fmt:setBundle basename="locale" var="locale"/>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="css/dashboard.css" rel="stylesheet" type="text/css">
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
        <link href="css/styles.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="cart.title" bundle="${locale}" /></title>
    </head>
    <body  style="margin-top: 0.01%;">
        <nav style="margin-top: 0.01%;" class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div style="margin-top: 0.4%; float: left;width: 4%;height: 80%;" class="navbar-header">
                    <form action="login" method="GET">
                        <input type="hidden" name="command" value="redirect" />
                        <input type="hidden" name="way_of_redirecting" value="log_out_way" />
                        <button title="<fmt:message key="log.out.title" bundle="${locale}" />" style="width:100%;" class="form-control" type="submit">
                            <span class="glyphicon glyphicon-log-out"></span>
                        </button>
                    </form>
                </div>
                <div style="margin-top: 0.4%;margin-left: 2%;width: 4%;height: 80%;" class="navbar-header">
                    <form action="cart" method="GET">
                        <input type="hidden" name="command" value="redirect" />
                        <input type="hidden" name="way_of_redirecting" value="cart_way" />
                        <button title="<fmt:message key="cart.title" bundle="${locale}" />" type="submit" style="width: 100%;" class="form-control">
                            <span class="glyphicon glyphicon-shopping-cart"></span>
                        </button>
                    </form>
                </div>
                <div style="margin-top: 0.4%;margin-left: 2%;width: 4%;height: 80%;" class="navbar-header">
                    <form action="payment" method="GET">
                        <input type="hidden" name="command" value="redirect" />
                        <input type="hidden" name="way_of_redirecting" value="payment_way" />
                        <button title="<fmt:message key="payment.title" bundle="${locale}" />" type="submit" style="width: 100%;" class="form-control">
                            <span class="glyphicon glyphicon-thumbs-up"></span>
                        </button>
                    </form>
                </div>             
            </div>
        </nav>
        <div style="text-align: center;" class="container-fluid">
            <h3><fmt:message key="cart.label" bundle="${locale}" /></h3>            
            <div class="row">
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <div style="width: 700px;margin-left: 15%;" class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th><fmt:message key="name.label" bundle="${locale}" /></th>
                                    <th style="width: 25%;text-align: center;"><fmt:message key="price.label" bundle="${locale}" /></th>
                                    <th style="width: 10%;"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="product" items="${sessionScope.cartProducts}">
                                    <tr style="text-align: left;">
                                        <td>${product.name}</td>
                                        <td style="text-align: right;">${product.price} <fmt:message key="money.index" bundle="${locale}" /></td>
                                        <td>
                                            <div style="width: 70%;margin-left: 20%;margin-top: -6%;">
                                                <form action="cart" method="POST">
                                                    <input type="hidden" name="command" value="remove_from_cart" />
                                                    <input type="hidden" name="product_id" value="${product.id}" />
                                                    <button style="width: 100%; height: 100%;border: 0;" class="form-control">
                                                        <span  class="glyphicon glyphicon-remove"></span>
                                                    </button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                        <div>
                            <form style="height: 100%;" action="cart" method="GET">
                                <input type="hidden" name="command" value="commit_order" />
                                <div style="margin-left: 75%;width: 25%;">
                                    <input class="form-control" type="date" name="date" value="2015-06-01" max="2015-12-31" min="2015-01-01" />                        
                                </div>
                                <div style="margin-left: 53%; width: 20%;">
                                    <input style="width: 100%;" class="btn btn-lg" type="submit" value="<fmt:message key="commit.order.button" bundle="${locale}" />" />
                                </div>
                            </form>
                            <div style="margin-left: 75%;margin-top: -56px;width: 25%;">
                                <form action="catalog" method="GET">
                                    <input type="hidden" name="command" value="redirect" />
                                    <input type="hidden" name="way_of_redirecting" value="catalog_way" />
                                    <input style="width: 100%;" class="btn btn-lg" type="submit" value="<fmt:message key="back.button" bundle="${locale}" />" />
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
