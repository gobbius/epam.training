<%-- 
    Document   : registration
    Created on : Aug 3, 2015, 9:39:38 AM
    Author     : notepad
--%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <fmt:setLocale value="${sessionScope.locale}" />
        <fmt:setBundle basename="locale" var="locale" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
        <link href="css/bootstrap-theme.min.css" type="text/css" rel="stylesheet">
        <link href="css/signin.css" rel="stylesheet" type="text/css">
        <link href="css/styles.css" rel="stylesheet" type="text/css">
        <style>input{margin-top: 10px;}</style>
        <title><fmt:message key="registration.button" bundle="${locale}" /></title>
    </head>
    <body  style="margin-top: 0.01%;">
        <h2 style="text-align: center;"><fmt:message key="registration.title" bundle="${locale}" /></h2>
        <div class="container">
            <div class="form-signin">  
                <form action="login" method="POST">
                    <input class="form-control" type="text" name="name" placeholder="<fmt:message key="name.label" bundle="${locale}" />" required pattern="[А-ЯA-Zа-яa-z]{1,50}"/>
                    <input class="form-control" type="text" name="lastName" placeholder="<fmt:message key="last.name.label" bundle="${locale}" />" required pattern="[А-ЯA-Zа-яa-z]{1,50}" />
                    <input class="form-control" type="text" name="login" placeholder="<fmt:message key="login.reg.label" bundle="${locale}" />" required pattern="[A-Za-z0-9]+" />
                    <input class="form-control" type="password" name="password" placeholder="<fmt:message key="password.reg.label" bundle="${locale}" />" required pattern="[A-Za-z0-9]+" />
                    <input class="form-control" type="text" name="phoneNumber" placeholder="<fmt:message key="phone.number.reg.label" bundle="${locale}" />" required pattern="[0-9]{7}"/>
                    <input class="form-control" type="text" name="adress" placeholder="<fmt:message key="adress.label" bundle="${locale}"/>" pattern=".{1,50}" />
                    <input class="form-control" type="text" name="mail" placeholder="<fmt:message key="mail.reg.label" bundle="${locale}" />" required pattern="[a-z]+[0-9]*@[a-z]+\.[a-z]+"/>
                    <input class="form-control" type="text" name="cardNumber" placeholder="<fmt:message key="card.number.reg.label" bundle="${locale}" />" required pattern="[0-9]{9}" />
                    <input type="hidden" name="command" value="registration" />
                    <input class="btn btn-lg btn-primary btn-block" type="submit" value="<fmt:message key="registrate.button" bundle="${locale}" />" />
                </form>
                <form action="login" method="GET">
                    <input type="hidden" name="way_of_redirecting" value="index_way" />
                    <input type="hidden" name="command" value="redirect" />
                    <input class="btn btn-lg btn-primary btn-block" type="submit" value="<fmt:message key="back.way.button" bundle="${locale}" />">
                </form>
            </div>
        </div>
    </body>
</html>
