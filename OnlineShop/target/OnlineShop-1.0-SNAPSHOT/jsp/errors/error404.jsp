<%-- 
    Document   : error404
    Created on : Aug 24, 2015, 4:18:03 PM
    Author     : notepad
--%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <fmt:setLocale value="${sessionScope.locale}" />
        <fmt:setBundle basename="locale" var="locale"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>404</title>
    </head>
    <body  style="margin-top: 0.01%;text-align: center">
        <div class="container">
            <h1>404 ERROR</h1>
        </div>
    </body>
</html>
