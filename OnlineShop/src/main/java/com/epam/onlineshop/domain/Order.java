/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.domain;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author notepad
 */
public class Order implements Serializable{

    private int id;
    private int idUser;
    private Date date;
    private int totalPrice;
    private boolean paid;

    public Order() {
    }

    public Order(int idUser, Date date, int totalPrice, boolean paid) {
        this.idUser = idUser;
        this.date = date;
        this.totalPrice = totalPrice;
        this.paid = paid;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Date getDate() {
        return date;
    }

    public int getId() {
        return id;
    }

    public int getIdUser() {
        return idUser;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public boolean isPaid() {
        return paid;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.id;
        result = prime * result + this.idUser;
        result = prime * result + this.totalPrice;
        result = prime * result + this.date.hashCode();
        result = prime * result + Boolean.toString(paid).hashCode();
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (this.getClass() != o.getClass()) {
            return false;
        }
        Order other = (Order) o;
        if (this.id != other.id) {
            return false;
        }
        if (this.idUser != other.idUser) {
            return false;
        }
        if (this.totalPrice != other.totalPrice) {
            return false;
        }
        if (!this.date.equals(other.date)) {
            return false;
        }
        if (Boolean.compare(this.paid, other.paid) != 0) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "id: " + this.id + "idUser: " + this.idUser + "date: " + this.date.toString() + "total price: " + this.totalPrice + "paid or not: " + this.paid;
    }

}
