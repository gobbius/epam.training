/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.domain;

import java.io.Serializable;

/**
 *
 * @author notepad
 */
public class Product implements Serializable {

    private int id;
    private int idType;
    private int idOrder;
    private String name;
    private int price;

    public Product() {
    }

    public Product(int idType, String name, int price) {
        this.idType = idType;
        this.name = name;
        this.price = price;
    }

    public Product(int id, int idType, String name, int price) {
        this.id = id;
        this.idType = idType;
        this.name = name;
        this.price = price;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdOrder() {
        return idOrder;
    }

    public void setIdType(int idType) {
        this.idType = idType;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    public int getIdType() {
        return idType;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.id;
        result = prime * result + this.name.hashCode();
        result = prime * result + this.idOrder;
        result = prime * result + this.idType;
        result = prime * result + this.price;
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (this.getClass() != o.getClass()) {
            return false;
        }
        Product other = (Product) o;
        if (this.id != other.id) {
            return false;
        }
        if (!this.name.equals(other.name)) {
            return false;
        }
        if (this.idOrder != other.idOrder) {
            return false;
        }
        if (this.idType != other.idType) {
            return false;
        }
        if (this.price != other.price) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "id: " + this.id + "idType: " + this.idType + "idOrder: " + this.idOrder + "name: " + this.name + "price: " + this.price;
    }

}
