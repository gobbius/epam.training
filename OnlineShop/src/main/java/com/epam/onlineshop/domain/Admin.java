/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.domain;

import java.io.Serializable;

/**
 *
 * @author notepad
 */
public class Admin implements Serializable{

    private int id;
    private String name;
    private String login;
    private String password;

    public Admin() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.id;
        result = prime * result + this.name.hashCode();
        result = prime * result + this.login.hashCode();
        result = prime * result + this.password.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (this.getClass() != o.getClass()) {
            return false;
        }
        Admin other = (Admin) o;
        if (this.id != other.id) {
            return false;
        }
        if (!this.name.equals(other.name)) {
            return false;
        }
        if (!this.login.equals(other.login)) {
            return false;
        }
        if (!this.password.equals(other.password)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "id: " + this.id + "name: " + this.name + "login: " + this.login + "password: " + this.password;
    }

}
