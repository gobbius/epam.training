/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.domain;

import java.io.Serializable;

/**
 *
 * @author notepad
 */
public class User implements Serializable{

    private int id;
    private String name;
    private String lastName;
    private String login;
    private String password;
    private int phoneNumber;
    private String adress;
    private String mail;
    private int cardNumber;

    public User() {
    }

    public User(String name, String lastName, String login, String password, int phoneNumber, String adress, String mail, int cardNumber) {
        this.name = name;
        this.lastName = lastName;
        this.login = login;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.adress = adress;
        this.mail = mail;
        this.cardNumber = cardNumber;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPhoneNumber(int phoneN) {
        this.phoneNumber = phoneN;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setCardNumber(int cardN) {
        this.cardNumber = cardN;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public String getLastName() {
        return lastName;
    }

    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public String getMail() {
        return mail;
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public String getAdress() {
        return adress;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.id;
        result = prime * result + this.phoneNumber;
        result = prime * result + this.cardNumber;
        result = prime * result + this.password.hashCode();
        result = prime * result + this.login.hashCode();
        result = prime * result + this.mail.hashCode();
        result = prime * result + this.name.hashCode();
        result = prime * result + this.adress.hashCode();
        result = prime * result + this.lastName.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (this.getClass() != o.getClass()) {
            return false;
        }
        User other = (User) o;
        if (this.id != other.id) {
            return false;
        }
        if (this.cardNumber != other.cardNumber) {
            return false;
        }
        if (this.phoneNumber != other.phoneNumber) {
            return false;
        }
        if (!this.name.equals(other.name)) {
            return false;
        }
        if (!this.lastName.equals(other.lastName)) {
            return false;
        }
        if (!this.adress.equals(other.adress)) {
            return false;
        }
        if (!this.mail.equals(other.mail)) {
            return false;
        }
        if (!this.login.equals(other.login)) {
            return false;
        }
        if (!this.password.equals(other.password)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.name + " " + this.lastName + " " + this.login + " " + this.password + " " + this.phoneNumber + " " + this.adress + " " + this.mail + " " + this.cardNumber;
    }

}
