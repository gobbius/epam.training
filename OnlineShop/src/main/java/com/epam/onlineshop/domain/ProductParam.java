/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.domain;

import java.io.Serializable;

/**
 *
 * @author notepad
 */
public class ProductParam implements Serializable{

    private int id;
    private int idProduct;
    private String name;
    private String value;

    public ProductParam() {
    }

    public ProductParam(int idProduct, String name, String value) {
        this.idProduct = idProduct;
        this.name = name;
        this.value = value;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.id;
        result = prime * result + this.name.hashCode();
        result = prime * result + this.idProduct;
        result = prime * result + this.value.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (this.getClass() != o.getClass()) {
            return false;
        }
        ProductParam other = (ProductParam) o;
        if (this.id != other.id) {
            return false;
        }
        if (!this.name.equals(other.name)) {
            return false;
        }
        if (!this.value.equals(other.value)) {
            return false;
        }
        if (this.idProduct != other.idProduct) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "id: " + this.id + "idProduct: " + this.idProduct + "name: " + this.name + "value: " + this.value;
    }

}
