/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.controller.listner;

import com.epam.onlineshop.dao.pool.ConnectionPoolException;
import com.epam.onlineshop.dao.pool.ConnectionPool;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.Logger;

/**
 *
 * @author notepad
 */
public class ContextListner implements ServletContextListener {

    private final static String EXCEPTION_MESSAGE = "You have exception: ";
    private final static Logger log = Logger.getLogger(ContextListner.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            ConnectionPool.getInstance();
        } catch (ConnectionPoolException ex) {
            log.error(EXCEPTION_MESSAGE, ex);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        try {
            ConnectionPool.getInstance().dispose();
        } catch (ConnectionPoolException ex) {
            log.error(EXCEPTION_MESSAGE, ex);
        }
    }

}
