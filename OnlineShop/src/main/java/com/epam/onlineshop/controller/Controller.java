/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.controller;

import com.epam.onlineshop.command.CommandException;
import com.epam.onlineshop.command.CommandFactory;
import com.epam.onlineshop.command.ICommand;
import com.epam.onlineshop.manager.PageManager;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import org.apache.log4j.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author notepad
 */
public class Controller extends HttpServlet {
/**
* The class {@code Controller} is servlet for handling
* user queries. Can be declared as action handler
* on jsp pages using attribute action in form.
* @author notepad
*/
    private final static String ERROR_PAGE = PageManager.getInstance().getPage(PageManager.ERROR_PAGE);
    private final static String COMMAND_NAME = "command";
    private final static String EXCEPTION_MESSAGE = "You have exception: ";
    private final static String MESSAGE = "message";
    private final static Logger log = Logger.getLogger(Controller.class);
    private final static CommandFactory commandFactory = CommandFactory.getInstance();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) {
        String forwardPage = null;
        String commandName = request.getParameter(COMMAND_NAME);
        ICommand command = commandFactory.getCommand(commandName);
        try {
            forwardPage = command.execute(request);
            RequestDispatcher dispatcher = request.getRequestDispatcher(forwardPage);
            dispatcher.forward(request, response);
        } catch (CommandException | ServletException | IOException ex) {
            log.error(EXCEPTION_MESSAGE + ex.getMessage());
            errorMessageDireclyFromResponse(request, response, ex.getMessage());
        }
    }

    private void errorMessageDireclyFromResponse(HttpServletRequest request, HttpServletResponse response, String message) {
        request.setAttribute(MESSAGE, message);
        RequestDispatcher dispatcher = request.getRequestDispatcher(ERROR_PAGE);
        try {
            dispatcher.include(request, response);
        } catch (ServletException | IOException ex) {
            log.error(ex.getMessage());
        }
    }
}
