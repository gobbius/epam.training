/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.service;

import com.epam.onlineshop.dao.DaoException;
import com.epam.onlineshop.dao.impl.ProductDao;
import com.epam.onlineshop.dao.impl.ProductTypeDao;
import com.epam.onlineshop.domain.Product;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author notepad
 */
public class GetProductsByTypeAndSortedByPrice {

    private final static ProductTypeDao productTypeDao = new ProductTypeDao();
    private final static ProductDao productDao = new ProductDao();
    private final static String PRODUCTS_ATTRIBUTE_NAME = "products";
    private final static String PRICE_PARAM_ASC_EN = "Ascending";
    private final static String PRICE_PARAM_ASC_RU = "По возрастанию";
    private final static String ASCENDING_SORT = "ASC";
    private final static String DESCENDING_SORT = "DESC";
    private final static String ALL_TYPES_ATTRIBUTE_NAME_EN = "All";
    private final static String ALL_TYPES_ATTRIBUTE_NAME_RU = "Все";

    public static void getProducts(HttpServletRequest request, String typeName, String priceParam) throws ServiceException {
        int idOfType = 0;
        String paramOfSort = null;
        List<Product> products = null;
        try {
            if (typeName.equals(ALL_TYPES_ATTRIBUTE_NAME_EN) || typeName.equals(ALL_TYPES_ATTRIBUTE_NAME_RU)) {
                idOfType = 0;
            } else {
                idOfType = productTypeDao.getIdTypeByName(typeName);
            }
            if (priceParam.equals(PRICE_PARAM_ASC_EN) || priceParam.equals(PRICE_PARAM_ASC_RU)) {
                paramOfSort = ASCENDING_SORT;
            } else {
                paramOfSort = DESCENDING_SORT;
            }
            products = productDao.getProductsSortedByTypeAndPrice(idOfType, paramOfSort);
            request.setAttribute(PRODUCTS_ATTRIBUTE_NAME, products);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }
}
