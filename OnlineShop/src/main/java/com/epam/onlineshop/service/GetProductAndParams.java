/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.service;

import com.epam.onlineshop.dao.DaoException;
import com.epam.onlineshop.dao.impl.ProductDao;
import com.epam.onlineshop.dao.impl.ProductParamDao;
import com.epam.onlineshop.domain.Product;
import com.epam.onlineshop.domain.ProductParam;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author notepad
 */
public class GetProductAndParams {

    private final static ProductDao productDao = new ProductDao();
    private final static ProductParamDao productParamDao = new ProductParamDao();
    private final static String PARAMS_ATTRIBUTE_NAME = "params";
    private final static String PRODUCT_ATTRIBUTE_NAME = "product";

    public static void getParamsAndName(int productId, HttpSession session) throws ServiceException {
        Product product = null;
        List<ProductParam> params = null;
        try {
            params = productParamDao.getParamsByProductId(productId);
            product = productDao.getProductById(productId);
            session.setAttribute(PARAMS_ATTRIBUTE_NAME, params);
            session.setAttribute(PRODUCT_ATTRIBUTE_NAME, product);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }
}
