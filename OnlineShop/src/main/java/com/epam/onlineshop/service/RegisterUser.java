/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.service;

import com.epam.onlineshop.dao.DaoException;
import com.epam.onlineshop.dao.impl.UserDao;
import com.epam.onlineshop.domain.User;

/**
 *
 * @author notepad
 */
public class RegisterUser {

    private final static UserDao userDao = new UserDao();

    public static int register(String userName, String userLastName, String userLogin, String userPassword, int userPhoneNumber, String userAdress, String userMail, int userCardNumber) throws ServiceException {
        int resultOfRegistration = 0;
        User user = new User(userName, userLastName, userLogin, userPassword, userPhoneNumber, userAdress, userMail, userCardNumber);
        try {
            resultOfRegistration = userDao.insertUser(user);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
        return resultOfRegistration;
    }
}
