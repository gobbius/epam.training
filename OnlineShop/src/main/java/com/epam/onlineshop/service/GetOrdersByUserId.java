/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.service;

import com.epam.onlineshop.dao.DaoException;
import com.epam.onlineshop.dao.impl.OrderDao;
import com.epam.onlineshop.domain.Order;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author notepad
 */
public class GetOrdersByUserId {

    private final static String ORDERS_SESSION_ATTRIBUTE_NAME = "orders";
    private final static OrderDao orderDao = new OrderDao();

    public static void getOrders(HttpSession session, int userId) throws ServiceException {
        List<Order> orders = null;
        try {
            orders = orderDao.getOrdersByUserId(userId);
            session.setAttribute(ORDERS_SESSION_ATTRIBUTE_NAME, orders);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }
}
