/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.service;

import com.epam.onlineshop.dao.DaoException;
import com.epam.onlineshop.dao.impl.ProductDao;
import com.epam.onlineshop.domain.Product;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author notepad
 */
public class FindProductByName {
    private final static String PRODUCTS_ATTRIBUTE_NAME = "products";
    private final static ProductDao productDao = new ProductDao();

    public static boolean findProduct(String productName, HttpServletRequest request) throws ServiceException {
        List<Product> products = null;
        boolean resultOfSearching = false;
        String productNameForTransaction = productName+'%';
        try {
            products = productDao.getProductsByName(productNameForTransaction);
            if (!products.isEmpty()) {
                request.setAttribute(PRODUCTS_ATTRIBUTE_NAME, products);
                resultOfSearching = true;
            }else{
                products = productDao.getListOfEntities();
                request.setAttribute(PRODUCTS_ATTRIBUTE_NAME, products);
            }
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
        return resultOfSearching;
    }
}
