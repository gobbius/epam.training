/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.service;

import com.epam.onlineshop.domain.Product;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author notepad
 */
public class RemoveFromCart {

    private final static String CART_PRODUCTS_ATTRIBUTE_NAME = "cartProducts";

    public static void remove(HttpSession session, int productId) throws ServiceException {
        List<Product> products = null;
        Product product = null;
        products = (List<Product>) session.getAttribute(CART_PRODUCTS_ATTRIBUTE_NAME);
        for (Product p : products) {
            if (p.getId() == productId) {
                product = p;
            }
        }
        products.remove(product);
        session.setAttribute(CART_PRODUCTS_ATTRIBUTE_NAME, products);
    }
}
