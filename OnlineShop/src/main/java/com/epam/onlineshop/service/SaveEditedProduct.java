/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.service;

import com.epam.onlineshop.dao.DaoException;
import com.epam.onlineshop.dao.impl.ProductDao;
import com.epam.onlineshop.dao.impl.ProductTypeDao;
import com.epam.onlineshop.domain.Product;
import com.epam.onlineshop.domain.ProductParam;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author notepad
 */
public class SaveEditedProduct {

    private final static String PARAMS_ATTRIBUTE_NAME = "params";
    private final static String PRODUCT_ATTRIBUTE_NAME = "product";
    private final static int PRODUCT_WAS_UPDATED = 1;
    private final static ProductDao productDao = new ProductDao();
    private final static ProductTypeDao productTypeDao = new ProductTypeDao();

    public static int save(String[] paramNames, String[] paramValues, String[] staticParamNames, String[] staticParamValues, String productType, String productName, int productPrice, HttpServletRequest request) throws ServiceException {
        int idOfType = 0;
        HttpSession session = request.getSession();
        List<ProductParam> oldParams = (List<ProductParam>) session.getAttribute(PARAMS_ATTRIBUTE_NAME);
        int resultOfSavingParams = 0;
        int resultOfUpdatingProduct = 0;
        int productId = 0;
        Product product = null;
        try {
            idOfType = productTypeDao.getIdTypeByName(productType);
            productId = ((Product) session.getAttribute(PRODUCT_ATTRIBUTE_NAME)).getId();
            product = new Product(productId, idOfType, productName, productPrice);
            resultOfUpdatingProduct = productDao.updateProduct(product);
            resultOfSavingParams = SaveEditedProductParams.save(paramNames, paramValues, staticParamNames, staticParamValues, productId, oldParams, session);
            if (resultOfUpdatingProduct == PRODUCT_WAS_UPDATED) {
                session.setAttribute(PRODUCT_ATTRIBUTE_NAME, product);
            }
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
        return resultOfUpdatingProduct;
    }
}
