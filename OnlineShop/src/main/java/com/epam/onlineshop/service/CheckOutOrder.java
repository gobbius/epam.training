/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.service;

import com.epam.onlineshop.dao.DaoException;
import com.epam.onlineshop.dao.impl.OrderDao;
import com.epam.onlineshop.domain.Order;

/**
 *
 * @author notepad
 */
public class CheckOutOrder {

    private final static OrderDao orderDao = new OrderDao();

    public static void checkOut(Order order) throws ServiceException {
        try {
            orderDao.setPaid(order.getId());
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }
}
