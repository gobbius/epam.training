/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.service;

import com.epam.onlineshop.dao.DaoException;
import com.epam.onlineshop.dao.impl.OrderDao;
import com.epam.onlineshop.domain.Order;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author notepad
 */
public class RemoveOrder {

    private final static String ORDERS_SESSION_ATTRIBUTE_NAME = "orders";
    private final static int ORDER_IS_REMOVED = 1;
    private final static OrderDao orderDao = new OrderDao();

    public static boolean remove(int orderId, HttpSession session) throws ServiceException {
        List<Order> orders = null;
        Order order = null;
        int resultOfRemovingOrder = 0;
        boolean resultOfRemovingFromSession = false;
        orders = (List<Order>) session.getAttribute(ORDERS_SESSION_ATTRIBUTE_NAME);
        for (Order o : orders) {
            if (o.getId() == orderId) {
                order = o;
            }
        }
        try {
            if (order != null) {
                resultOfRemovingOrder = orderDao.deleteEntityById(order.getId());
            }
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
        if (resultOfRemovingOrder == ORDER_IS_REMOVED) {
            resultOfRemovingFromSession = orders.remove(order);
            session.setAttribute(ORDERS_SESSION_ATTRIBUTE_NAME, orders);
        }
        return resultOfRemovingFromSession;
    }
}
