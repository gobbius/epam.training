/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.service;

import com.epam.onlineshop.dao.DaoException;
import com.epam.onlineshop.dao.impl.ProductDao;
import com.epam.onlineshop.dao.impl.ProductParamDao;
import com.epam.onlineshop.domain.ProductParam;
import com.epam.onlineshop.manager.RegexpManager;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author notepad
 */
public class SaveEditedProductParams {

    private final static ProductParamDao productParamDao = new ProductParamDao();
    private final static ProductDao productDao = new ProductDao();
    private final static String PARAMS_ATTRIBUTE_NAME = "params";
    private final static int PARAMS_WAS_SAVED_WITHOUT_NEW_PARAMS = 1;
    private final static int PARAMS_WAS_SAVED_WITH_NEW_PARAMS = 0;
    private final static int PARAMS_WAS_NOT_INSERTED = 0;
    public final static String REGEXP_FOR_PARAM_NAME = RegexpManager.getInstance().getRegexp(RegexpManager.REGEXP_FOR_PARAM_NAME);
    public final static String REGEXP_FOR_PARAM_VALUE = RegexpManager.getInstance().getRegexp(RegexpManager.REGEXP_FOR_PARAM_VALUE);

    public static int save(String[] paramNames, String[] paramValues, String[] staticParamNames, String[] staticParamValues, int productId, List<ProductParam> oldParams, HttpSession session) throws ServiceException {
        ProductParam param = null;
        int resultOfAddingParams = 0;
        int resultOfUpdatingParams = 0;
        int resultOfSaving = 0;
        int paramId = 0;
        try {
            List<ProductParam> newParams = new ArrayList<ProductParam>();
            List<ProductParam> paramsForUpdate = new ArrayList<ProductParam>();
            List<ProductParam> allParams = new ArrayList<ProductParam>();
            if (paramNames != null) {
                for (int i = 0; i < paramNames.length; i++) {
                    if (paramNames[i].matches(REGEXP_FOR_PARAM_NAME) && paramValues[i].matches(REGEXP_FOR_PARAM_VALUE)) {
                        param = new ProductParam();
                        param.setIdProduct(productId);
                        param.setName(paramNames[i]);
                        param.setValue(paramValues[i]);
                        newParams.add(param);
                        allParams.add(param);
                        param = null;
                    }
                }
            }
            if (staticParamNames != null) {
                for (int i = 0; i < staticParamNames.length; i++) {
                    if (staticParamNames[i].matches(REGEXP_FOR_PARAM_NAME) && staticParamValues[i].matches(REGEXP_FOR_PARAM_VALUE)) {
                        param = new ProductParam();
                        paramId = oldParams.get(i).getId();
                        param.setId(paramId);
                        param.setIdProduct(productId);
                        param.setName(staticParamNames[i]);
                        param.setValue(staticParamValues[i]);
                        paramsForUpdate.add(param);
                        allParams.add(param);
                        param = null;
                    }
                }
            }
            resultOfAddingParams = productParamDao.insertParams(newParams);
            resultOfUpdatingParams = productParamDao.updateProductParam(paramsForUpdate);
            if (resultOfUpdatingParams > 0 && resultOfAddingParams == PARAMS_WAS_NOT_INSERTED) {
                resultOfSaving = PARAMS_WAS_SAVED_WITHOUT_NEW_PARAMS;
            } else {
                resultOfSaving = PARAMS_WAS_SAVED_WITH_NEW_PARAMS;
            }
            session.setAttribute(PARAMS_ATTRIBUTE_NAME, allParams);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
        return resultOfSaving;
    }
}
