/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.service;

import com.epam.onlineshop.dao.DaoException;
import com.epam.onlineshop.dao.impl.ProductDao;
import com.epam.onlineshop.dao.impl.ProductParamDao;
import com.epam.onlineshop.domain.Product;
import com.epam.onlineshop.domain.ProductParam;
import com.epam.onlineshop.manager.RegexpManager;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author notepad
 */
public class AddProductParams {

    private final static ProductParamDao productParamDao = new ProductParamDao();
    private final static ProductDao productDao = new ProductDao();
    public final static String REGEXP_FOR_PARAM_NAME = RegexpManager.getInstance().getRegexp(RegexpManager.REGEXP_FOR_PARAM_NAME);
    public final static String REGEXP_FOR_PARAM_VALUE = RegexpManager.getInstance().getRegexp(RegexpManager.REGEXP_FOR_PARAM_VALUE);

    public static int addParams(String[] paramsNames, String[] paramsValues, Product product) throws ServiceException {
        ProductParam param = null;
        int resultOfAddingParams = 0;
        try {
            int productId = productDao.getIdProduct(product);
            List<ProductParam> params = new ArrayList<ProductParam>();
            if (paramsNames != null) {
                for (int i = 0; i < paramsNames.length; i++) {
                    if (paramsNames[i].matches(REGEXP_FOR_PARAM_NAME) && paramsValues[i].matches(REGEXP_FOR_PARAM_VALUE)) {
                        param = new ProductParam();
                        param.setIdProduct(productId);
                        param.setName(paramsNames[i]);
                        param.setValue(paramsValues[i]);
                        params.add(param);
                        param = null;
                    }
                }
            } else {
                return resultOfAddingParams;
            }
            resultOfAddingParams = productParamDao.insertParams(params);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
        return resultOfAddingParams;
    }
}
