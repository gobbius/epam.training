/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.service;

import com.epam.onlineshop.dao.DaoException;
import com.epam.onlineshop.dao.impl.ProductDao;
import com.epam.onlineshop.dao.impl.ProductParamDao;
import com.epam.onlineshop.domain.Product;
import com.epam.onlineshop.domain.ProductParam;
import java.util.List;

/**
 *
 * @author notepad
 */
public class DeleteProductWithParams {

    private final static ProductDao productDao = new ProductDao();
    private final static ProductParamDao productParamDao = new ProductParamDao();

    public static int deleteProduct(Product product, List<ProductParam> params) throws ServiceException {
        int resultOfDeleting = 0;
        try {
            for (ProductParam param : params) {
                productParamDao.deleteEntityById(param.getId());
            }
            resultOfDeleting = productDao.deleteEntityById(product.getId());
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
        return resultOfDeleting;
    }
}
