/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.service;

import com.epam.onlineshop.dao.DaoException;
import com.epam.onlineshop.dao.impl.AdminDao;
import com.epam.onlineshop.dao.impl.UserDao;
import com.epam.onlineshop.domain.Admin;
import com.epam.onlineshop.domain.User;
import javax.servlet.http.HttpSession;

/**
 *
 * @author notepad
 */
public class CheckUser {

    private final static String USER_NAME_ATTRIBUTE = "userName";
    private final static String USER_ID_ATTRIBUTE_NAME = "userId";
    private final static String ADMIN_ATTRIBUTE_NAME = "isAdmin";
    private final static String YES_ITS_ADMIN = "yes";
    private final static String NO_ITS_NOT_ADMIN = "no";
    private final static UserDao userDao = new UserDao();
    private final static AdminDao adminDao = new AdminDao();

    public static int checkUser(String login, String password, HttpSession session) throws ServiceException {

        String userName = null;
        User user = null;
        Admin admin = null;
        int resultOfChecking = 0;
        try {
            user = userDao.getUser(login, password);
            admin = adminDao.getAdmin(login, password);
            if (user != null) {
                userName = user.getName();
                session.setAttribute(USER_NAME_ATTRIBUTE, user.getName());
                session.setAttribute(ADMIN_ATTRIBUTE_NAME, NO_ITS_NOT_ADMIN);
                session.setAttribute(USER_ID_ATTRIBUTE_NAME, user.getId());
                resultOfChecking = 2;
            } else if (admin != null) {
                userName = admin.getName();
                session.setAttribute(USER_NAME_ATTRIBUTE, admin.getName());
                session.setAttribute(ADMIN_ATTRIBUTE_NAME, YES_ITS_ADMIN);
                resultOfChecking = 1;
            } else {
            }
            return resultOfChecking;
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }
}
