/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.service;

import com.epam.onlineshop.dao.DaoException;
import com.epam.onlineshop.dao.impl.ProductDao;
import com.epam.onlineshop.domain.Product;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author notepad
 */
public class GetAllProducts {
    private final static ProductDao productDao = new ProductDao();
    private final static String PRODUCTS_ATTRIBUTE_NAME = "products";
    public static void getProducts(HttpServletRequest request) throws ServiceException {
        List<Product> products = null;
        try {
            products = productDao.getListOfEntities();
            request.setAttribute(PRODUCTS_ATTRIBUTE_NAME, products);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }
}
