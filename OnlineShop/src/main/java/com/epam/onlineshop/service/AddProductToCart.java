/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.service;

import com.epam.onlineshop.domain.Product;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author notepad
 */
public class AddProductToCart {

    private final static String CART_PRODUCTS_ATTRIBUTE_NAME = "cartProducts";

    public static void AddToCart(int productPrice, int productId, String productName,int productTypeId, HttpSession session) throws ServiceException{
        Product product = new Product();
        List<Product> products = null;
        if (session.getAttribute(CART_PRODUCTS_ATTRIBUTE_NAME) == null) {
            products = new ArrayList<Product>();
        } else {
            products = (List<Product>) session.getAttribute(CART_PRODUCTS_ATTRIBUTE_NAME);
        }
        product.setId(productId);
        product.setIdType(productTypeId);
        product.setPrice(productPrice);
        product.setName(productName);
        products.add(product);
        session.setAttribute(CART_PRODUCTS_ATTRIBUTE_NAME, products);
    }
}
