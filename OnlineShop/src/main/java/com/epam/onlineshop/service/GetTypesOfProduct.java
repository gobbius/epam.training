/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.service;

import com.epam.onlineshop.dao.DaoException;
import com.epam.onlineshop.dao.impl.ProductTypeDao;
import com.epam.onlineshop.domain.ProductType;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author notepad
 */
public class GetTypesOfProduct {

    private final static ProductTypeDao productTypeDao = new ProductTypeDao();
    private final static String TYPES_ATTRIBUTE_NAME = "types";
    public static void getTypes(HttpServletRequest request) throws ServiceException {
        List<ProductType> types = null;
        try {
            types = productTypeDao.getListOfEntities();
            request.setAttribute(TYPES_ATTRIBUTE_NAME, types);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }
}
