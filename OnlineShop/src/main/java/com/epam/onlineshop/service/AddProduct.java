/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.service;

import com.epam.onlineshop.dao.DaoException;
import com.epam.onlineshop.dao.impl.ProductDao;
import com.epam.onlineshop.dao.impl.ProductTypeDao;
import com.epam.onlineshop.domain.Product;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author notepad
 */
public class AddProduct {

    private final static ProductDao productDao = new ProductDao();
    private final static ProductTypeDao productTypeDao = new ProductTypeDao();
    private final static String LOCALE_SESSION_ATTRIBUTE_NAME = "locale";
    private final static String EN_LOCALE = "en";
    private final static String RU_LOCALE = "ru";
    private final static int PARAMS_NOT_ADDED = 0;
    private final static String WARNING_MESSAGE_ATTRIBUTE_NAME = "warningMessage";
    private final static String MESSAGE_TEXT_EN = ", but without any params";
    private final static String MESSAGE_TEXT_RU = ", но без характеристик";

    public static int addProduct(String[] paramsNames, String[] paramsValues, String productType, String productName, int productPrice, HttpServletRequest request) throws ServiceException {
        int idOfType = 0;
        HttpSession session = request.getSession();
        int resultOfAddingParams = 0;
        int resultOfAddingProduct = 0;
        try {
            idOfType = productTypeDao.getIdTypeByName(productType);
            Product product = new Product(idOfType, productName, productPrice);
            resultOfAddingProduct = productDao.insertProduct(product);
            resultOfAddingParams = AddProductParams.addParams(paramsNames, paramsValues, product);
            if (resultOfAddingParams == PARAMS_NOT_ADDED) {
                if (session.getAttribute(LOCALE_SESSION_ATTRIBUTE_NAME) == null) {
                    request.setAttribute(WARNING_MESSAGE_ATTRIBUTE_NAME, MESSAGE_TEXT_EN);
                } else if (session.getAttribute(LOCALE_SESSION_ATTRIBUTE_NAME).equals(EN_LOCALE)) {
                    request.setAttribute(WARNING_MESSAGE_ATTRIBUTE_NAME, MESSAGE_TEXT_EN);
                } else if (session.getAttribute(LOCALE_SESSION_ATTRIBUTE_NAME).equals(RU_LOCALE)) {
                    request.setAttribute(WARNING_MESSAGE_ATTRIBUTE_NAME, MESSAGE_TEXT_RU);
                }
            }
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
        return resultOfAddingProduct;
    }
}
