/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.service;

import com.epam.onlineshop.dao.DaoException;
import com.epam.onlineshop.dao.impl.OrderDao;
import com.epam.onlineshop.dao.impl.ProductDao;
import com.epam.onlineshop.dao.impl.ProductParamDao;
import com.epam.onlineshop.domain.Product;
import com.epam.onlineshop.domain.ProductParam;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author notepad
 */
public class CommitOrder {

    private final static ProductDao productDao = new ProductDao();
    private final static OrderDao orderDao = new OrderDao();
    private final static ProductParamDao productParamDao = new ProductParamDao();

    public static int commit(List<Product> products, Date dateOfOrder, int idUser) throws ServiceException {
        int totalPrice = 0;
        int resultOfCommiting = 0;
        int idOrder = new Random().nextInt(10000);
        List<ProductParam> params = null;
        int productId = 0;
        List<Product> productsForInsert = new ArrayList<Product>();
        int resultOfInsertingParams = 0;
        int resultOfInsertingProduct = 0;
        for (Product product : products) {
            totalPrice += product.getPrice();
        }
        try {
            resultOfCommiting = orderDao.insertOrder(idUser, dateOfOrder, totalPrice, idOrder);
            for (Product product : products) {
                productDao.setIdOrder(idOrder, product.getId());
                if (!productsForInsert.contains(product)) {
                    productsForInsert.add(product);
                    resultOfInsertingProduct = productDao.insertProduct(product);
                    if (resultOfInsertingProduct == 1) {
                        params = productParamDao.getParamsByProductId(product.getId());
                        productId = productDao.getIdProduct(product);
                        for (ProductParam param : params) {
                            param.setIdProduct(productId);
                        }
                        resultOfInsertingParams = productParamDao.insertParams(params);
                        resultOfInsertingProduct = 0;
                    }
                }
            }
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
        return resultOfCommiting;
    }
}
