/*
 * To change this license header, choose License HeaderesultSet in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.dao.impl;

import com.epam.onlineshop.dao.DaoException;
import com.epam.onlineshop.dao.DBDao;
import com.epam.onlineshop.dao.pool.ConnectionPool;
import com.epam.onlineshop.dao.pool.ConnectionPoolException;
import com.epam.onlineshop.domain.ProductType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author notepad
 */
public class ProductTypeDao implements DBDao<ProductType> {

    private ConnectionPool connectionPool;
    private final static String SELECT_ALL_TYPES = "SELECT * FROM `productTypes`";
    private final static String GET_ID_TYPE = "SELECT `id` FROM `productTypes` WHERE `name` = ? ";
    private final static String ID = "id";
    private final static String NAME = "name";
/**
 * It get list of all objects from table productTypes
 * @return List<ProductType>
 * @throws DaoException 
 */
    @Override
    public List<ProductType> getListOfEntities() throws DaoException {
        List<ProductType> types = new ArrayList<ProductType>();
        ProductType type = null;
        Statement statement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        try {
            connectionPool = ConnectionPool.getInstance();
            connection = connectionPool.takeConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SELECT_ALL_TYPES);
            while (resultSet.next()) {
                type = new ProductType();
                type.setId(resultSet.getInt(ID));
                type.setName(resultSet.getString(NAME));
                types.add(type);
                type = null;
            }
            return types;
        } catch (SQLException | ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connectionPool.closeConnection(connection, statement, resultSet);
            } catch (ConnectionPoolException ex) {
                throw new DaoException(ex);
            }
        }
    }
/**
 * 
 * @param productTypeName
 * It take a connection from database and get information about id of type from it/
 * @return idOfType
 * @throws DaoException 
 */
    public int getIdTypeByName(String productTypeName) throws DaoException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        int idOfType = 0;
        try {
            connectionPool = ConnectionPool.getInstance();
            connection = connectionPool.takeConnection();
            preparedStatement = connection.prepareStatement(GET_ID_TYPE);
            preparedStatement.setString(1, productTypeName);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                idOfType = resultSet.getInt(ID);
            }
            return idOfType;
        } catch (SQLException | ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connectionPool.closeConnection(connection, preparedStatement, resultSet);
            } catch (ConnectionPoolException ex) {
                throw new DaoException(ex);
            }
        }
    }

    @Override
    public int deleteEntityById(int id) throws DaoException{
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
