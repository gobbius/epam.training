/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.dao.impl;

import com.epam.onlineshop.dao.pool.ConnectionPoolException;
import com.epam.onlineshop.dao.pool.ConnectionPool;
import com.epam.onlineshop.dao.DaoException;
import com.epam.onlineshop.dao.DBDao;
import com.epam.onlineshop.domain.Admin;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author notepad
 */
public class AdminDao implements DBDao<Admin> {

    private final static String GET_ADMIN = "SELECT * FROM `admins` WHERE login = ? AND password = ?";
    public final static String ID = "id";
    public final static String LOGIN = "login";
    public final static String PASSWORD = "password";
    public final static String NAME = "name";
    private ConnectionPool connectionPool;

    @Override
    public List<Admin> getListOfEntities() throws DaoException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Admin getAdmin(String login, String password) throws DaoException {
        Admin admin = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            connectionPool = ConnectionPool.getInstance();
            connection = connectionPool.takeConnection();
            preparedStatement = connection.prepareStatement(GET_ADMIN);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, Integer.toString(password.hashCode()));
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                admin = new Admin();
                admin.setId(rs.getInt(ID));
                admin.setName(rs.getString(NAME));
                admin.setLogin(rs.getString(LOGIN));
                admin.setPassword(rs.getString(PASSWORD));
            }
            return admin;
        } catch (SQLException | ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connectionPool.closeConnection(connection, preparedStatement, rs);
            } catch (ConnectionPoolException ex) {
                throw new DaoException(ex);
            }
        }
    }

    @Override
    public int deleteEntityById(int id) throws DaoException{
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
