/*
 * To change this license header, choose License HeaderesultSet in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.dao.impl;

import com.epam.onlineshop.dao.pool.ConnectionPoolException;
import com.epam.onlineshop.dao.pool.ConnectionPool;
import com.epam.onlineshop.dao.DaoException;
import com.epam.onlineshop.dao.DBDao;
import com.epam.onlineshop.domain.User;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author notepad
 */
public class UserDao implements DBDao<User> {
    private ConnectionPool connectionPool;
    private final static String INSERT_INTO_USER = "INSERT INTO `online_shop`.`users` (`name`, `lastName`, `login`, `password`, `phoneN`, `adress`, `mail`, `cardN`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    private final static String SELECT_ALL_USERS = "SELECT * FROM `users`";
    private final static String GET_USER = "SELECT * FROM `users` WHERE login = ? AND password = ?";
    private final static String ID = "id";
    private final static String NAME = "name";
    private final static String LAST_NAME = "lastName";
    private final static String LOGIN = "login";
    private final static String PASSWORD = "password";
    private final static String PHONE_NUMBER = "phoneN";
    private final static String ADRESS = "adress";
    private final static String MAIL = "mail";
    private final static String CARD_NUMBER = "cardN";
/**
 * it take list of all objects from table users
 * @return List<User>
 * @throws DaoException 
 */
    @Override
    public List<User> getListOfEntities() throws DaoException {
        User user = null;
        List<User> useresultSet = new ArrayList<User>();
        Statement statement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connectionPool = ConnectionPool.getInstance();
            connection = connectionPool.takeConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SELECT_ALL_USERS);
            while (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getInt(ID));
                user.setName(resultSet.getString(NAME));
                user.setLastName(resultSet.getString(LAST_NAME));
                user.setLogin(resultSet.getString(LOGIN));
                user.setPassword(resultSet.getString(PASSWORD));
                user.setPhoneNumber(resultSet.getInt(PHONE_NUMBER));
                user.setAdress(resultSet.getString(ADRESS));
                user.setMail(resultSet.getString(MAIL));
                user.setCardNumber(resultSet.getInt(CARD_NUMBER));
                useresultSet.add(user);
                user = null;
            }
            return useresultSet;
        } catch (SQLException | ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connectionPool.closeConnection(connection, statement, resultSet);
            } catch (ConnectionPoolException ex) {
                throw new DaoException(ex);
            }
        }
    }
/**
 * it insert user into table users from online_shop database
 * @param user
 * @return
 * @throws DaoException 
 */
    public int insertUser(User user) throws DaoException {
        PreparedStatement preparedStatement = null;
        int resultOfInserting = 0;
        Connection connection = null;
        try {
            connectionPool = ConnectionPool.getInstance();
            connection = connectionPool.takeConnection();
            preparedStatement = connection.prepareStatement(INSERT_INTO_USER);
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getLogin());
            preparedStatement.setString(4, Integer.toString(user.getPassword().hashCode()));
            preparedStatement.setInt(5, user.getPhoneNumber());
            preparedStatement.setString(6, user.getAdress());
            preparedStatement.setString(7, user.getMail());
            preparedStatement.setInt(8, user.getCardNumber());
            resultOfInserting = preparedStatement.executeUpdate();
        } catch (SQLException | ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connectionPool.closeConnection(connection, preparedStatement);
            } catch (ConnectionPoolException ex) {
                throw new DaoException(ex);
            }
        }
        return resultOfInserting;
    }
/**
 * it getting user from table users from online_shop database
 * @param login
 * @param password
 * @return
 * @throws DaoException 
 */
    public User getUser(String login, String password) throws DaoException {
        User user = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connectionPool = ConnectionPool.getInstance();
            connection = connectionPool.takeConnection();
            preparedStatement = connection.prepareStatement(GET_USER);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, Integer.toString(password.hashCode()));
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getInt(ID));
                user.setName(resultSet.getString(NAME));
                user.setLastName(resultSet.getString(LAST_NAME));
                user.setLogin(resultSet.getString(LOGIN));
                user.setPassword(resultSet.getString(PASSWORD));
                user.setAdress(resultSet.getString(ADRESS));
                user.setPhoneNumber(resultSet.getInt(PHONE_NUMBER));
                user.setMail(resultSet.getString(MAIL));
                user.setCardNumber(resultSet.getInt(CARD_NUMBER));
            }
            return user;
        } catch (SQLException | ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connectionPool.closeConnection(connection, preparedStatement, resultSet);
            } catch (ConnectionPoolException ex) {
                throw new DaoException(ex);
            }
        }
    }

    @Override
    public int deleteEntityById(int id) throws DaoException{
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
