/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.dao.impl;

import com.epam.onlineshop.dao.DaoException;
import com.epam.onlineshop.dao.DBDao;
import com.epam.onlineshop.dao.pool.ConnectionPool;
import com.epam.onlineshop.dao.pool.ConnectionPoolException;
import com.epam.onlineshop.domain.Order;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author notepad
 */
public class OrderDao implements DBDao<Order> {

    private ConnectionPool connectionPool;
    private final static String INSERT_ORDER = "INSERT INTO `orders` (`id`, `idUser`, `date`, `totalPrice`, `paid`) VALUES (?, ?, ?, ?, ?);";
    private final static String GET_ORDERS_BY_USER_ID_AND_DATE = "SELECT * FROM `orders` WHERE `idUser` = ? AND `date` = ? AND `paid`= 0";
    private final static String GET_ORDERS_BY_USER_ID = "SELECT * FROM `orders` WHERE `idUser` = ? AND `paid`= 0";
    private final static String DELETE_ORDER_BY_ID = "DELETE FROM `orders` WHERE `id` = ?";
    private final static String UPDATE_PAID_COLUMN = "UPDATE `orders` SET `paid` = '1' WHERE `id` = ?";
    private final static String ID_ORDER = "id";
    private final static String ORDER_DATE = "date";
    private final static String TOTAL_PRICE = "totalPrice";

    @Override
    public List<Order> getListOfEntities() throws DaoException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int deleteEntityById(int id) throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        int resultOfDeleting = 0;
        try {
            connectionPool = ConnectionPool.getInstance();
            connection = connectionPool.takeConnection();
            preparedStatement = connection.prepareStatement(DELETE_ORDER_BY_ID);
            preparedStatement.setInt(1, id);
            resultOfDeleting = preparedStatement.executeUpdate();
            return resultOfDeleting;
        } catch (SQLException | ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connectionPool.closeConnection(connection, preparedStatement);
            } catch (ConnectionPoolException ex) {
                throw new DaoException(ex);
            }
        }
    }

    public int insertOrder(int idUser, Date dateOfOrder, int totalPrice, int idOrder) throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        int resultOfInserting = 0;
        try {
            connectionPool = ConnectionPool.getInstance();
            connection = connectionPool.takeConnection();
            preparedStatement = connection.prepareStatement(INSERT_ORDER);
            preparedStatement.setInt(1, idOrder);
            preparedStatement.setInt(2, idUser);
            preparedStatement.setDate(3, dateOfOrder);
            preparedStatement.setInt(4, totalPrice);
            preparedStatement.setBoolean(5, false);
            resultOfInserting = preparedStatement.executeUpdate();
        } catch (ConnectionPoolException | SQLException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connectionPool.closeConnection(connection, preparedStatement);
            } catch (ConnectionPoolException ex) {
                throw new DaoException(ex);
            }
        }
        return resultOfInserting;
    }
/**
 * it getting orders by user id and date of order
 * @param userId
 * @param date
 * @return List<Order>
 * @throws DaoException 
 */
    public List<Order> getOrdersByUserIdAndDate(int userId, Date date) throws DaoException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Order> orders = new ArrayList<Order>();
        Order order = null;
        try {
            connectionPool = ConnectionPool.getInstance();
            connection = connectionPool.takeConnection();
            preparedStatement = connection.prepareStatement(GET_ORDERS_BY_USER_ID_AND_DATE);
            preparedStatement.setInt(1, userId);
            preparedStatement.setDate(2, date);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                order = new Order();
                order.setId(resultSet.getInt(ID_ORDER));
                order.setIdUser(userId);
                order.setDate(resultSet.getDate(ORDER_DATE));
                order.setTotalPrice(resultSet.getInt(TOTAL_PRICE));
                orders.add(order);
                order = null;
            }
        } catch (ConnectionPoolException | SQLException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connectionPool.closeConnection(connection, preparedStatement, resultSet);
            } catch (ConnectionPoolException ex) {
                throw new DaoException(ex);
            }
        }
        return orders;
    }
    /**
     * it getting orders by user id
     * @param userId
     * @return List<Order>
     * @throws DaoException 
     */
    public List<Order> getOrdersByUserId(int userId) throws DaoException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Order> orders = new ArrayList<Order>();
        Order order = null;
        try {
            connectionPool = ConnectionPool.getInstance();
            connection = connectionPool.takeConnection();
            preparedStatement = connection.prepareStatement(GET_ORDERS_BY_USER_ID);
            preparedStatement.setInt(1, userId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                order = new Order();
                order.setId(resultSet.getInt(ID_ORDER));
                order.setIdUser(userId);
                order.setDate(resultSet.getDate(ORDER_DATE));
                order.setTotalPrice(resultSet.getInt(TOTAL_PRICE));
                orders.add(order);
                order = null;
            }
        } catch (ConnectionPoolException | SQLException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connectionPool.closeConnection(connection, preparedStatement, resultSet);
            } catch (ConnectionPoolException ex) {
                throw new DaoException(ex);
            }
        }
        return orders;
    }

    public void setPaid(int orderId) throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        int resultOfSetting = 0;
        try {
            connectionPool = ConnectionPool.getInstance();
            connection = connectionPool.takeConnection();
            preparedStatement = connection.prepareStatement(UPDATE_PAID_COLUMN);
            preparedStatement.setInt(1, orderId);
            resultOfSetting = preparedStatement.executeUpdate();
        } catch (ConnectionPoolException | SQLException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connectionPool.closeConnection(connection, preparedStatement);
            } catch (ConnectionPoolException ex) {
                throw new DaoException(ex);
            }
        }
    }
}
