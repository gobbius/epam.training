/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.dao.pool;

import com.epam.onlineshop.exception.ProjectException;

/**
 *
 * @author notepad
 */
public class ConnectionPoolException extends ProjectException {

    public ConnectionPoolException() {
    }

    public ConnectionPoolException(Throwable thrwbl) {
        super(thrwbl);
    }

}
