/*
 * To change this license header, choose License HeaderesultSet in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.dao.impl;

import com.epam.onlineshop.dao.DaoException;
import com.epam.onlineshop.dao.DBDao;
import com.epam.onlineshop.dao.pool.ConnectionPool;
import com.epam.onlineshop.dao.pool.ConnectionPoolException;
import com.epam.onlineshop.domain.ProductParam;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author notepad
 */
public class ProductParamDao implements DBDao<ProductParam> {

    private ConnectionPool connectionPool;
    private final static String ID = "id";
    private final static String PRODUCT_ID = "idProduct";
    private final static String NAME = "name";
    private final static String VALUE = "value";
    private final static String UPDATE_PRODUCT_PARAM = "UPDATE `productParams` SET `name` =?, `value` =?  WHERE `id` =? ";
    private final static String DELETE_PARAM_BY_ID = "DELETE FROM `productParams` WHERE `id` = ?";
    private final static String GET_PARAMS_BY_PRODUCT_ID = "SELECT `id`,`name`,`value` FROM `productParams` WHERE `idProduct`= ? ";
    private final static String INSERT_INTO_PRODUCT_PARAMS = "INSERT INTO `productParams` (`idProduct`, `name`, `value`) VALUES (?, ?, ?)";

    @Override
    public List<ProductParam> getListOfEntities() throws DaoException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int insertParams(List<ProductParam> params) throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        int resultOfInserting = 0;
        try {
            connectionPool = ConnectionPool.getInstance();
            connection = connectionPool.takeConnection();
            preparedStatement = connection.prepareStatement(INSERT_INTO_PRODUCT_PARAMS);
            for (ProductParam param : params) {
                preparedStatement.setInt(1, param.getIdProduct());
                preparedStatement.setString(2, param.getName());
                preparedStatement.setString(3, param.getValue());
                resultOfInserting = preparedStatement.executeUpdate();
                preparedStatement.clearParameters();
            }
        } catch (SQLException | ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connectionPool.closeConnection(connection, preparedStatement);
            } catch (ConnectionPoolException ex) {
                throw new DaoException(ex);
            }
        }
        return resultOfInserting;
    }

    public int updateProductParam(List<ProductParam> params) throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        int resultOfUpdating = 0;
        try {
            connectionPool = ConnectionPool.getInstance();
            connection = connectionPool.takeConnection();
            preparedStatement = connection.prepareStatement(UPDATE_PRODUCT_PARAM);
            for (ProductParam param : params) {
                preparedStatement.setString(1, param.getName());
                preparedStatement.setString(2, param.getValue());
                preparedStatement.setInt(3, param.getId());
                resultOfUpdating = preparedStatement.executeUpdate();
                preparedStatement.clearParameters();
            }
        } catch (SQLException | ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connectionPool.closeConnection(connection, preparedStatement);
            } catch (ConnectionPoolException ex) {
                throw new DaoException(ex);
            }
        }
        return resultOfUpdating;
    }

    /**
     * it getting params of product by its id from productParams table from
     * online_shop database
     *
     * @param productId
     * @return List<ProductParam>
     * @throws DaoException
     */
    public List<ProductParam> getParamsByProductId(int productId) throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        ProductParam param = null;
        List<ProductParam> params = new ArrayList<ProductParam>();
        try {
            connectionPool = ConnectionPool.getInstance();
            connection = connectionPool.takeConnection();
            preparedStatement = connection.prepareStatement(GET_PARAMS_BY_PRODUCT_ID);
            preparedStatement.setInt(1, productId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                param = new ProductParam();
                param.setId(resultSet.getInt(ID));
                param.setIdProduct(productId);
                param.setName(resultSet.getString(NAME));
                param.setValue(resultSet.getString(VALUE));
                params.add(param);
                param = null;
            }
            return params;
        } catch (SQLException | ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connectionPool.closeConnection(connection, preparedStatement, resultSet);
            } catch (ConnectionPoolException ex) {
                throw new DaoException(ex);
            }
        }
    }

    /**
     * it remove all params of product by product id from productParams table
     * from online_shop database
     *
     * @param id of product
     * @return resultOfDeleting
     * @throws DaoException
     */
    @Override
    public int deleteEntityById(int id) throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        int resultOfDeleting = 0;
        try {
            connectionPool = ConnectionPool.getInstance();
            connection = connectionPool.takeConnection();
            preparedStatement = connection.prepareStatement(DELETE_PARAM_BY_ID);
            preparedStatement.setInt(1, id);
            resultOfDeleting = preparedStatement.executeUpdate();
            return resultOfDeleting;
        } catch (SQLException | ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connectionPool.closeConnection(connection, preparedStatement);
            } catch (ConnectionPoolException ex) {
                throw new DaoException(ex);
            }
        }
    }
}
