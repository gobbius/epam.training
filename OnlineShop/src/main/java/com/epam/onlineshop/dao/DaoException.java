/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.dao;

import com.epam.onlineshop.exception.ProjectException;

/**
 *
 * @author notepad
 */
public class DaoException extends ProjectException {

    public DaoException() {
    }

    public DaoException(Throwable thrwbl) {
        super(thrwbl);
    }

}
