/*
 * To change this license header, choose License HeaderesultSet in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.dao.impl;

import com.epam.onlineshop.dao.DaoException;
import com.epam.onlineshop.dao.DBDao;
import com.epam.onlineshop.dao.pool.ConnectionPool;
import com.epam.onlineshop.dao.pool.ConnectionPoolException;
import com.epam.onlineshop.domain.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author notepad
 */
public class ProductDao implements DBDao<Product> {

    private ConnectionPool connectionPool;
    private final static String ASCENDING_SORT = "ASC";
    private final static String DESCENDING_SORT = "DESC";
    private final static String UPDATE_PRODUCT = "UPDATE `products` SET `idType` =?, `name` =?, `price` =?  WHERE `id` = ? ";
    private final static String UPDATE_ID_ORDER = "UPDATE `products` SET `idOrder` = ? WHERE `id` = ?";
    private final static String SELECT_SORTED_BY_PRICE_AND_TYPE_PRODUCTS_DESC = "SELECT * FROM `products` WHERE `idType` =? AND `idOrder` IS NULL ORDER BY `price` DESC";
    private final static String SELECT_SORTED_BY_PRICE_AND_TYPE_PRODUCTS_ASC = "SELECT * FROM `products` WHERE `idType` =? AND `idOrder` IS NULL ORDER BY `price` ASC";
    private final static String SELECT_SORTED_BY_PRICE_PRODUCTS_DESC = "SELECT * FROM `products` WHERE `idOrder` IS NULL ORDER BY `price` DESC";
    private final static String SELECT_SORTED_BY_PRICE_PRODUCTS_ASC = "SELECT * FROM `products` WHERE `idOrder` IS NULL ORDER BY `price` ASC";
    private final static String DELETE_PRODUCT_BY_ID = "DELETE FROM `products` WHERE `id` = ? AND `idOrder` IS NULL";
    private final static String GET_PRODUCT_BY_ID = "SELECT * FROM `products` WHERE `id` = ? AND `idOrder` IS NULL";
    private final static String GET_PRODUCT_BY_NAME = "SELECT * FROM `products` WHERE `name` LIKE ? AND `idOrder` IS NULL";
    private final static String INSERT_INTO_PRODUCT = "INSERT INTO `products` (`idType`, `name`, `price`) VALUES ( ?, ?, ?);";
    private final static String GET_ID_PRODUCT = "SELECT `id` FROM `products` WHERE `idType` = ? AND `name` = ? AND `price` = ? AND `idOrder` IS NULL";
    private final static String SELECT_ALL_PRODUCTS = "SELECT * FROM `products` WHERE `idOrder` IS NULL";
    private final static String ID = "id";
    private final static String NAME = "name";
    private final static String PRICE = "price";
    private final static String ID_TYPE = "idType";
    private final static String ID_ORDER = "idOrder";

    @Override
    public List<Product> getListOfEntities() throws DaoException {
        Product product = null;
        List<Product> products = new ArrayList<Product>();
        Connection connection = null;
        Statement st = null;
        ResultSet resultSet = null;
        try {
            connectionPool = ConnectionPool.getInstance();
            connection = connectionPool.takeConnection();
            st = connection.createStatement();
            resultSet = st.executeQuery(SELECT_ALL_PRODUCTS);
            while (resultSet.next()) {
                product = new Product();
                product.setId(resultSet.getInt(ID));
                product.setIdOrder(resultSet.getInt(ID_ORDER));
                product.setIdType(resultSet.getInt(ID_TYPE));
                product.setName(resultSet.getString(NAME));
                product.setPrice(resultSet.getInt(PRICE));
                products.add(product);
                product = null;
            }
            return products;
        } catch (SQLException | ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connectionPool.closeConnection(connection, st, resultSet);
            } catch (ConnectionPoolException ex) {
                throw new DaoException(ex);
            }
        }
    }

    public int insertProduct(Product product) throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        int resultOfInserting = 0;
        try {
            connectionPool = ConnectionPool.getInstance();
            connection = connectionPool.takeConnection();
            preparedStatement = connection.prepareStatement(INSERT_INTO_PRODUCT);
            preparedStatement.setInt(1, product.getIdType());
            preparedStatement.setString(2, product.getName());
            preparedStatement.setInt(3, product.getPrice());
            resultOfInserting = preparedStatement.executeUpdate();
        } catch (SQLException | ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connectionPool.closeConnection(connection, preparedStatement);
            } catch (ConnectionPoolException ex) {
                throw new DaoException(ex);
            }
        }
        return resultOfInserting;
    }

    /**
     * it getting products id by its reference
     *
     * @param product
     * @return id of product
     * @throws DaoException
     */
    public int getIdProduct(Product product) throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        int idOfProduct = 0;
        try {
            connectionPool = ConnectionPool.getInstance();
            connection = connectionPool.takeConnection();
            preparedStatement = connection.prepareStatement(GET_ID_PRODUCT);
            preparedStatement.setInt(1, product.getIdType());
            preparedStatement.setString(2, product.getName());
            preparedStatement.setInt(3, product.getPrice());
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                idOfProduct = resultSet.getInt(ID);
            }
            return idOfProduct;
        } catch (SQLException | ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connectionPool.closeConnection(connection, preparedStatement, resultSet);
            } catch (ConnectionPoolException ex) {
                throw new DaoException(ex);
            }
        }
    }

    /**
     * it getting products by its id
     *
     * @param productId
     * @return
     * @throws DaoException
     */
    public Product getProductById(int productId) throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        Product product = null;
        try {
            connectionPool = ConnectionPool.getInstance();
            connection = connectionPool.takeConnection();
            preparedStatement = connection.prepareStatement(GET_PRODUCT_BY_ID);
            preparedStatement.setInt(1, productId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                product = new Product();
                product.setId(resultSet.getInt(ID));
                product.setIdType(resultSet.getInt(ID_TYPE));
                product.setName(resultSet.getString(NAME));
                product.setPrice(resultSet.getInt(PRICE));
            }
            return product;
        } catch (SQLException | ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connectionPool.closeConnection(connection, preparedStatement, resultSet);
            } catch (ConnectionPoolException ex) {
                throw new DaoException(ex);
            }
        }
    }

    /**
     * it deleting products from database by product id
     *
     * @param id
     * @return
     * @throws DaoException
     */
    @Override
    public int deleteEntityById(int id) throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        int resultOfDeleting = 0;
        try {
            connectionPool = ConnectionPool.getInstance();
            connection = connectionPool.takeConnection();
            preparedStatement = connection.prepareStatement(DELETE_PRODUCT_BY_ID);
            preparedStatement.setInt(1, id);
            resultOfDeleting = preparedStatement.executeUpdate();
            return resultOfDeleting;
        } catch (SQLException | ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connectionPool.closeConnection(connection, preparedStatement);
            } catch (ConnectionPoolException ex) {
                throw new DaoException(ex);
            }
        }
    }

    /**
     * it getting products by name
     *
     * @param productName
     * @return
     * @throws DaoException
     */
    public List<Product> getProductsByName(String productName) throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        List<Product> products = new ArrayList<Product>();
        Product product = null;
        try {
            connectionPool = ConnectionPool.getInstance();
            connection = connectionPool.takeConnection();
            preparedStatement = connection.prepareStatement(GET_PRODUCT_BY_NAME);
            preparedStatement.setString(1, productName);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                product = new Product();
                product.setId(resultSet.getInt(ID));
                product.setIdType(resultSet.getInt(ID_TYPE));
                product.setName(resultSet.getString(NAME));
                product.setPrice(resultSet.getInt(PRICE));
                products.add(product);
                product = null;
            }
            return products;
        } catch (SQLException | ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connectionPool.closeConnection(connection, preparedStatement, resultSet);
            } catch (ConnectionPoolException ex) {
                throw new DaoException(ex);
            }
        }
    }

    public int updateProduct(Product product) throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        int resultOfUpdating = 0;
        try {
            connectionPool = ConnectionPool.getInstance();
            connection = connectionPool.takeConnection();
            preparedStatement = connection.prepareStatement(UPDATE_PRODUCT);
            preparedStatement.setInt(1, product.getIdType());
            preparedStatement.setString(2, product.getName());
            preparedStatement.setInt(3, product.getPrice());
            preparedStatement.setInt(4, product.getId());
            resultOfUpdating = preparedStatement.executeUpdate();
        } catch (SQLException | ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connectionPool.closeConnection(connection, preparedStatement);
            } catch (ConnectionPoolException ex) {
                throw new DaoException(ex);
            }
        }
        return resultOfUpdating;
    }

    /**
     * it sorting products by type and ASC\DESC or only by ASC\DESC
     *
     * @param typeId
     * @param priceSortParam
     * @return
     * @throws DaoException
     */
    public List<Product> getProductsSortedByTypeAndPrice(int typeId, String priceSortParam) throws DaoException {
        PreparedStatement preparedStatement = null;
        Statement statement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        List<Product> products = new ArrayList<Product>();
        Product product = null;
        String transactSqlRequest = null;
        try {
            connectionPool = ConnectionPool.getInstance();
            connection = connectionPool.takeConnection();
            if (priceSortParam.equals(ASCENDING_SORT) && typeId != 0) {
                transactSqlRequest = SELECT_SORTED_BY_PRICE_AND_TYPE_PRODUCTS_ASC;
                preparedStatement = connection.prepareStatement(transactSqlRequest);
                preparedStatement.setInt(1, typeId);
                resultSet = preparedStatement.executeQuery();
            } else if (!priceSortParam.equals(ASCENDING_SORT) && typeId != 0) {
                transactSqlRequest = SELECT_SORTED_BY_PRICE_AND_TYPE_PRODUCTS_DESC;
                preparedStatement = connection.prepareStatement(transactSqlRequest);
                preparedStatement.setInt(1, typeId);
                resultSet = preparedStatement.executeQuery();
            } else if (priceSortParam.equals(ASCENDING_SORT) && typeId == 0) {
                transactSqlRequest = SELECT_SORTED_BY_PRICE_PRODUCTS_ASC;
                statement = connection.createStatement();
                resultSet = statement.executeQuery(transactSqlRequest);
            } else if (!priceSortParam.equals(ASCENDING_SORT) && typeId == 0) {
                transactSqlRequest = SELECT_SORTED_BY_PRICE_PRODUCTS_DESC;
                statement = connection.createStatement();
                resultSet = statement.executeQuery(transactSqlRequest);
            }
            while (resultSet.next()) {
                product = new Product();
                product.setId(resultSet.getInt(ID));
                product.setIdType(resultSet.getInt(ID_TYPE));
                product.setName(resultSet.getString(NAME));
                product.setPrice(resultSet.getInt(PRICE));
                products.add(product);
                product = null;
            }
            return products;
        } catch (SQLException | ConnectionPoolException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                if (statement != null) {
                    connectionPool.closeConnection(connection, statement, resultSet);
                } else {
                    connectionPool.closeConnection(connection, preparedStatement, resultSet);
                }
            } catch (ConnectionPoolException ex) {
                throw new DaoException(ex);
            }
        }
    }

    /**
     * it setting id of order to the choosen products when user commiting his
     * order
     *
     * @param idOrder
     * @param idProduct
     * @throws DaoException
     */
    public void setIdOrder(int idOrder, int idProduct) throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        int resultOfSetting = 0;
        try {
            connectionPool = ConnectionPool.getInstance();
            connection = connectionPool.takeConnection();
            preparedStatement = connection.prepareStatement(UPDATE_ID_ORDER);
            preparedStatement.setInt(1, idOrder);
            preparedStatement.setInt(2, idProduct);
            resultOfSetting = preparedStatement.executeUpdate();
        } catch (ConnectionPoolException | SQLException ex) {
            throw new DaoException(ex);
        } finally {
            try {
                connectionPool.closeConnection(connection, preparedStatement);
            } catch (ConnectionPoolException ex) {
                throw new DaoException(ex);
            }
        }
    }
}
