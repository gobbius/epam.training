/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.manager;

import java.util.ResourceBundle;

/**
 *
 * @author notepad
 */
public class PageManager {

    public final static String ADMIN_MENU_PAGE = "admin.menu";
    public final static String INCORRECT_PASS = "incorrect.password";
    public final static String ERROR_PAGE = "error.page";
    public final static String REGISTRATION_PAGE = "registration.page";
    public final static String INDEX_PAGE = "index.page";
    public final static String ADD_PRODUCT_PAGE = "add.product.page";
    public final static String ADMIN_CATALOG_PAGE = "admin.catalog.page";
    public final static String ADMIN_PRODUCT_PAGE = "admin.product.page";
    public final static String USER_PRODUCT_PAGE = "user.product.page";
    public final static String CART_PAGE = "cart.page";
    public final static String PAYMENT_PAGE = "payment.page";
    public final static String USER_CATALOG_PAGE = "user.catalog.page";
    public final static String EDIT_PRODUCT_PAGE = "admin.edit.product.page";
    private ResourceBundle bundle;
    private static final String BUNDLE_NAME = "pages";

    private final static PageManager instance = new PageManager();

    private PageManager() {
        bundle = ResourceBundle.getBundle(BUNDLE_NAME);
    }

    public static PageManager getInstance() {
        return instance;
    }

    public String getPage(String pageName) {
        return (String) bundle.getObject(pageName);
    }

}
