/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.manager;

import java.util.ResourceBundle;

/**
 *
 * @author notepad
 */
public class RegexpManager {

    public final static String REGEXP_FOR_TEXT = "regexp.for.text";
    public final static String REGEXP_FOR_LOGIN_AND_PASSWORD = "regexp.for.login.and.password";
    public final static String REGEXP_FOR_PHONE_NUMBER = "regexp.for.phone.number";
    public final static String REGEXP_FOR_MAIl = "regexp.for.mail";
    public final static String REGEXP_FOR_CARD_NUMBER = "regexp.for.card.number";
    public final static String REGEXP_FOR_PRODUCT_NAME = "regexp.for.product.name";
    public final static String REGEXP_FOR_PRODUCT_PRICE = "regexp.for.product.price";
    public final static String REGEXP_FOR_PARAM_NAME = "regexp.for.param.name";
    public final static String REGEXP_FOR_PARAM_VALUE ="regexp.for.param.value";
    private ResourceBundle bundle;
    private static final String BUNDLE_NAME = "regexp";

    private final static RegexpManager instance = new RegexpManager();

    private RegexpManager() {
        bundle = ResourceBundle.getBundle(BUNDLE_NAME);
    }

    public static RegexpManager getInstance() {
        return instance;
    }

    public String getRegexp(String regexpName) {
        return (String) bundle.getObject(regexpName);
    }
}
