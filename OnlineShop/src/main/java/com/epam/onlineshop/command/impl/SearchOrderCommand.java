/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.command.impl;

import com.epam.onlineshop.command.CommandException;
import com.epam.onlineshop.command.ICommand;
import com.epam.onlineshop.manager.PageManager;
import com.epam.onlineshop.service.GetOrdersByUserIdAndDate;
import com.epam.onlineshop.service.ServiceException;
import java.sql.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author notepad
 */
public class SearchOrderCommand implements ICommand {

    private final static String ID_USER_ATTRIBUTE_NAME = "userId";
    private final static String DATE_OF_ORDERS = "date";
    private final static String PAYMENT_PAGE = PageManager.getInstance().getPage(PageManager.PAYMENT_PAGE);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String forwardPage = null;
        HttpSession session = request.getSession();
        int userId = (int) session.getAttribute(ID_USER_ATTRIBUTE_NAME);
        Date dateOfOrders = Date.valueOf(request.getParameter(DATE_OF_ORDERS));
        try {
            GetOrdersByUserIdAndDate.getOrders(session, dateOfOrders, userId);
        } catch (ServiceException ex) {
            throw new CommandException(ex);
        }
        forwardPage = PAYMENT_PAGE;
        return forwardPage;
    }

}
