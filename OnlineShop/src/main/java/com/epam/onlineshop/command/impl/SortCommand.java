/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.command.impl;

import com.epam.onlineshop.command.CommandException;
import com.epam.onlineshop.command.ICommand;
import com.epam.onlineshop.service.GetProductsByTypeAndSortedByPrice;
import com.epam.onlineshop.service.ServiceException;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author notepad
 */
public class SortCommand implements ICommand{
private final static String TYPE_ATTRIBUTE_NAME = "type";
private final static String PRICE_ATTRIBUTE_NAME = "price";
private final static RedirectCommand redirectCommand = new RedirectCommand();
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String typeName = request.getParameter(TYPE_ATTRIBUTE_NAME);
        String priceParam = request.getParameter(PRICE_ATTRIBUTE_NAME);
    try {
        GetProductsByTypeAndSortedByPrice.getProducts(request, typeName, priceParam);
    } catch (ServiceException ex) {
        throw new CommandException(ex);
    }
    return redirectCommand.execute(request);
    }
    
}
