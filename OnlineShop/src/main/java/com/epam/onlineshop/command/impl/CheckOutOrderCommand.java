/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.command.impl;

import com.epam.onlineshop.command.CommandException;
import com.epam.onlineshop.command.ICommand;
import com.epam.onlineshop.domain.Order;
import com.epam.onlineshop.manager.PageManager;
import com.epam.onlineshop.service.CheckOutOrder;
import com.epam.onlineshop.service.ServiceException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author notepad
 */
public class CheckOutOrderCommand implements ICommand {

    private final static String ORDERS_SESSION_ATTRIBUTE_NAME = "orders";
    private final static String ORDER_ID = "order_id";
    private final static String PAYMENT_PAGE = PageManager.getInstance().getPage(PageManager.PAYMENT_PAGE);
    private final static String RESULT_OF_CHECKING_OUT_ATTRIBUTE_NAME = "resultOfPayment";
    private final static String RESULT_OF_CHECKING_OUT_MESSAGE_EN = "Order is paid";
    private final static String RESULT_OF_CHECKING_OUT_MESSAGE_RU = "Заказ оплачен";
    private final static String LOCALE_SESSION_ATTRIBUTE_NAME = "locale";
    private final static String EN_LOCALE = "en";
    private final static String RU_LOCALE = "ru";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession();
        String forwardPage = null;
        List<Order> orders = null;
        Order order = null;
        int orderId = 0;
        orderId = Integer.parseInt(request.getParameter(ORDER_ID));
        orders = (List<Order>) session.getAttribute(ORDERS_SESSION_ATTRIBUTE_NAME);
        if (orders != null && !orders.isEmpty()) {
            try {
                for(Order ord : orders){
                    if(ord.getId()==orderId){
                        order = ord;
                    }
                }
                CheckOutOrder.checkOut(order);
                orders.remove(order);
                session.setAttribute(ORDERS_SESSION_ATTRIBUTE_NAME, orders);
            } catch (ServiceException ex) {
                throw new CommandException(ex);
            }
            if (session.getAttribute(LOCALE_SESSION_ATTRIBUTE_NAME) == null) {
                request.setAttribute(RESULT_OF_CHECKING_OUT_ATTRIBUTE_NAME, RESULT_OF_CHECKING_OUT_MESSAGE_EN);
            } else if (session.getAttribute(LOCALE_SESSION_ATTRIBUTE_NAME).equals(EN_LOCALE)) {
                request.setAttribute(RESULT_OF_CHECKING_OUT_ATTRIBUTE_NAME, RESULT_OF_CHECKING_OUT_MESSAGE_EN);
            } else if (session.getAttribute(LOCALE_SESSION_ATTRIBUTE_NAME).equals(RU_LOCALE)) {
                request.setAttribute(RESULT_OF_CHECKING_OUT_ATTRIBUTE_NAME, RESULT_OF_CHECKING_OUT_MESSAGE_RU);
            }
        }
        forwardPage = PAYMENT_PAGE;
        return forwardPage;
    }

}
