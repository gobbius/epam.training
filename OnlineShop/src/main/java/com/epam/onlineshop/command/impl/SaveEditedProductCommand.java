/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.command.impl;

import com.epam.onlineshop.command.CommandException;
import com.epam.onlineshop.command.ICommand;
import com.epam.onlineshop.manager.PageManager;
import com.epam.onlineshop.manager.RegexpManager;
import com.epam.onlineshop.service.SaveEditedProduct;
import com.epam.onlineshop.service.ServiceException;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author notepad
 */
public class SaveEditedProductCommand implements ICommand {

    private final static String PARAM_NAME_ATTRIBUTE = "param_name";
    private final static String PARAM_VALUE_ATTRIBUTE_NAME = "param_value";
    private final static String STATIC_PARAM_NAME_ATTRIBUTE = "static_param_name";
    private final static String STATIC_PARAM_VALUE_ATTRIBUTE_NAME = "static_param_value";
    private final static String PRODUCT_NAME_ATTRIBUTE = "product_name";
    private final static String PRODUCT_PRICE_ATTRIBUTE_NAME = "price";
    private final static String PRODUCT_TYPE_ATTRIBUTE_NAME = "type";
    private final static String LOCALE_SESSION_ATTRIBUTE_NAME = "locale";
    private final static String MESSAGE_ATTRIBUTE_NAME = "message";
    private final static String EXCEPTION_MESSAGE_FOR_PARAMS = "Wrong name or value of param";
    private final static String EXCEPTION_MESSAGE_FOR_PRODUCT = "Wrong name or price of product";
    private final static String COMPLETLY_ADDED_MESSAGE_EN = "Product was successfully edited";
    private final static String COMPLETLY_ADDED_MESSAGE_RU = "Товар был успешно изменен";
    private final static String COMPLETE_MESSAGE_ATTRIBUTE_NAME = "completeMessage";
    private final static String EN_LOCALE = "en";
    private final static String RU_LOCALE = "ru";
    public final static String REGEXP_FOR_PRODUCT_NAME = RegexpManager.getInstance().getRegexp(RegexpManager.REGEXP_FOR_PRODUCT_NAME);
    public final static String REGEXP_FOR_PRODUCT_PRICE = RegexpManager.getInstance().getRegexp(RegexpManager.REGEXP_FOR_PRODUCT_PRICE);
    private final static String ADMIN_PRODUCT_PAGE = PageManager.getInstance().getPage(PageManager.ADMIN_PRODUCT_PAGE);
    private final static String ERROR_PAGE = PageManager.getInstance().getPage(PageManager.ERROR_PAGE);
    private final static RedirectCommand redirectCommand = new RedirectCommand();

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String forwardPage = null;
        String[] staticParamNames = request.getParameterValues(STATIC_PARAM_NAME_ATTRIBUTE);
        String[] staticParamValues = request.getParameterValues(STATIC_PARAM_VALUE_ATTRIBUTE_NAME);
        String[] paramNames = request.getParameterValues(PARAM_NAME_ATTRIBUTE);
        String[] paramValues = request.getParameterValues(PARAM_VALUE_ATTRIBUTE_NAME);
        String productName = request.getParameter(PRODUCT_NAME_ATTRIBUTE);
        String productType = request.getParameter(PRODUCT_TYPE_ATTRIBUTE_NAME);
        int productPrice = 0;
        int resultOfSaving = 0;
        if (request.getParameter(PRODUCT_PRICE_ATTRIBUTE_NAME) != null && request.getParameter(PRODUCT_PRICE_ATTRIBUTE_NAME).matches(REGEXP_FOR_PRODUCT_PRICE)) {
            productPrice = Integer.parseInt(request.getParameter(PRODUCT_PRICE_ATTRIBUTE_NAME));
        }
        if (productName.matches(REGEXP_FOR_PRODUCT_NAME) && productPrice != 0) {
            try {
                resultOfSaving = SaveEditedProduct.save(paramNames, paramValues, staticParamNames, staticParamValues, productType, productName, productPrice, request);
            } catch (ServiceException ex) {
                throw new CommandException(ex);
            }
            if (resultOfSaving > 0) {
                if (request.getSession().getAttribute(LOCALE_SESSION_ATTRIBUTE_NAME) == null) {
                    request.setAttribute(COMPLETE_MESSAGE_ATTRIBUTE_NAME, COMPLETLY_ADDED_MESSAGE_EN);
                } else if (request.getSession().getAttribute(LOCALE_SESSION_ATTRIBUTE_NAME).equals(EN_LOCALE)) {
                    request.setAttribute(COMPLETE_MESSAGE_ATTRIBUTE_NAME, COMPLETLY_ADDED_MESSAGE_EN);
                } else if (request.getSession().getAttribute(LOCALE_SESSION_ATTRIBUTE_NAME).equals(RU_LOCALE)) {
                    request.setAttribute(COMPLETE_MESSAGE_ATTRIBUTE_NAME, COMPLETLY_ADDED_MESSAGE_RU);
                }
                forwardPage = redirectCommand.execute(request);
            } else {
                forwardPage = ERROR_PAGE;
                request.setAttribute(MESSAGE_ATTRIBUTE_NAME, EXCEPTION_MESSAGE_FOR_PARAMS);
            }
        } else {
            forwardPage = ERROR_PAGE;
            request.setAttribute(MESSAGE_ATTRIBUTE_NAME, EXCEPTION_MESSAGE_FOR_PRODUCT);
        }
        return forwardPage;
    }

}
