/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.command.impl;

import com.epam.onlineshop.command.CommandException;
import com.epam.onlineshop.command.ICommand;
import com.epam.onlineshop.domain.Product;
import com.epam.onlineshop.manager.PageManager;
import com.epam.onlineshop.service.CommitOrder;
import com.epam.onlineshop.service.ServiceException;
import java.sql.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author notepad
 */
public class CommitOrderCommand implements ICommand {

    private final static String CART_PRODUCTS_ATTRIBUTE_NAME = "cartProducts";
    private final static String DATE_OF_ODER_PARAM_NAME = "date";
    private final static String ID_USER_ATTRIBUTE_NAME = "userId";
    private final static String EXCEPTION_MESSAGE_ATTRIBUTE_NAME = "message";
    private final static String EXCEPTION_MESSAGE = "Wrong amount of inserted rows";
    private final static int ORDER_WAS_COMMITED = 1;
    private final static String CART_PAGE = PageManager.getInstance().getPage(PageManager.CART_PAGE);
    private final static String ERROR_PAGE = PageManager.getInstance().getPage(PageManager.ERROR_PAGE);

    @Override

    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession();
        String forwardPage = null;
        int resultOfCommiting = 0;
        int userId = 0;
        List<Product> products = null;
        Date dateOfOrder = Date.valueOf(request.getParameter(DATE_OF_ODER_PARAM_NAME));
        products = (List<Product>) session.getAttribute(CART_PRODUCTS_ATTRIBUTE_NAME);
        if (products != null && !products.isEmpty()) {
            userId = (int) session.getAttribute(ID_USER_ATTRIBUTE_NAME);
            try {
                resultOfCommiting = CommitOrder.commit(products, dateOfOrder, userId);
            } catch (ServiceException ex) {
                throw new CommandException(ex);
            }
            if (resultOfCommiting == ORDER_WAS_COMMITED) {
                forwardPage = CART_PAGE;
            } else {
                forwardPage = ERROR_PAGE;
                request.setAttribute(EXCEPTION_MESSAGE_ATTRIBUTE_NAME, EXCEPTION_MESSAGE);
            }
            session.setAttribute(CART_PRODUCTS_ATTRIBUTE_NAME, null);
        }
        forwardPage = CART_PAGE;
        return forwardPage;
    }

}
