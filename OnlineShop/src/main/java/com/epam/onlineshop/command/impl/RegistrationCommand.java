/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.command.impl;

import com.epam.onlineshop.command.CommandException;
import com.epam.onlineshop.command.ICommand;
import com.epam.onlineshop.manager.PageManager;
import com.epam.onlineshop.manager.RegexpManager;
import com.epam.onlineshop.service.RegisterUser;
import com.epam.onlineshop.service.ServiceException;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author notepad
 */
public class RegistrationCommand implements ICommand {

    private final static String NAME_ATTRIBUTE = "name";
    private final static String LAST_NAME_ATTRIBUTE = "lastName";
    private final static String LOGIN_ATTRIBUTE_NAME = "login";
    private final static String PASSWORD_ATTRIBUTE_NAME = "password";
    private final static String PHONE_NUMBER_ATTRIBUTE_NAME = "phoneNumber";
    private final static String ADRESS_ATTRIBUTE_NAME = "adress";
    private final static String MAIL_ATTRIBUTE_NAME = "mail";
    private final static String CARD_NUMBER_ATTRIBUTE_NAME = "cardNumber";
    private final static String MESSAGE_ATTRIBUTE_NAME = "message";
    private final static String EXCEPTION_MESSAGE = "Data which you write is not valid!";
    private final static int USER_WAS_REGISTERED = 1;
    public final static String REGEXP_FOR_TEXT = RegexpManager.getInstance().getRegexp(RegexpManager.REGEXP_FOR_TEXT);
    public final static String REGEXP_FOR_LOGIN_AND_PASSWORD = RegexpManager.getInstance().getRegexp(RegexpManager.REGEXP_FOR_LOGIN_AND_PASSWORD);
    public final static String REGEXP_FOR_PHONE_NUMBER = RegexpManager.getInstance().getRegexp(RegexpManager.REGEXP_FOR_PHONE_NUMBER);
    public final static String REGEXP_FOR_MAIl = RegexpManager.getInstance().getRegexp(RegexpManager.REGEXP_FOR_MAIl);
    public final static String REGEXP_FOR_CARD_NUMBER = RegexpManager.getInstance().getRegexp(RegexpManager.REGEXP_FOR_CARD_NUMBER);
    private final static String INDEX_PAGE = PageManager.getInstance().getPage(PageManager.INDEX_PAGE);
    private final static String ERROR_PAGE = PageManager.getInstance().getPage(PageManager.ERROR_PAGE);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String forwardPage = null;
        int resultOfRegistration = 0;
        String userName = request.getParameter(NAME_ATTRIBUTE);
        String userLastName = request.getParameter(LAST_NAME_ATTRIBUTE);
        String userLogin = request.getParameter(LOGIN_ATTRIBUTE_NAME);
        String userPassword = request.getParameter(PASSWORD_ATTRIBUTE_NAME);
        int userPhoneNumber = 0;
        String userAdress = request.getParameter(ADRESS_ATTRIBUTE_NAME);
        String userMail = request.getParameter(MAIL_ATTRIBUTE_NAME);
        int userCardNumber = 0;
        if (request.getParameter(CARD_NUMBER_ATTRIBUTE_NAME)!=null&&request.getParameter(CARD_NUMBER_ATTRIBUTE_NAME).matches(REGEXP_FOR_CARD_NUMBER)) {
            userCardNumber = Integer.parseInt(request.getParameter(CARD_NUMBER_ATTRIBUTE_NAME));
        }
        if (request.getParameter(PHONE_NUMBER_ATTRIBUTE_NAME)!=null&&request.getParameter(PHONE_NUMBER_ATTRIBUTE_NAME).matches(REGEXP_FOR_PHONE_NUMBER)) {
            userPhoneNumber = Integer.parseInt(request.getParameter(PHONE_NUMBER_ATTRIBUTE_NAME));
        }
        try {
            if (userAdress != null && !userAdress.isEmpty() && userName.matches(REGEXP_FOR_TEXT) && userLogin.matches(REGEXP_FOR_LOGIN_AND_PASSWORD)
                    && userPassword.matches(REGEXP_FOR_LOGIN_AND_PASSWORD) && userMail.matches(REGEXP_FOR_MAIl)
                    && userLastName.matches(REGEXP_FOR_TEXT)&&userCardNumber!=0&&userPhoneNumber!=0) {
                resultOfRegistration = RegisterUser.register(userName, userLastName, userLogin, userPassword, userPhoneNumber, userAdress, userMail, userCardNumber);
            }
            if (resultOfRegistration == USER_WAS_REGISTERED) {
                forwardPage = INDEX_PAGE;
            } else {
                request.setAttribute(MESSAGE_ATTRIBUTE_NAME, EXCEPTION_MESSAGE);
                forwardPage = ERROR_PAGE;
            }
        } catch (ServiceException ex) {
            throw new CommandException(ex);
        }
        return forwardPage;
    }

}
