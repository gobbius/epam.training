/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.command.impl;

import com.epam.onlineshop.command.CommandException;
import com.epam.onlineshop.command.ICommand;
import com.epam.onlineshop.manager.PageManager;
import com.epam.onlineshop.service.RemoveOrder;
import com.epam.onlineshop.service.ServiceException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author notepad
 */
public class RemoveOrderCommand implements ICommand {

    private final static String ORDER_ID = "order_id";
    private final static String PAYMENT_PAGE = PageManager.getInstance().getPage(PageManager.PAYMENT_PAGE);
    private final static String ERROR_PAGE = PageManager.getInstance().getPage(PageManager.ERROR_PAGE);
    private final static String EXCEPTION_MESSAGE_ATTRIBUTE_NAME = "message";
    private final static String EXCEPTION_MESSAGE = "Wrong amount of deleted rows";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String forwardPage = null;
        HttpSession session = request.getSession();
        boolean resultOfRemoving = false;
        int orderId = Integer.parseInt(request.getParameter(ORDER_ID));
        try {
            resultOfRemoving = RemoveOrder.remove(orderId, session);
            if(resultOfRemoving==true){
                forwardPage = PAYMENT_PAGE;
            }
            else{
                request.setAttribute(EXCEPTION_MESSAGE_ATTRIBUTE_NAME, EXCEPTION_MESSAGE);
                forwardPage = ERROR_PAGE;
            }
        } catch (ServiceException ex) {
            throw new CommandException(ex);
        }
        
        return forwardPage;
    }

}
