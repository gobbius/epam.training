/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.command.impl;

import com.epam.onlineshop.command.CommandException;
import com.epam.onlineshop.command.ICommand;
import com.epam.onlineshop.domain.Product;
import com.epam.onlineshop.domain.ProductParam;
import com.epam.onlineshop.manager.PageManager;
import com.epam.onlineshop.service.DeleteProductWithParams;
import com.epam.onlineshop.service.ServiceException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author notepad
 */
public class RemoveProductCommand implements ICommand {

    private final static String PARAMS_ATTRIBUTES_NAME = "params";
    private final static String PRODUCT_ATTRIBUTE_NAME = "product";
    private final static String MESSAGE_ATTRIBUTE_NAME = "message";
    private final static String EXCEPTION_MESSAGE = "Wrong amount of deleted rows";
    private final static int PRODUCT_WAS_DELETED = 1;
    private final static String ERROR_PAGE = PageManager.getInstance().getPage(PageManager.ERROR_PAGE);
    private final static RedirectCommand redirectCommand = new RedirectCommand();

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession();
        int resultOfDeleting = 0;
        Product product = (Product) session.getAttribute(PRODUCT_ATTRIBUTE_NAME);
        List<ProductParam> params = (List<ProductParam>) session.getAttribute(PARAMS_ATTRIBUTES_NAME);
        try {
            resultOfDeleting = DeleteProductWithParams.deleteProduct(product, params);
            if (resultOfDeleting == PRODUCT_WAS_DELETED) {
                return redirectCommand.execute(request);
            } else {
                request.setAttribute(MESSAGE_ATTRIBUTE_NAME, EXCEPTION_MESSAGE);
                return ERROR_PAGE;
            }
        } catch (ServiceException ex) {
            throw new CommandException(ex);
        }
    }

}
