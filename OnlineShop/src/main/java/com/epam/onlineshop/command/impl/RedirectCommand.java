/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.command.impl;

import com.epam.onlineshop.command.CommandException;
import com.epam.onlineshop.command.ICommand;
import com.epam.onlineshop.domain.Product;
import com.epam.onlineshop.manager.PageManager;
import com.epam.onlineshop.service.GetAllProducts;
import com.epam.onlineshop.service.GetOrdersByUserId;
import com.epam.onlineshop.service.GetProductAndParams;
import com.epam.onlineshop.service.GetTypesOfProduct;
import com.epam.onlineshop.service.ServiceException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * RedirectCommand class for redirecting client after requests to the right responses pages
 * @author notepad
 */
public class RedirectCommand implements ICommand {

    private final static String REGISTRATION_PAGE = PageManager.getInstance().getPage(PageManager.REGISTRATION_PAGE);
    private final static String INDEX_PAGE = PageManager.getInstance().getPage(PageManager.INDEX_PAGE);
    private final static String ADD_PRODUCT_PAGE = PageManager.getInstance().getPage(PageManager.ADD_PRODUCT_PAGE);
    private final static String ADMIN_MENU_PAGE = PageManager.getInstance().getPage(PageManager.ADMIN_MENU_PAGE);
    private final static String ADMIN_CATALOG_PAGE = PageManager.getInstance().getPage(PageManager.ADMIN_CATALOG_PAGE);
    private final static String USER_CATALOG_PAGE = PageManager.getInstance().getPage(PageManager.USER_CATALOG_PAGE);
    private final static String ADMIN_PRODUCT_PAGE = PageManager.getInstance().getPage(PageManager.ADMIN_PRODUCT_PAGE);
    private final static String USER_PRODUCT_PAGE = PageManager.getInstance().getPage(PageManager.USER_PRODUCT_PAGE);
    private final static String CART_PAGE = PageManager.getInstance().getPage(PageManager.CART_PAGE);
    private final static String PAYMENT_PAGE = PageManager.getInstance().getPage(PageManager.PAYMENT_PAGE);
    private final static String EDIT_PRODUCT_PAGE = PageManager.getInstance().getPage(PageManager.EDIT_PRODUCT_PAGE);
    private final static String USER_NAME_ATTRIBUTE = "userName";
    private final static String USER_ID_ATTRIBUTE_NAME = "userId";
    private final static String ADMIN_ATTRIBUTE_NAME = "isAdmin";
    private final static String PRODUCT_ID_ATTRIBUTE_NAME = "product_id";
    private final static String YES_ITS_ADMIN = "yes";
    private final static String NO_ITS_NOT_ADMIN = "no";
    private final static String EDIT_PRODUCT_WAY = "edit_product_way";
    private final static String REGISTRATION_WAY = "registration_way";
    private final static String PRODUCT_WAY = "product_way";
    private final static String INDEX_WAY = "index_way";
    private final static String CART_WAY = "cart_way";
    private final static String ADD_PRODUCT_WAY = "add_product_way";
    private final static String MENU_WAY = "menu_way";
    private final static String CATALOG_WAY = "catalog_way";
    private final static String BACK_WAY = "back_way";
    private final static String SEARCH_WAY = "search_way";
    private final static String SORTING_WAY = "sorting_way";
    private final static String LOG_OUT_WAY = "log_out_way";
    private final static String PAYMENT_WAY = "payment_way";
    private final static String CHANGE_LOCALE = "change_locale_way";
    private final static String LOCALE = "locale";
    private final static String WAY_ATTRIBUTE = "way_attribute";
    private final static String TO_PRODUCT_ATTRIBUTE = "to_product";
    private final static String LOCALE_BUTTON = "locale_button";
    private final static String WAY_OF_REDIRECTING = "way_of_redirecting";
    private final static String CART_PRODUCTS_ATTRIBUTE_NAME = "cartProducts";
    private final static String PRODUCT_ATTRIBUTE_NAME = "product";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String forwardPage = null;
        HttpSession session = request.getSession();
        int userId = 0;
        String wayOfRedirecting = request.getParameter(WAY_OF_REDIRECTING);
        switch (wayOfRedirecting) {
            case REGISTRATION_WAY: {
                forwardPage = REGISTRATION_PAGE;
                break;
            }
            case INDEX_WAY: {
                forwardPage = INDEX_PAGE;
                break;
            }
            case CHANGE_LOCALE: {
                forwardPage = INDEX_PAGE;
                String locale = request.getParameter(LOCALE_BUTTON).toLowerCase();
                session.setAttribute(LOCALE, locale);
                break;
            }
            case ADD_PRODUCT_WAY: {
                forwardPage = ADD_PRODUCT_PAGE;
                try {
                    GetTypesOfProduct.getTypes(request);
                } catch (ServiceException ex) {
                    throw new CommandException(ex);
                }
                break;
            }
            case MENU_WAY: {
                if (session.getAttribute(ADMIN_ATTRIBUTE_NAME) == null) {
                    forwardPage = INDEX_PAGE;
                } else if (session.getAttribute(ADMIN_ATTRIBUTE_NAME).equals(NO_ITS_NOT_ADMIN)) {
                    forwardPage = INDEX_PAGE;
                } else if (session.getAttribute(ADMIN_ATTRIBUTE_NAME).equals(YES_ITS_ADMIN)) {
                    forwardPage = ADMIN_MENU_PAGE;
                }
                break;
            }
            case CATALOG_WAY: {
                if (session.getAttribute(ADMIN_ATTRIBUTE_NAME) == null) {
                    forwardPage = INDEX_PAGE;
                } else if (session.getAttribute(ADMIN_ATTRIBUTE_NAME).equals(YES_ITS_ADMIN)) {
                    if (session.getAttribute(PRODUCT_ATTRIBUTE_NAME) != null) {
                        session.setAttribute(PRODUCT_ATTRIBUTE_NAME, null);
                    }
                    forwardPage = ADMIN_CATALOG_PAGE;
                } else if (session.getAttribute(ADMIN_ATTRIBUTE_NAME).equals(NO_ITS_NOT_ADMIN)) {
                    if (session.getAttribute(PRODUCT_ATTRIBUTE_NAME) != null) {
                        session.setAttribute(PRODUCT_ATTRIBUTE_NAME, null);
                    }
                    forwardPage = USER_CATALOG_PAGE;
                }
                try {
                    GetTypesOfProduct.getTypes(request);
                    GetAllProducts.getProducts(request);
                } catch (ServiceException ex) {
                    throw new CommandException(ex);
                }
                break;
            }
            case PRODUCT_WAY: {
                int idProduct = 0;
                if (session.getAttribute(PRODUCT_ATTRIBUTE_NAME) != null) {
                    idProduct = ((Product) session.getAttribute(PRODUCT_ATTRIBUTE_NAME)).getId();
                } else {
                    idProduct = Integer.parseInt(request.getParameter(PRODUCT_ID_ATTRIBUTE_NAME));
                }
                try {
                    GetProductAndParams.getParamsAndName(idProduct, session);
                } catch (ServiceException ex) {
                    throw new CommandException(ex);
                }
                if (session.getAttribute(ADMIN_ATTRIBUTE_NAME) == null) {
                    forwardPage = INDEX_PAGE;
                } else if (session.getAttribute(ADMIN_ATTRIBUTE_NAME).equals(NO_ITS_NOT_ADMIN)) {
                    forwardPage = USER_PRODUCT_PAGE;
                } else if (session.getAttribute(ADMIN_ATTRIBUTE_NAME).equals(YES_ITS_ADMIN)) {
                    forwardPage = ADMIN_PRODUCT_PAGE;
                }
                break;
            }
            case SEARCH_WAY: {
                if (session.getAttribute(ADMIN_ATTRIBUTE_NAME) == null) {
                    forwardPage = INDEX_PAGE;
                } else if (session.getAttribute(ADMIN_ATTRIBUTE_NAME).equals(YES_ITS_ADMIN)) {
                    forwardPage = ADMIN_CATALOG_PAGE;
                } else if (session.getAttribute(ADMIN_ATTRIBUTE_NAME).equals(NO_ITS_NOT_ADMIN)) {
                    forwardPage = USER_CATALOG_PAGE;
                }
                try {
                    GetTypesOfProduct.getTypes(request);
                } catch (ServiceException ex) {
                    throw new CommandException(ex);
                }
                break;
            }
            case SORTING_WAY: {
                if (session.getAttribute(ADMIN_ATTRIBUTE_NAME) == null) {
                    forwardPage = INDEX_PAGE;
                } else if (session.getAttribute(ADMIN_ATTRIBUTE_NAME).equals(YES_ITS_ADMIN)) {
                    forwardPage = ADMIN_CATALOG_PAGE;
                } else if (session.getAttribute(ADMIN_ATTRIBUTE_NAME).equals(NO_ITS_NOT_ADMIN)) {
                    forwardPage = USER_CATALOG_PAGE;
                }
                try {
                    GetTypesOfProduct.getTypes(request);
                } catch (ServiceException ex) {
                    throw new CommandException(ex);
                }
                break;
            }
            case CART_WAY: {
                if (session.getAttribute(ADMIN_ATTRIBUTE_NAME) == null) {
                    forwardPage = INDEX_PAGE;
                } else if (session.getAttribute(ADMIN_ATTRIBUTE_NAME).equals(YES_ITS_ADMIN)) {
                    forwardPage = CART_PAGE;
                } else if (session.getAttribute(ADMIN_ATTRIBUTE_NAME).equals(NO_ITS_NOT_ADMIN)) {
                    forwardPage = CART_PAGE;
                }
                break;
            }
            case LOG_OUT_WAY: {
                session.setAttribute(ADMIN_ATTRIBUTE_NAME, null);
                session.setAttribute(CART_PRODUCTS_ATTRIBUTE_NAME, null);
                forwardPage = INDEX_PAGE;
                break;
            }
            case PAYMENT_WAY: {
                if (session.getAttribute(ADMIN_ATTRIBUTE_NAME) != null) {
                    if (session.getAttribute(ADMIN_ATTRIBUTE_NAME).equals(NO_ITS_NOT_ADMIN)) {
                        userId = (int) session.getAttribute(USER_ID_ATTRIBUTE_NAME);
                        try {
                            GetOrdersByUserId.getOrders(session, userId);
                        } catch (ServiceException ex) {
                            throw new CommandException(ex);
                        }
                    }
                    forwardPage = PAYMENT_PAGE;
                } else {
                    forwardPage = INDEX_PAGE;
                }
                break;
            }
            case BACK_WAY: {
                if (session.getAttribute(ADMIN_ATTRIBUTE_NAME) == null) {
                    forwardPage = REGISTRATION_PAGE;
                } else if (session.getAttribute(ADMIN_ATTRIBUTE_NAME).equals(YES_ITS_ADMIN)) {
                    if (request.getParameter(WAY_ATTRIBUTE) != null && request.getParameter(WAY_ATTRIBUTE).equals(TO_PRODUCT_ATTRIBUTE)) {
                        forwardPage = ADMIN_PRODUCT_PAGE;
                    } else {
                        forwardPage = ADMIN_MENU_PAGE;
                    }
                } else {
                    forwardPage = INDEX_PAGE;
                }
                break;
            }
            case EDIT_PRODUCT_WAY: {
                try {
                    GetTypesOfProduct.getTypes(request);
                } catch (ServiceException ex) {
                    throw new CommandException(ex);
                }
                forwardPage = EDIT_PRODUCT_PAGE;
                break;
            }
        }
        return forwardPage;
    }
}
