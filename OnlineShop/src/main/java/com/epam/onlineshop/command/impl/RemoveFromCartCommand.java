/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.command.impl;

import com.epam.onlineshop.command.CommandException;
import com.epam.onlineshop.command.ICommand;
import com.epam.onlineshop.manager.PageManager;
import com.epam.onlineshop.service.RemoveFromCart;
import com.epam.onlineshop.service.ServiceException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author notepad
 */
public class RemoveFromCartCommand implements ICommand {

    private final static String PRODUCT_ID_ATTRIBUTE_NAME = "product_id";
    private final static String CART_PAGE = PageManager.getInstance().getPage(PageManager.CART_PAGE);
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String forwardPage = null;
        HttpSession session = request.getSession();
        int productId = Integer.parseInt(request.getParameter(PRODUCT_ID_ATTRIBUTE_NAME));
        try {
            RemoveFromCart.remove(session, productId);
        } catch (ServiceException ex) {
            throw new CommandException(ex);
        }
        forwardPage = CART_PAGE;
        return forwardPage;
    }

}
