/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.command.impl;

import com.epam.onlineshop.command.CommandException;
import com.epam.onlineshop.command.ICommand;
import com.epam.onlineshop.manager.PageManager;
import com.epam.onlineshop.service.AddProductToCart;
import com.epam.onlineshop.service.ServiceException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author notepad
 */
public class AddProductToCartCommand implements ICommand {

    private final static String PRODUCT_PRICE_PARAM_NAME = "price";
    private final static String PRODUCT_ID_PARAM_NAME = "product_id";
    private final static String PRODUCT_NAME_PARAM = "product_name";
    private final static String PRODUCT_TYPE_ID_PARAM_NAME = "product_type_id";
    private final static String USER_PRODUCT_PAGE = PageManager.getInstance().getPage(PageManager.USER_PRODUCT_PAGE);
    private final static String RESULT_OF_ADDING_ATTRIBUTE_NAME = "resultOfAdding";
    private final static String RESULT_OF_ADDING_MESSAGE_RU = "Товар успешно добавлен в корзину";
    private final static String RESULT_OF_ADDING_MESSAGE_EN = "Product is sucessfuly added";
    private final static String LOCALE_SESSION_ATTRIBUTE_NAME = "locale";
    private final static String EN_LOCALE = "en";
    private final static String RU_LOCALE = "ru";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession();
        String forwardPage = null;
        int productPrice = Integer.parseInt(request.getParameter(PRODUCT_PRICE_PARAM_NAME));
        int productId = Integer.parseInt(request.getParameter(PRODUCT_ID_PARAM_NAME));
        String productName = request.getParameter(PRODUCT_NAME_PARAM);
        int productTypeId = Integer.parseInt(request.getParameter(PRODUCT_TYPE_ID_PARAM_NAME));
        try {
            AddProductToCart.AddToCart(productPrice, productId, productName, productTypeId, session);
            forwardPage = USER_PRODUCT_PAGE;
        } catch (ServiceException ex) {
            throw new CommandException(ex);
        }
        if (session.getAttribute(LOCALE_SESSION_ATTRIBUTE_NAME) == null) {
            request.setAttribute(RESULT_OF_ADDING_ATTRIBUTE_NAME, RESULT_OF_ADDING_MESSAGE_EN);
        } else if (session.getAttribute(LOCALE_SESSION_ATTRIBUTE_NAME).equals(EN_LOCALE)) {
            request.setAttribute(RESULT_OF_ADDING_ATTRIBUTE_NAME, RESULT_OF_ADDING_MESSAGE_EN);
        } else if (session.getAttribute(LOCALE_SESSION_ATTRIBUTE_NAME).equals(RU_LOCALE)) {
            request.setAttribute(RESULT_OF_ADDING_ATTRIBUTE_NAME, RESULT_OF_ADDING_MESSAGE_RU);
        }
        return forwardPage;
    }

}
