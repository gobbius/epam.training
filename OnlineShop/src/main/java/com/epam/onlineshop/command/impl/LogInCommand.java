/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.command.impl;

import com.epam.onlineshop.command.CommandException;
import com.epam.onlineshop.command.ICommand;
import com.epam.onlineshop.service.CheckUser;
import com.epam.onlineshop.service.ServiceException;
import com.epam.onlineshop.manager.PageManager;
import com.epam.onlineshop.service.GetAllProducts;
import com.epam.onlineshop.service.GetTypesOfProduct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author notepad
 */
public class LogInCommand implements ICommand {

    private final static String USER_CATALOG_PAGE = PageManager.getInstance().getPage(PageManager.USER_CATALOG_PAGE);
    private final static String ADMIN_MENU_PAGE = PageManager.getInstance().getPage(PageManager.ADMIN_MENU_PAGE);
    private final static String INCORRECT_PASS_PAGE = PageManager.getInstance().getPage(PageManager.INCORRECT_PASS);
    private final static int INCORRECT_PASSWORD = 0;
    private final static int ADMIN_LOGED_IN = 1;
    private final static int USER_LOGED_IN = 2;
    private final static String LOGIN = "login";
    private final static String PASSWORD = "password";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        String forwardPage = null;
        HttpSession session = request.getSession();
        String login = request.getParameter(LOGIN);
        String password = request.getParameter(PASSWORD);
        int resultOfChecking = 0;
        try {
            resultOfChecking = CheckUser.checkUser(login, password, session);
            switch (resultOfChecking) {
                case INCORRECT_PASSWORD: {
                    forwardPage = INCORRECT_PASS_PAGE;
                    break;
                }
                case ADMIN_LOGED_IN: {
                    forwardPage = ADMIN_MENU_PAGE;
                    break;
                }
                case USER_LOGED_IN: {
                    forwardPage = USER_CATALOG_PAGE;
                    GetTypesOfProduct.getTypes(request);
                    GetAllProducts.getProducts(request);
                    break;
                }
            }
        } catch (ServiceException ex) {
            throw new CommandException(ex);
        }
        return forwardPage;
    }

}
