/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.command;

import com.epam.onlineshop.command.impl.AddProductToCartCommand;
import com.epam.onlineshop.command.impl.AddProductCommand;
import com.epam.onlineshop.command.impl.CheckOutOrderCommand;
import com.epam.onlineshop.command.impl.CommitOrderCommand;
import com.epam.onlineshop.command.impl.RemoveProductCommand;
import com.epam.onlineshop.command.impl.LogInCommand;
import com.epam.onlineshop.command.impl.RedirectCommand;
import com.epam.onlineshop.command.impl.RegistrationCommand;
import com.epam.onlineshop.command.impl.RemoveFromCartCommand;
import com.epam.onlineshop.command.impl.RemoveOrderCommand;
import com.epam.onlineshop.command.impl.SaveEditedProductCommand;
import com.epam.onlineshop.command.impl.SearchOrderCommand;
import com.epam.onlineshop.command.impl.SearchProductCommand;
import com.epam.onlineshop.command.impl.SortCommand;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author notepad
 */
public class CommandFactory {

    private final static String LOG_IN = "log_in";
    private final static String REGISTRATION = "registration";
    private final static String REDIRECT = "redirect";
    private final static String ADD_PRODUCT = "add_product";
    private final static String DELETE_PRODUCT = "delete_product";
    private final static String ADD_PRODUCT_TO_CART = "add_product_to_cart";
    private final static String SEARCHING = "searching";
    private final static String SORTING = "sorting";
    private final static String COMMIT_ORDER = "commit_order";
    private final static String REMOVE_FROM_CART = "remove_from_cart";
    private final static String SEARCH_ORDER = "search_order";
    private final static String REMOVE_ORDER = "remove_order";
    private final static String CHECK_OUT = "check_out";
    private final static String SAVE_EDITED_PRODUCT = "save_edited_product";
    private final static CommandFactory instance = new CommandFactory();
    private Map<String, ICommand> commands = new HashMap<String, ICommand>();

    private CommandFactory() {
        commands.put(LOG_IN, new LogInCommand());
        commands.put(REGISTRATION, new RegistrationCommand());
        commands.put(REDIRECT, new RedirectCommand());
        commands.put(ADD_PRODUCT, new AddProductCommand());
        commands.put(DELETE_PRODUCT, new RemoveProductCommand());
        commands.put(SEARCHING, new SearchProductCommand());
        commands.put(SORTING, new SortCommand());
        commands.put(ADD_PRODUCT_TO_CART, new AddProductToCartCommand());
        commands.put(COMMIT_ORDER, new CommitOrderCommand());
        commands.put(REMOVE_FROM_CART, new RemoveFromCartCommand());
        commands.put(SEARCH_ORDER, new SearchOrderCommand());
        commands.put(REMOVE_ORDER, new RemoveOrderCommand());
        commands.put(CHECK_OUT, new CheckOutOrderCommand());
        commands.put(SAVE_EDITED_PRODUCT, new SaveEditedProductCommand());
    }

    public static CommandFactory getInstance() {
        return instance;
    }

    public ICommand getCommand(String commandName) {
        return commands.get(commandName);
    }
}
