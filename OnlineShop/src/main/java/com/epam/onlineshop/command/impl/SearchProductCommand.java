/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.onlineshop.command.impl;

import com.epam.onlineshop.command.CommandException;
import com.epam.onlineshop.command.ICommand;
import com.epam.onlineshop.service.FindProductByName;
import com.epam.onlineshop.service.GetAllProducts;
import com.epam.onlineshop.service.ServiceException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author notepad
 */
public class SearchProductCommand implements ICommand {

    private final static String SEARCHING_RESULT_LABEL_ATTRIBUTE_NAME = "resultLabel";
    private final static String LOCALE_SESSION_ATTRIBUTE_NAME = "locale";
    private final static String EN_LOCALE = "en";
    private final static String RU_LOCALE = "ru";
    private final static String PRODUCT_NOT_FIND_MESSAGE_EN = "Product with this name doesnt exists";
    private final static String PRODUCT_NOT_FIND_MESSAGE_RU = "Товар с таким названием не найден";
    private final static String PRODUCT_NAME_ATTRIBUTE = "product_name";
    private final static RedirectCommand redirectCommand = new RedirectCommand();

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String forwardPage = null;
        HttpSession session = request.getSession();
        String productName = request.getParameter(PRODUCT_NAME_ATTRIBUTE);
        boolean resultOfSearching = false;
        if (productName != null && !productName.isEmpty()) {
            try {
                resultOfSearching = FindProductByName.findProduct(productName, request);
                if (!resultOfSearching) {
                    if (session.getAttribute(LOCALE_SESSION_ATTRIBUTE_NAME) == null) {
                        request.setAttribute(SEARCHING_RESULT_LABEL_ATTRIBUTE_NAME, PRODUCT_NOT_FIND_MESSAGE_EN);
                    } else if (session.getAttribute(LOCALE_SESSION_ATTRIBUTE_NAME).equals(EN_LOCALE)) {
                        request.setAttribute(SEARCHING_RESULT_LABEL_ATTRIBUTE_NAME, PRODUCT_NOT_FIND_MESSAGE_EN);
                    } else if (session.getAttribute(LOCALE_SESSION_ATTRIBUTE_NAME).equals(RU_LOCALE)) {
                        request.setAttribute(SEARCHING_RESULT_LABEL_ATTRIBUTE_NAME, PRODUCT_NOT_FIND_MESSAGE_RU);
                    }
                }
            } catch (ServiceException ex) {
                throw new CommandException(ex);
            }
            return redirectCommand.execute(request);
        } else {
            try {
                GetAllProducts.getProducts(request);
                return redirectCommand.execute(request);
            } catch (ServiceException ex) {
                throw new CommandException(ex);
            }
        }

    }

}
