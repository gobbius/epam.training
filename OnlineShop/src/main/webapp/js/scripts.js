function deleteParam(e) {
    if(e.target.getAttribute('class').split(" ").indexOf("del-ico") !== -1) {
        e.target.parentElement.parentElement.remove();
    }
}
function addParam() {
    var first = document.createElement('input');
    first.setAttribute('type', 'text');
    first.setAttribute('name', 'param_name');
    first.setAttribute('pattern', '[A-ZА-Яa-zа-я\\s][a-zа-я\\s]{1,50}');
    first.setAttribute('placeholder', localeName);
    first.setAttribute('class', 'form-control');
    var second = document.createElement('input');
    second.setAttribute('type', 'text');
    second.setAttribute('name', 'param_value');
    second.setAttribute('pattern', '[А-ЯA-Za-zа-я0-9\\s\.]{1,20}');
    second.setAttribute('placeholder', localeValue);
    second.setAttribute('class', 'form-control');
    var container = document.querySelector('#more-params')
    var div = document.createElement('div');
    div.setAttribute('class', 'more-container');
    div.setAttribute('name', 'params');
    var del = document.createElement('div');
    del.setAttribute('class', 'delete-block');
    del.innerHTML = "<i class='glyphicon glyphicon-remove del-ico'></i>"
    div.appendChild(del);
    div.appendChild(first);
    div.appendChild(second);
    container.appendChild(div);
}
function confirmRemoving(){    
    if(confirm('Delete?')){
        var xhr = new XMLHttpRequest();
        var params = 'command=' + encodeURIComponent('delete_product') + '&way_of_redirecting='+encodeURIComponent('catalog_way');
        xhr.open("GET",'catalog?' + params, true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send();
    }
}