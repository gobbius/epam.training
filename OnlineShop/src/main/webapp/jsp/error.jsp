<%-- 
    Document   : error
    Created on : Jul 22, 2015, 12:29:00 PM
    Author     : notepad
--%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <fmt:setLocale value="${sessionScope.locale}" />
        <fmt:setBundle basename="locale" var="locale"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/styles.css" rel="stylesheet" type="text/css">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
        <link href="css/signin.css" rel="stylesheet" type="text/css">
        <title><fmt:message key="error.page.title" bundle="${locale}" /></title>
    </head>
    <body  style="margin-top: 0.01%;text-align: center">
        <div class="container">
            <h1>${message}</h1>
            <div class="form-signin">
                <form action="Controller" method="GET">
                    <input type="hidden" name="command" value="redirect" />
                    <input type="hidden" name="way_of_redirecting" value="back_way" />
                    <input style="width: 80%;" class="btn btn-primary" type="submit" value="<fmt:message key="back.way.button" bundle="${locale}" />" />
                </form>
            </div>
        </div>
    </body>
</html>
