<%-- 
    Document   : incorrectPassword
    Created on : Jul 22, 2015, 12:37:22 PM
    Author     : notepad
--%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <fmt:setLocale value="${sessionScope.locale}" />
        <fmt:setBundle basename="locale" var="locale" />
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
        <link href="css/bootstrap-theme.min.css" type="text/css" rel="stylesheet">
        <link href="css/signin.css" rel="stylesheet" type="text/css">
        <link href="css/styles.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>.error-form{margin: 0 auto; width: 500px;}</style>
        <title><fmt:message key="incorrect.password.title" bundle="${locale}" /></title>
    </head>
    <body  style="margin-top: 0.01%;">
        <div style="text-align: center;" class="error-form"><h3><fmt:message key="incorrect.password" bundle="${locale}"/></h3>
            <form action="login" method="GET">
                <input type="hidden" name="way_of_redirecting" value="index_way" />
                <input type="hidden" name="command" value="redirect" />
                <input class="btn btn-lg btn-primary btn-block" type="submit" value="<fmt:message key="log.out.button" bundle="${locale}" />">
            </form>
        </div>
    </body>
</html>
