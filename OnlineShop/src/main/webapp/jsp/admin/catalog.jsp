<%-- 
    Document   : catalog
    Created on : Aug 12, 2015, 12:31:29 PM
    Author     : notepad
--%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <fmt:setLocale value="${sessionScope.locale}" />
        <fmt:setBundle basename="locale" var="locale" />
        <title><fmt:message key="catalog.title" bundle="${locale}" /></title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="css/dashboard.css" rel="stylesheet" type="text/css">
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
        <link href="css/styles.css" rel="stylesheet" type="text/css">
    </head>
    <body style="margin-top: 0.01%;">
        <nav style="margin-top: 0.01%;" class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div style="width: 4%;height: 100%;margin-top: 0.4%;" class="navbar-header">
                    <form action="menu" method="GET">
                        <input type="hidden" name="command" value="redirect" />
                        <input type="hidden" name="way_of_redirecting" value="menu_way" />
                        <button title="<fmt:message key="menu.title" bundle="${locale}" />" style="width:100%; height: 100%;" class="form-control" type="submit">
                            <span class="glyphicon glyphicon-arrow-left"></span>
                        </button>
                    </form>
                </div>
                <div style="margin-top: 0.4%; margin-left: 0.5%;width: 4%;height: 100%;" class="navbar-header">
                    <form action="login" method="GET">
                        <input type="hidden" name="command" value="redirect" />
                        <input type="hidden" name="way_of_redirecting" value="log_out_way" />
                        <button title="<fmt:message key="log.out.title" bundle="${locale}" />" style="width:100%; height: 100%;" class="form-control" type="submit">
                            <span class="glyphicon glyphicon-log-out"></span>
                        </button>
                    </form>
                </div>
                <div style="color: white;margin-top: 0.6%;margin-left: 30%;" class="navbar-header">
                    <label style="width: 100%">${resultLabel}</label>
                </div>
                <div class="navbar-header" style="width: 400px;float: right;">
                    <form style="margin-top: -3px;width: 400px;" class="navbar-form navbar-right" action="catalog" method="GET">
                        <input style="margin-left: 30px;width: 220px;" type="text" name="product_name" class="form-control" placeholder="<fmt:message key="search.placeholder" bundle="${locale}" />">
                        <input type="hidden" name="way_of_redirecting" value="search_way" />
                        <input type="hidden" name="command" value="searching" />
                        <input style="float: right;width: 100px;" class="btn btn-primary" type="submit" value="<fmt:message key="search.button" bundle="${locale}" />" />
                    </form>
                </div>
            </div>
        </nav>
        <div class="container-fluid">
            <div class="row">
                <div style="margin-top: 0.01%;" class="col-sm-3 col-md-2 sidebar">
                    <form class="catalog-sorting-form" action="catalog" method="POST">
                        <label><fmt:message  key="type.label" bundle="${locale}" /></label>
                        <div style="margin-left: 20%;margin-top: -12%;">
                            <select style="width: 100%;" class="form-control" name="type">
                                <option><fmt:message key="all.types.label" bundle="${locale}" /></option>
                                <c:forEach var="type" items="${types}">
                                    <option>${type.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <br/>
                        <label><fmt:message key="sorting.price.label" bundle="${locale}" /></label>
                        <div style="margin-top:-12%;margin-left: 20%;width: 100%;">
                            <select style="width: 80%;" class="form-control" name="price">
                                <option><fmt:message key="sorting.by.price.option.one"  bundle="${locale}" /></option>
                                <option><fmt:message key="sorting.by.price.option.two"  bundle="${locale}" /></option>
                            </select>
                        </div>
                        <input type="hidden" name="command" value="sorting" />
                        <input type="hidden" name="way_of_redirecting" value="sorting_way" />
                        <input style="margin-left: 20%;" class="btn btn-primary" type="submit" value="<fmt:message key="sorting.button" bundle="${locale}" />" />
                    </form>
                </div>
                <div style="height: 80%;" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <div  class="table-responsive">
                        <table  style="margin-top: -0.5%;" class="table table-striped">
                            <thead>
                                <tr>
                                    <th style="width: 78%;"><fmt:message key="name.label" bundle="${locale}" /></th>
                                    <th style="width: 14%;"><fmt:message key="price.label" bundle="${locale}" /></th>
                                    <th style="width: 8%;"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="product" items="${products}">

                                    <tr>
                                        <td style="text-align: left;">${product.name}</td>
                                        <td style="text-align: left;"><fmt:formatNumber value="${product.price}" type="CURRENCY" currencySymbol="" /> <fmt:message key="money.index" bundle="${locale}" /></td>
                                        <td>
                                            <div style="margin-top: -11%;">
                                                <form action="product" method="GET" >
                                                    <input type="hidden" name="command" value="redirect" />
                                                    <input type="hidden" name="way_of_redirecting" value="product_way" />
                                                    <input type="hidden" name="product_id" value="${product.id}" />
                                                    <input class="btn btn-primary" type="submit" value="<fmt:message key="more.button" bundle="${locale}" />" />
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

