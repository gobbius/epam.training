<%-- 
    Document   : admin_product_page
    Created on : Aug 12, 2015, 12:50:01 PM
    Author     : notepad
--%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <fmt:setLocale value="${sessionScope.locale}" />
        <fmt:setBundle basename="locale" var="locale"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="css/dashboard.css" rel="stylesheet" type="text/css">
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
        <link href="css/styles.css" rel="stylesheet" type="text/css">
        <title>${product.name}</title>
    </head>
    <body style="margin-top: 0.01%;">
        <nav style="margin-top: 0.01%;" class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div style="width: 4%;height: 100%;margin-top: 0.4%;" class="navbar-header">
                    <form action="menu" method="GET">
                        <input type="hidden" name="command" value="redirect" />
                        <input type="hidden" name="way_of_redirecting" value="menu_way" />
                        <button title="<fmt:message key="menu.title" bundle="${locale}" />" style="width:100%; height: 100%;" class="form-control" type="submit">
                            <span class="glyphicon glyphicon-arrow-left"></span>
                        </button>
                    </form>
                </div>
                <div style="margin-top: 0.4%; margin-left: 0.5%;width: 4%;height: 100%;" class="navbar-header">
                    <form action="login" method="GET">
                        <input type="hidden" name="command" value="redirect" />
                        <input type="hidden" name="way_of_redirecting" value="log_out_way" />
                        <button title="<fmt:message key="log.out.title" bundle="${locale}" />" style="width:100%; height: 100%;" class="form-control" type="submit">
                            <span class="glyphicon glyphicon-log-out"></span>
                        </button>
                    </form>
                </div>
            </div>
        </nav>
        <div style="width: 42.6%; margin: 0 auto; text-align: center;">
            <h3><fmt:message key="params.label" bundle="${locale}" /></h3>
            <table style="width: 100%;" class="table table-striped">
                <thead>
                    <tr>
                        <th style="text-align: center;width: 50%;"><fmt:message key="param.name.label" bundle="${locale}" /></th>
                        <th style="text-align: center;width: 50%;"><fmt:message key="param.value.label" bundle="${locale}" /></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="item" items="${params}">
                        <tr>
                            <td style="text-align: center;">${item.name}</td>
                            <td style="text-align: center;">${item.value}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <div style="text-align: right;">
                <h3><fmt:message key="price.label" bundle="${locale}" />: <fmt:formatNumber value="${product.price}" type="CURRENCY" currencySymbol="" /> <fmt:message key="money.index" bundle="${locale}" /></h3>
            </div>
            <div>
                <div style="margin-left: 20%;margin-top: 5%;">
                    <form action="catalog" method="GET">
                        <input type="hidden" name="command" value="delete_product" />
                        <input type="hidden" name="way_of_redirecting" value="catalog_way" />
                        <input style="width: 30%;" class="btn btn-lg" type="submit" value="<fmt:message key="delete.button" bundle="${locale}" />" />
                    </form>
                </div>
                <div style="margin-left: 25%;width: 20%;margin-top: -7%;">
                    <form action="edit_product" method="GET">
                        <input type="hidden" name="command" value="redirect" />
                        <input type="hidden" name="way_of_redirecting" value="edit_product_way" />
                        <input style="width: 100%;" class="btn btn-lg" type="submit" value="<fmt:message key="edit.button" bundle="${locale}" />" />
                    </form>
                </div>
                <div style="margin-left: 75%;margin-top: -7%;">
                    <form action="catalog" method="GET">
                        <input type="hidden" name="command" value="redirect" />
                        <input type="hidden" name="way_of_redirecting" value="catalog_way" />
                        <input style="width: 100%;" class="btn btn-lg" type="submit" value="<fmt:message key="back.button" bundle="${locale}" />" />
                    </form>
                </div>
            </div>
            <div style="margin-top: 3%;">
                <label>${completeMessage}</label>
            </div>
        </div>
    </body>
</html>
