/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.logic;

import com.epam.course.entity.PartOfSentense;
import com.epam.course.entity.Sentense;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
/**
 *
 * @author notepad
 */
public class FindWordByLengthAction {
    private final static Logger log = Logger.getLogger(FindWordByLengthAction.class);
    public void findWordByLength(int length){
        Sentense sentense = new Sentense();
        List<PartOfSentense> listOfParts = sentense.getPartsOfSentenses();
        List<String> listOfWords = new ArrayList<String>();
        for(PartOfSentense p : listOfParts){
            if(p.getWord()!=null){
                if(p.getWord().length()==length){
                    listOfWords.add(p.getWord());
                }
            }
        }
        log.info("Action is completed");
    }
}
