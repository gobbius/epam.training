/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.logic.parser;

import com.epam.course.entity.Paragraph;
import com.epam.course.entity.PartOfSentense;
import com.epam.course.entity.Sentense;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;

/**
 *
 * @author notepad
 */
public class PartOfSentenseParser implements Parser {

    private final static Logger log = Logger.getLogger(PartOfSentenseParser.class);

    @Override
    public void parse(String input) {

        List<String> listSent = new ArrayList<String>();
        Paragraph paragraph = new Paragraph();
        if (paragraph.getSentenses().isEmpty()) {
            new SentenseParser().parse(input);
        }
        Sentense sentense = new Sentense();
        Pattern patternWords = Pattern.compile(RegexManager.getInstance().getProperty(RegexManager.REGEX_FOR_PART_OF_SENTENSE_WORD));
        Pattern patternSigns = Pattern.compile(RegexManager.getInstance().getProperty(RegexManager.REGEX_FOR_PART_OF_SENTENTSE_SIGN));
        for (Sentense s : paragraph.getSentenses()) {
            Matcher matcher = patternSigns.matcher(s.getSentense());
            while (matcher.find()) {
                PartOfSentense pos = new PartOfSentense();
                pos.setSign(matcher.group(0).charAt(0));
                sentense.addTextComponent(pos);
            }
            for (String st : patternSigns.split(s.getSentense())) {
                listSent.add(st);
            }
        }
        for (String s : listSent) {
            Matcher matcher = patternWords.matcher(s);
            while (matcher.find()) {
                PartOfSentense pos = new PartOfSentense();
                pos.setWord(matcher.group(0));
                sentense.addTextComponent(pos);
            }
        }
        log.info("Part of sentense is parsed");
    }
}
