/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.logic.parser;

import com.epam.course.entity.Paragraph;
import com.epam.course.entity.Sentense;
import com.epam.course.entity.Text;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;

/**
 *
 * @author notepad
 */
public class SentenseParser implements Parser {

    private final static Logger log = Logger.getLogger(SentenseParser.class);

    @Override
    public void parse(String input) {
        Text text = new Text();
        Paragraph paragraph = new Paragraph();
        if (text.getParagraphs().isEmpty()) {
            new ParagraphParser().parse(input);
        }
        List<Paragraph> listOfParagraphs = text.getParagraphs();
        Pattern pattern = Pattern.compile(RegexManager.getInstance().getProperty(RegexManager.REGEX_FOR_SENTENSE));
        for (Paragraph p : listOfParagraphs) {
            Matcher matcher = pattern.matcher(p.getParagraph());
            while (matcher.find()) {
                Sentense s = new Sentense();
                s.setSentense(matcher.group(0));
                p.addTextComponent(s);
            }
        }
        log.info("Sentense is parsed");
    }

}
