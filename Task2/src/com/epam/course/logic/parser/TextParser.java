/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.logic.parser;

import com.epam.course.entity.Text;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;

/**
 *
 * @author notepad
 */
public class TextParser implements Parser {

    private final static Logger log = Logger.getLogger(TextParser.class);

    @Override
    public void parse(String input) {
        Text text = new Text();
        if (text.getText().equals("")) {
            Pattern pattern = Pattern.compile(RegexManager.getInstance().getProperty(RegexManager.REGEX_FOR_TEXT));
            text.setText(pattern.split(input));
            log.info("Parsing of text is complete");
        }
    }
}
