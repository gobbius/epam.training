/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.logic.parser;

import java.util.ResourceBundle;

/**
 *
 * @author notepad
 */
public class RegexManager {

    private final static String BUNDLE_NAME = "regex";
    public final static String REGEX_FOR_TEXT = "regex.for.text";
    public final static String REGEX_FOR_PARAGRAPH = "regex.for.paragraph";
    public final static String REGEX_FOR_SENTENSE = "regex.for.sentense";
    public final static String REGEX_FOR_PART_OF_SENTENSE_WORD = "regex.for.part.of.sentense.word";
    public final static String REGEX_FOR_PART_OF_SENTENTSE_SIGN = "regex.for.part.of.sentense.sign";
    private static RegexManager instance;
    private ResourceBundle bundle;

    public static RegexManager getInstance() {
        if (instance == null) {
            instance = new RegexManager();
            instance.bundle = ResourceBundle.getBundle(BUNDLE_NAME);
        }
        return instance;
    }

    public String getProperty(String key) {
        return (String) instance.bundle.getObject(key);
    }
}
