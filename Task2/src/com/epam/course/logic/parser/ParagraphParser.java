/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.logic.parser;

import com.epam.course.entity.Paragraph;
import com.epam.course.entity.Text;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;

/**
 *
 * @author notepad
 */
public class ParagraphParser implements Parser {
private final static Logger log = Logger.getLogger(ParagraphParser.class);
    @Override
    public void parse(String input) {
        Text text = new Text();
        if (text.getParagraphs().isEmpty()) {
            new TextParser().parse(input);
        }
        String textToParse = text.getText();
        Pattern pattern = Pattern.compile(RegexManager.getInstance().getProperty(RegexManager.REGEX_FOR_PARAGRAPH));
        Matcher matcher = pattern.matcher(textToParse);
        while (matcher.find()) {
            Paragraph p = new Paragraph();
            p.setParagraph(matcher.group(0));
            text.addTextComponent(p);
        }
        log.info("Paragraph is parsed");
    }

}
