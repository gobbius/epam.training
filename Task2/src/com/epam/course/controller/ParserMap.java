/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.controller;

import com.epam.course.logic.parser.ParagraphParser;
import com.epam.course.logic.parser.Parser;
import com.epam.course.logic.parser.ParserEnum;
import com.epam.course.logic.parser.PartOfSentenseParser;
import com.epam.course.logic.parser.SentenseParser;
import com.epam.course.logic.parser.TextParser;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author notepad
 */
public class ParserMap {

    private static ParserMap instance;
    private static Map<ParserEnum, Parser> parserMap = new HashMap<ParserEnum, Parser>();

    private ParserMap() {
        parserMap.put(ParserEnum.PARSE_TEXT, new TextParser());
        parserMap.put(ParserEnum.PARSE_PARAGRAPH, new ParagraphParser());
        parserMap.put(ParserEnum.PARSE_SENTENSE, new SentenseParser());
        parserMap.put(ParserEnum.PARSE_PART_OF_SENTENSE, new PartOfSentenseParser());
    }

    public static ParserMap getInstance() {
        if (instance == null) {
            instance = new ParserMap();
        }
        return instance;
    }

    public Parser getProperty(ParserEnum elem) {
        return parserMap.get(elem);
    }
}
