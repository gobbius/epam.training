/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.controller;

import com.epam.course.logic.FindWordByLengthAction;
import com.epam.course.logic.parser.Parser;
import com.epam.course.logic.parser.ParserEnum;

/**
 *
 * @author notepad
 */
public class Controller {

    public void parseText(ParserEnum elem, String input, Object... arr) {
        Parser parser = ParserMap.getInstance().getProperty(elem);
        parser.parse(input);
        if (arr.length != 0) {
            if (arr[0].getClass() == Integer.class) {
                new FindWordByLengthAction().findWordByLength((int) arr[0]);
            }
        }
    }
}
