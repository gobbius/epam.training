/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author notepad
 */
public class Sentense implements TextComponent {

    private String sentense;
    private static List<PartOfSentense> partsOfSentenses = new ArrayList<PartOfSentense>();

    public Sentense() {
    }

    public List<PartOfSentense> getPartsOfSentenses() {
        return partsOfSentenses;
    }

    public String getSentense() {
        return sentense;
    }

    public static void setPartsOfSentenses(List<PartOfSentense> partsOfSentenses) {
        Sentense.partsOfSentenses = partsOfSentenses;
    }

    public void setSentense(String sentense) {
        this.sentense = sentense;
    }

    @Override
    public TextComponent getTextComponent(int index) {
        return partsOfSentenses.get(index);
    }

    @Override
    public void addTextComponent(TextComponent component) {
        partsOfSentenses.add((PartOfSentense) component);
    }

    @Override
    public String toString() {
        return this.getClass() + "\n" + this.sentense;
    }

}
