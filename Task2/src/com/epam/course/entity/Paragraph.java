/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author notepad
 */
public class Paragraph implements TextComponent {

    private String paragraph;
    private static List<Sentense> sentenses = new ArrayList<Sentense>();

    public Paragraph() {
    }

    public void setParagraph(String paragraph) {
        this.paragraph = paragraph;
    }

    public String getParagraph() {
        return paragraph;
    }

    public List<Sentense> getSentenses() {
        return sentenses;
    }

    @Override
    public TextComponent getTextComponent(int inrex) {
        return sentenses.get(inrex);
    }

    @Override
    public void addTextComponent(TextComponent component) {
        sentenses.add((Sentense) component);
    }

    @Override
    public String toString() {
        return this.getClass() + "\n" + paragraph;
    }

}
