/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.entity;

/**
 *
 * @author notepad
 */
public interface TextComponent {

    TextComponent getTextComponent(int index);

    void addTextComponent(TextComponent component);
}
