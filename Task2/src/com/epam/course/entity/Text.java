/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author notepad
 */
public class Text implements TextComponent {

    private static String text = "";
    private static List<Paragraph> paragraphs = new ArrayList<Paragraph>();

    public Text() {
    }

    public void setText(String[] input) {
        if (text.equals("")) {
            for (String s : input) {
                text += s;
            }
        }
    }

    public void setText(String input) {
        text = input;
    }

    public String getText() {
        return text;
    }

    public List<Paragraph> getParagraphs() {
        return paragraphs;
    }

    @Override
    public TextComponent getTextComponent(int index) {
        return paragraphs.get(index);
    }

    @Override
    public void addTextComponent(TextComponent component) {
        paragraphs.add((Paragraph) component);
    }

}
