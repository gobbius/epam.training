/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.entity;

/**
 *
 * @author notepad
 */
public class PartOfSentense implements TextComponent {

    private String word;
    private char sign;

    public PartOfSentense() {
    }

    public void setSign(char sign) {
        this.sign = sign;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public char getSign() {
        return sign;
    }

    public String getWord() {
        return word;
    }

    @Override
    public TextComponent getTextComponent(int inrex) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addTextComponent(TextComponent component) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        if (this.word == null) {
            return String.valueOf(this.sign);
        } else if (this.sign == 0) {
            return this.word;
        } else {
            return this.word + this.sign;
        }
    }

}
