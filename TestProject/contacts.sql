-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Сен 21 2015 г., 23:37
-- Версия сервера: 5.6.19-0ubuntu0.14.04.1
-- Версия PHP: 5.5.9-1ubuntu4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `contacts`
--

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `surname` varchar(20) NOT NULL,
  `login` varchar(20) NOT NULL,
  `e_mail` varchar(50) NOT NULL,
  `phone_number` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=41 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `login`, `e_mail`, `phone_number`) VALUES
(1, 'Justin', 'Wilson', 'jwilson0', 'jwilson0@goodreads.com', 6102852),
(2, 'Christopher', 'Richardson', 'crichardson1', 'crichardson1@icio.us', 3312312),
(3, 'Kenneth', 'Meyer', 'kmeyer2', 'kmeyer2@elpais.com', 7194036),
(4, 'Jose', 'Vasquez', 'jvasquez3', 'jvasquez3@addthis.com', 7365553),
(5, 'Jimmy', 'Greene', 'jgreene4', 'jgreene4@bluehost.com', 2478406),
(6, 'Howard', 'Weaver', 'hweaver5', 'hweaver5@miibeian.gov.cn', 9046969),
(7, 'Louis', 'Gibson', 'lgibson6', 'lgibson6@goodreads.com', 8805579),
(8, 'James', 'Phillips', 'jphillips7', 'jphillips7@nih.gov', 1662894),
(9, 'Jeremy', 'Fernandez', 'jfernandez8', 'jfernandez8@techcrunch.com', 6771878),
(10, 'Jeremy', 'Morrison', 'jmorrison9', 'jmorrison9@hibu.com', 9860765),
(11, 'Ronald', 'Fowler', 'rfowlera', 'rfowlera@bloglovin.com', 9307700),
(12, 'Frank', 'Carr', 'fcarrb', 'fcarrb@oaic.gov.au', 2638986),
(13, 'Douglas', 'Long', 'dlongc', 'dlongc@shareasale.com', 7880934),
(14, 'George', 'Morrison', 'gmorrisond', 'gmorrisond@soup.io', 5680502),
(15, 'Christopher', 'Freeman', 'cfreemane', 'cfreemane@tiny.cc', 7056261),
(16, 'Gerald', 'Burke', 'gburkef', 'gburkef@gizmodo.com', 9576882),
(17, 'Jeffrey', 'Carter', 'jcarterg', 'jcarterg@sourceforge.net', 1132932),
(18, 'Nicholas', 'Stevens', 'nstevensh', 'nstevensh@house.gov', 7703478),
(19, 'Alan', 'Schmidt', 'aschmidti', 'aschmidti@google.nl', 4289015),
(20, 'Brandon', 'Perkins', 'bperkinsj', 'bperkinsj@lulu.com', 8883650),
(21, 'Robin', 'Gilbert', 'rgilbert0', 'rgilbert0@artisteer.com', 1482393),
(22, 'Anthony', 'Williams', 'awilliams1', 'awilliams1@ycombinator.com', 3723651),
(23, 'Jack', 'Bell', 'jbell2', 'jbell2@mediafire.com', 9959624),
(24, 'Emily', 'Rogers', 'erogers3', 'erogers3@twitter.com', 6243098),
(25, 'Judith', 'Grant', 'jgrant4', 'jgrant4@cpanel.net', 3514799),
(26, 'William', 'Hicks', 'whicks5', 'whicks5@accuweather.com', 1374270),
(27, 'Cynthia', 'Fisher', 'cfisher6', 'cfisher6@blogspot.com', 1370793),
(28, 'Russell', 'Edwards', 'redwards7', 'redwards7@ehow.com', 3680515),
(29, 'Gregory', 'Scott', 'gscott8', 'gscott8@cloudflare.com', 8465767),
(30, 'Ralph', 'Garcia', 'rgarcia9', 'rgarcia9@sfgate.com', 2377525),
(31, 'Steve', 'Baker', 'sbakera', 'sbakera@ihg.com', 1592222),
(32, 'Arthur', 'Morales', 'amoralesb', 'amoralesb@cornell.edu', 8458120),
(33, 'Teresa', 'Black', 'tblackc', 'tblackc@constantcontact.com', 5333668),
(34, 'James', 'Wagner', 'jwagnerd', 'jwagnerd@mashable.com', 3836703),
(35, 'Eric', 'Sanders', 'esanderse', 'esanderse@jugem.jp', 7137887),
(36, 'Henry', 'Ramos', 'hramosf', 'hramosf@theatlantic.com', 514379),
(37, 'Cheryl', 'Fernandez', 'cfernandezg', 'cfernandezg@twitpic.com', 8518148),
(38, 'Gary', 'Davis', 'gdavish', 'gdavish@slate.com', 1248312),
(39, 'Anthony', 'Moore', 'amoorei', 'amoorei@dedecms.com', 7110099),
(40, 'Justin', 'Schmidt', 'jschmidtj', 'jschmidtj@springer.com', 6363943);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
