/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.job.controller.listner;

import com.test.job.dao.UserConnection;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 *
 * @author notepad
 */
public class ContextListner implements ServletContextListener {

    private final static String EXCEPTION_MESSAGE = "You have exception: ";

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        UserConnection.getInstance();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }

}
