/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.job.comparators;

import com.test.job.domain.User;
import java.util.Comparator;

/**
 *
 * @author notepad
 */
public class SurNameComparator implements Comparator<User> {

    @Override
    public int compare(User t, User t1) {
        return t.getSurName().length() - t1.getSurName().length();
    }

}
