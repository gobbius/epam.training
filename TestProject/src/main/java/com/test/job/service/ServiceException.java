/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.job.service;

import com.test.job.exception.ProjectException;

/**
 *
 * @author notepad
 */
public class ServiceException extends ProjectException {

    public ServiceException() {
    }

    public ServiceException(Throwable thrwbl) {
        super(thrwbl);
    }

}
