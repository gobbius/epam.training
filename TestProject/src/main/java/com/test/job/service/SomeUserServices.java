/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.job.service;

import com.test.job.comparators.EMailComparator;
import com.test.job.comparators.LoginComparator;
import com.test.job.comparators.NameComparator;
import com.test.job.comparators.PhoneNumberComparator;
import com.test.job.comparators.SurNameComparator;
import com.test.job.dao.CsvFileException;
import com.test.job.dao.DataBaseDaoException;
import com.test.job.dao.impl.UserDao;
import com.test.job.domain.User;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author notepad
 */
public class SomeUserServices {

    private final static Logger log = Logger.getLogger(SomeUserServices.class);
    private final static String NAME_COLUMN = "name";
    private final static String SUR_NAME_COLUMN = "surname";
    private final static String LOGIN_COLUMN = "login";
    private final static String E_MAIL_COLUMN = "e_mail";
    private final static String PHONE_NUMBER_COLUMN = "phone_number";
    private final static String USERS_ATTRIBUTE_NAME = "users";
    private final static String NUMBER_OF_PAGES_ATTRIBUTE_NAME = "numberOfPages";

    private final static UserDao userDao = new UserDao();

    public static void takeUsersFromTable(HttpServletRequest request, int offset, int recordsPerPage) throws ServiceException {
        List<User> allUsers = null;
        List<User> usersOnPage = null;
        int numberOfRecords = 0;
        int numberOfPages = 0;
        try {
            allUsers = userDao.readAllUsers();
            if (!allUsers.isEmpty()) {
                usersOnPage = allUsers.subList(offset, offset + recordsPerPage);
                numberOfRecords = allUsers.size();
                numberOfPages = (int) Math.ceil(numberOfRecords * 1 / recordsPerPage);
                request.setAttribute(NUMBER_OF_PAGES_ATTRIBUTE_NAME, numberOfPages);
                request.setAttribute(USERS_ATTRIBUTE_NAME, usersOnPage);
            } else {
                request.setAttribute(USERS_ATTRIBUTE_NAME, null);
            }
        } catch (DataBaseDaoException ex) {
            throw new ServiceException(ex);
        }
    }

    public static void importUsersFromCsv() throws ServiceException {
        List<User> users = null;
        try {
            users = userDao.takeUsers();
            userDao.insertUsers(users);
        } catch (CsvFileException | DataBaseDaoException ex) {
            throw new ServiceException(ex);
        }
    }

    public static void updateUsersInTable() throws ServiceException {
        List<User> usersFromTable = null;
        List<User> usersFromFile = null;
        List<User> usersForUpdate = new ArrayList<User>();
        List<User> usersForInsert = new ArrayList<User>();
        List<User> usersForDelete = new ArrayList<User>();
        try {
            usersFromTable = userDao.readAllUsers();
            usersFromFile = userDao.takeUsers();
        } catch (DataBaseDaoException | CsvFileException ex) {
            throw new ServiceException(ex);
        }
        if (!usersFromFile.isEmpty() && !usersFromTable.isEmpty()) {
            if (usersFromFile.size() == usersFromTable.size()) {
                for (User userFromFile : usersFromFile) {
                    for (User userFromTable : usersFromTable) {
                        if (userFromFile.getId() == userFromTable.getId()) {
                            if (!userFromFile.equals(userFromTable)) {
                                usersForUpdate.add(userFromFile);
                            }
                        }
                    }
                }
            } else if (usersFromFile.size() > usersFromTable.size()) {
                for (User userFromFile : usersFromFile) {
                    for (User userFromTable : usersFromTable) {
                        if (usersFromFile.indexOf(userFromFile) < usersFromTable.size()) {
                            if (userFromFile.getId() == userFromTable.getId()) {
                                if (!userFromFile.equals(userFromTable)) {
                                    usersForUpdate.add(userFromFile);
                                }
                            }
                        } else {
                            usersForInsert.add(userFromFile);
                        }
                    }
                }
            } else if (usersFromFile.size() < usersFromTable.size()) {
                for (User userFromTable : usersFromTable) {
                    for (User userFromFile : usersFromFile) {
                        if (usersFromTable.indexOf(userFromTable) < usersFromFile.size()) {
                            if (userFromFile.getId() == userFromTable.getId()) {
                                if (!userFromFile.equals(userFromTable)) {
                                    usersForUpdate.add(userFromFile);
                                }
                            }
                        } else {
                            usersForDelete.add(userFromTable);
                        }
                    }
                }
            }
        }
        try {
            if (!usersForInsert.isEmpty()) {
                userDao.insertUsers(usersForInsert);
            } else if (!usersForUpdate.isEmpty()) {
                userDao.updateUsers(usersForUpdate);
            } else if (!usersForDelete.isEmpty()) {
                userDao.deleteUsers(usersForDelete);
            }
        } catch (DataBaseDaoException ex) {
            throw new ServiceException(ex);
        }
    }

    public static void sortByColumn(HttpServletRequest request, int offset, int recordsPerPage, String columnName) throws ServiceException {
        List<User> allUsers = null;
        List<User> usersOnPage = null;
        int numberOfRecords = 0;
        int numberOfPages = 0;
        try {
            allUsers = userDao.readAllUsers();
        } catch (DataBaseDaoException ex) {
            throw new ServiceException(ex);
        }
        if (!allUsers.isEmpty()) {
            switch (columnName) {
                case NAME_COLUMN: {
                    Collections.sort(allUsers, new NameComparator());
                    break;
                }
                case SUR_NAME_COLUMN: {
                    Collections.sort(allUsers, new SurNameComparator());
                    break;
                }
                case LOGIN_COLUMN: {
                    Collections.sort(allUsers, new LoginComparator());
                    break;
                }
                case E_MAIL_COLUMN: {
                    Collections.sort(allUsers, new EMailComparator());
                    break;
                }
                case PHONE_NUMBER_COLUMN: {
                    Collections.sort(allUsers, new PhoneNumberComparator());
                    break;
                }
            }
            usersOnPage = allUsers.subList(offset, offset + recordsPerPage);
            numberOfRecords = allUsers.size();
            numberOfPages = (int) Math.ceil(numberOfRecords * 1 / recordsPerPage);
            request.setAttribute(NUMBER_OF_PAGES_ATTRIBUTE_NAME, numberOfPages);
            request.setAttribute(USERS_ATTRIBUTE_NAME, usersOnPage);
        } else {
            request.setAttribute(USERS_ATTRIBUTE_NAME, null);
        }
    }
}
