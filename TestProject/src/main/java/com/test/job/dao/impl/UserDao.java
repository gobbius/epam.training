/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.job.dao.impl;

import org.apache.log4j.Logger;
import com.test.job.dao.CsvFileException;
import com.test.job.dao.DataBaseDaoException;
import com.test.job.dao.UserConnection;
import com.test.job.domain.User;
import com.test.job.manager.ConfigurationManager;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.logging.Level;

/**
 *
 * @author notepad
 */
public class UserDao {

    private final static String DELETING_QUERY = "DELETE FROM `users` WHERE `id` = ?";
    private final static String UPDATING_QUERY = "UPDATE `users` SET `name` = ?, `surname` = ?, `login` = ?, `e_mail` = ?, `phone_number` = ? WHERE `id` = ?";
    private final static String SELECTING_QUERY = "SELECT * FROM `users`";
    private final static String INSERTING_QUERY = "INSERT IGNORE INTO `users` (`id`,`name`, `surname`, `login`,`e_mail`, `phone_number`) VALUES (?, ?, ?, ?, ?, ?)";
    private final static String FILE_DELIMITER = ",";
    private final static String FILE_PATH = ConfigurationManager.getInstance().getConfig(ConfigurationManager.FILE_PATH);
    private final static String ID = "id";
    private final static String NAME = "name";
    private final static String SUR_NAME = "surname";
    private final static String USER_LOGIN = "login";
    private final static String E_MAIL = "e_mail";
    private final static String PHONE_NUMBER = "phone_number";

    public void insertUsers(List<User> users) throws DataBaseDaoException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        Lock connectionLock = UserConnection.getInstance().getLock();
        boolean isLocked = false;
        int[] resultOfInserting = null;
        try {
            isLocked = connectionLock.tryLock(5, TimeUnit.SECONDS);
            if (isLocked) {
                connection = UserConnection.getInstance().getConnection();
                preparedStatement = connection.prepareStatement(INSERTING_QUERY);
                for (User user : users) {
                    preparedStatement.setInt(1, user.getId());
                    preparedStatement.setString(2, user.getName());
                    preparedStatement.setString(3, user.getSurName());
                    preparedStatement.setString(4, user.getLogin());
                    preparedStatement.setString(5, user.geteMail());
                    preparedStatement.setInt(6, user.getPhoneNumber());
                    preparedStatement.addBatch();
                    preparedStatement.clearParameters();
                }
                resultOfInserting = preparedStatement.executeBatch();
                if (resultOfInserting.length > 0) {
                    if (!connection.getAutoCommit()) {
                        connection.commit();
                    }
                }
            }
        } catch (ClassNotFoundException | SQLException | InterruptedException ex) {
            throw new DataBaseDaoException(ex);
        } finally {
            if (isLocked) {
                connectionLock.unlock();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                throw new DataBaseDaoException(ex);
            }
        }
    }

    /**
     * Read users from database's table
     *
     * @return
     * @throws DataBaseDaoException
     */
    public List<User> readAllUsers() throws DataBaseDaoException {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        List<User> users = new ArrayList<User>();
        User user = null;
        try {
            connection = UserConnection.getInstance().getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SELECTING_QUERY);
            while (resultSet.next()) {
                user = new User(resultSet.getInt(ID), resultSet.getString(NAME), resultSet.getString(SUR_NAME), resultSet.getString(USER_LOGIN), resultSet.getString(E_MAIL), resultSet.getInt(PHONE_NUMBER));
                users.add(user);
            }
        } catch (ClassNotFoundException | SQLException ex) {
            throw new DataBaseDaoException(ex);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                throw new DataBaseDaoException(ex);
            }
        }
        return users;
    }

    public void updateUsers(List<User> users) throws DataBaseDaoException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int[] resultOfUpdating = null;
        try {
            connection = UserConnection.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(UPDATING_QUERY);
            for (User user : users) {
                preparedStatement.setString(1, user.getName());
                preparedStatement.setString(2, user.getSurName());
                preparedStatement.setString(3, user.getLogin());
                preparedStatement.setString(4, user.geteMail());
                preparedStatement.setInt(5, user.getPhoneNumber());
                preparedStatement.setInt(6, user.getId());
                preparedStatement.addBatch();
                preparedStatement.clearParameters();
            }
            resultOfUpdating = preparedStatement.executeBatch();
            if (resultOfUpdating.length > 0) {
                if (!connection.getAutoCommit()) {
                    connection.commit();
                }
            }
        } catch (SQLException | ClassNotFoundException ex) {
            throw new DataBaseDaoException(ex);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                throw new DataBaseDaoException(ex);
            }
        }

    }

    public void deleteUsers(List<User> users) throws DataBaseDaoException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int[] resultOfUpdating = null;
        try {
            connection = UserConnection.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(DELETING_QUERY);
            for (User user : users) {
                preparedStatement.setInt(1, user.getId());
                preparedStatement.addBatch();
                preparedStatement.clearParameters();
            }
            resultOfUpdating = preparedStatement.executeBatch();
            if (resultOfUpdating.length > 0) {
                if (!connection.getAutoCommit()) {
                    connection.commit();
                }
            }
        } catch (SQLException | ClassNotFoundException ex) {
            throw new DataBaseDaoException(ex);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                throw new DataBaseDaoException(ex);
            }
        }
    }

    /**
     * This method take users from csv file
     *
     * @param filePath
     * @return
     */
    public List<User> takeUsers() throws CsvFileException {
        List<User> users = new ArrayList<User>();
        User user = null;
        BufferedReader bufferedReader = null;
        String resultLine = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(FILE_PATH));
            while ((resultLine = bufferedReader.readLine()) != null) {
                String[] userLine = resultLine.split(FILE_DELIMITER);
                user = new User();
                user.setId(Integer.parseInt(userLine[0]));
                user.setName(userLine[1]);
                user.setSurName(userLine[2]);
                user.setLogin(userLine[3]);
                user.seteMail(userLine[4]);
                user.setPhoneNumber(Integer.parseInt(userLine[5]));
                users.add(user);
                user = null;
            }
        } catch (FileNotFoundException ex) {
            throw new CsvFileException(ex);
        } catch (IOException ex) {
            throw new CsvFileException(ex);
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException ex) {
                throw new CsvFileException(ex);
            }
        }
        return users;
    }
}
