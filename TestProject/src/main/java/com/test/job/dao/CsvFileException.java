/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.job.dao;

import com.test.job.exception.ProjectException;

/**
 *
 * @author notepad
 */
public class CsvFileException extends ProjectException {

    public CsvFileException() {
    }

    public CsvFileException(Throwable thrwbl) {
        super(thrwbl);
    }

}
