/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.job.dao;

import com.test.job.exception.ProjectException;

/**
 *
 * @author notepad
 */
public class DataBaseDaoException extends ProjectException {

    public DataBaseDaoException() {
    }

    public DataBaseDaoException(Throwable thrwbl) {
        super(thrwbl);
    }

}
