/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.job.dao;

import com.test.job.manager.ConfigurationManager;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.log4j.Logger;

/**
 *
 * @author notepad
 */
public class UserConnection {

    private final static Logger log = Logger.getLogger(UserConnection.class);
    private Lock lock;
    private static Connection connection;
    private static final String DRIVER_NAME = ConfigurationManager.getInstance().getConfig(ConfigurationManager.DATABASE_DRIVER_NAME);
    private static final String URL = ConfigurationManager.getInstance().getConfig(ConfigurationManager.DATABASE_URL);
    private static final String LOGIN = ConfigurationManager.getInstance().getConfig(ConfigurationManager.DATABASE_LOGIN);
    private static final String PASSWORD = ConfigurationManager.getInstance().getConfig(ConfigurationManager.DATABASE_PASSWORD);
    private final static UserConnection instance = new UserConnection();

    private UserConnection() {
        try {
            Class.forName(DRIVER_NAME);
            connection = DriverManager.getConnection(URL, LOGIN, PASSWORD);
            lock = new ReentrantLock();
        } catch (ClassNotFoundException | SQLException ex) {
            log.error(ex.getMessage());
        }
    }

    public void closeConnection() throws SQLException {
        connection.close();
    }

    public Connection getConnection() throws SQLException, ClassNotFoundException {
        if (connection == null || connection.isClosed()) {
            connection = DriverManager.getConnection(URL, LOGIN, PASSWORD);
        }
        return connection;
    }

    public Lock getLock() {
        return lock;
    }

    public static UserConnection getInstance() {
        return instance;
    }
}
