/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.job.command;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author notepad
 */
public interface ICommand {

    public String execute(HttpServletRequest request) throws CommandException;
}
