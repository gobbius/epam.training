/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.job.command;

import com.test.job.command.impl.CreateCommand;
import com.test.job.command.impl.RedirectCommand;
import com.test.job.command.impl.ShowCommand;
import com.test.job.command.impl.SortingCommand;
import com.test.job.command.impl.UpdateCommand;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author notepad
 */
public class CommandFactory {

    private final static String CREATE_COMMAND = "create";
    private final static String UPDATE_COMMAND = "update";
    private final static String SHOW_COMMAND = "show";
    private final static String REDIRECT_COMMAND = "redirect";
    private final static String SORT_COMMAND = "sort";
    private final static CommandFactory instance = new CommandFactory();
    private Map<String, ICommand> commands = new HashMap<String, ICommand>();

    private CommandFactory() {
        commands.put(CREATE_COMMAND, new CreateCommand());
        commands.put(UPDATE_COMMAND, new UpdateCommand());
        commands.put(SHOW_COMMAND, new ShowCommand());
        commands.put(REDIRECT_COMMAND, new RedirectCommand());
        commands.put(SORT_COMMAND, new SortingCommand());
    }

    public static CommandFactory getInstance() {
        return instance;
    }

    public ICommand getCommand(String commandName) {
        return commands.get(commandName);
    }
}
