/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.job.command.impl;

import com.test.job.command.CommandException;
import com.test.job.command.ICommand;
import com.test.job.manager.PageManager;
import com.test.job.service.ServiceException;
import com.test.job.service.SomeUserServices;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author notepad
 */
public class UpdateCommand implements ICommand {

    private final static String IMPORT_PAGE = PageManager.getInstance().getPage(PageManager.IMPORT_PAGE);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String forwardPage = null;

        try {
            SomeUserServices.updateUsersInTable();
        } catch (ServiceException ex) {
            throw new CommandException(ex);
        }
        forwardPage = IMPORT_PAGE;
        return forwardPage;
    }

}
