/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.job.command.impl;

import com.test.job.command.CommandException;
import com.test.job.command.ICommand;
import com.test.job.manager.PageManager;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author notepad
 */
public class RedirectCommand implements ICommand {

    private final static String INDEX_PAGE = PageManager.getInstance().getPage(PageManager.INDEX_PAGE);
    private final static String IMPORT_PAGE = PageManager.getInstance().getPage(PageManager.IMPORT_PAGE);
    private final static String WAY_OF_REDIRECTING_PARAMETER_NAME = "way_of_redirecting";
    private final static String MENU_WAY = "menu";
    private final static String IMPORT_WAY = "import";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String forwardPage = null;
        String wayOfRedirecting = request.getParameter(WAY_OF_REDIRECTING_PARAMETER_NAME);
        switch (wayOfRedirecting) {
            case MENU_WAY: {
                forwardPage = INDEX_PAGE;
                break;
            }
            case IMPORT_WAY: {
                forwardPage = IMPORT_PAGE;
                break;
            }
        }
        return forwardPage;
    }

}
