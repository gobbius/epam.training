/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.job.command.impl;

import com.test.job.command.CommandException;
import com.test.job.command.ICommand;
import com.test.job.manager.PageManager;
import com.test.job.service.ServiceException;
import com.test.job.service.SomeUserServices;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author notepad
 */
public class SortingCommand implements ICommand {

    private final static String PAGE_PARAMETER = "page";
    private final static String CURRENT_PAGE_PARAMETER = "currentPage";
    private final static String COLUMN_PARAMETER = "column";
    private final static String CONTACTS_PAGE = PageManager.getInstance().getPage(PageManager.CONTACTS_PAGE);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String forwardPage = null;
        int currentPage = 1;
        int recordsPerPage = 10;
        String column = null;
        if (request.getParameter(PAGE_PARAMETER) != null) {
            currentPage = Integer.parseInt(request.getParameter(PAGE_PARAMETER));
        }
        if (request.getParameter(COLUMN_PARAMETER) != null) {
            column = request.getParameter(COLUMN_PARAMETER);
        }
        try {
            SomeUserServices.sortByColumn(request, (currentPage - 1) * recordsPerPage, recordsPerPage, column);
        } catch (ServiceException ex) {
            throw new CommandException(ex);
        }
        request.setAttribute(CURRENT_PAGE_PARAMETER, currentPage);
        forwardPage = CONTACTS_PAGE;
        return forwardPage;
    }
}
