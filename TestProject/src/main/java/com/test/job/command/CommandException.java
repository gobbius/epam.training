/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.job.command;

import com.test.job.exception.ProjectException;

/**
 *
 * @author notepad
 */
public class CommandException extends ProjectException {

    public CommandException() {
    }

    public CommandException(Throwable thrwbl) {
        super(thrwbl);
    }

}
