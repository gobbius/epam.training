/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.job.manager;

import java.util.ResourceBundle;

/**
 *
 * @author notepad
 */
public class ConfigurationManager {

    private ResourceBundle bundle;
    private static final String BUNDLE_NAME = "configs";
    public static final String DATABASE_DRIVER_NAME = "driver.name";
    public static final String DATABASE_URL = "url";
    public static final String DATABASE_LOGIN = "login";
    public static final String DATABASE_PASSWORD = "password";
    public final static String FILE_PATH = "file.path";
    private final static ConfigurationManager instance = new ConfigurationManager();

    private ConfigurationManager() {
        bundle = ResourceBundle.getBundle(BUNDLE_NAME);
    }

    public static ConfigurationManager getInstance() {
        return instance;
    }

    public String getConfig(String configName) {
        return (String) bundle.getObject(configName);
    }

}
