/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.job.manager;

import java.util.ResourceBundle;

/**
 *
 * @author notepad
 */
public class PageManager {

    public final static String ERROR_PAGE = "error.page";
    public final static String IMPORT_PAGE = "import.page";
    public final static String CONTACTS_PAGE = "contacts.page";
    public final static String INDEX_PAGE = "index.page";
    private ResourceBundle bundle;
    private static final String BUNDLE_NAME = "pages";

    private final static PageManager instance = new PageManager();

    private PageManager() {
        bundle = ResourceBundle.getBundle(BUNDLE_NAME);
    }

    public static PageManager getInstance() {
        return instance;
    }

    public String getPage(String pageName) {
        return (String) bundle.getObject(pageName);
    }

}
