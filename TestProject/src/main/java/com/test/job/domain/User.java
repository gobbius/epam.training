/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.job.domain;

/**
 *
 * @author notepad
 */
public class User {

    private int id;
    private String name;
    private String surName;
    private String login;
    private String eMail;
    private int phoneNumber;

    public User() {
    }

    public User(int id, String name, String surName, String login, String eMail, int phoneNumber) {
        this.id = id;
        this.name = name;
        this.surName = surName;
        this.login = login;
        this.eMail = eMail;
        this.phoneNumber = phoneNumber;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }

    public String getSurName() {
        return surName;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public String geteMail() {
        return eMail;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.id;
        result = prime * result + this.phoneNumber;
        result = prime * result + this.login.hashCode();
        result = prime * result + this.name.hashCode();
        result = prime * result + this.surName.hashCode();
        result = prime * result + this.eMail.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (this.getClass() != o.getClass()) {
            return false;
        }
        User other = (User) o;
        if (this.id != other.id) {
            return false;
        }
        if (this.phoneNumber != other.phoneNumber) {
            return false;
        }
        if (!this.name.equals(other.name)) {
            return false;
        }

        if (!this.eMail.equals(other.eMail)) {
            return false;
        }
        if (!this.login.equals(other.login)) {
            return false;
        }
        if (!this.surName.equals(other.surName)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.name + " " + this.surName + " " + this.login + " " + this.phoneNumber + " " + this.eMail;
    }
}
