<%-- 
    Document   : contacts
    Created on : Sep 18, 2015, 2:47:08 PM
    Author     : notepad
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Контакты</title>
    </head>
    <body>
        <div style="margin-left: 40%;margin-top: 10%;">
            <table border="1">
                <th>
                <form action="testproject">
                    <input type="submit" value="Имя" />
                    <input type="hidden" name="column" value="name" />
                    <input type="hidden" name="command" value="sort" />
                </form>
                </th>
                <th>
                <form action="testproject">
                    <input type="submit" value="Отчество" />
                    <input type="hidden" name="column" value="surname" />
                    <input type="hidden" name="command" value="sort" />
                </form>
                </th>
                <th>
                <form action="testproject">
                    <input type="submit" value="Логин" />
                    <input type="hidden" name="column" value="login" />
                    <input type="hidden" name="command" value="sort" />
                </form>
                </th>
                <th>
                <form action="testproject">
                    <input type="submit" value="E-mail" />
                    <input type="hidden" name="column" value="e_mail" />
                    <input type="hidden" name="command" value="sort" />
                </form>
                </th>
                <th>
                <form action="testproject">
                    <input type="submit" value="Телефон" />
                    <input type="hidden" name="column" value="phone_number" />
                    <input type="hidden" name="command" value="sort" />
                </form>
                </th>
                <c:forEach var="user" items="${users}">
                    <tr>
                        <td>${user.name}</td>
                        <td>${user.surName}</td> 
                        <td>${user.login}</td> 
                        <td>${user.eMail}</td> 
                        <td>${user.phoneNumber}</td> 
                    </tr>
                </c:forEach>
            </table>
            <table border="1" cellpadding="5" cellspacing="5">
                <tr>
                    <c:forEach begin="1" end="${numberOfPages}" var="i">
                        <c:choose>
                            <c:when test="${currentPage eq i}">
                                <td>
                                    <form action="testproject">
                                        <input type="submit" value="${i}" />
                                        <input type="hidden" name="page" value="${i}" />
                                        <input type="hidden" name="command" value="show" />
                                    </form>
                                </td>
                            </c:when>
                            <c:otherwise>
                                <td>
                                    <form action="testproject">
                                        <input type="submit" value="${i}" />
                                        <input type="hidden" name="page" value="${i}" />
                                        <input type="hidden" name="command" value="show" />
                                    </form>
                                </td>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </tr>
            </table>
            <form action="testproject">
                <input style="width: 100px;margin-top: 10px;" type="submit" value="Назад в меню" />
                <input type="hidden" name="command" value="redirect" />
                <input type="hidden" name="way_of_redirecting" value="menu" />
            </form>
        </div>
    </body>
</html>
