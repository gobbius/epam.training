/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.freecashbox;

/**
 *
 * @author notepad
 */
public class RestaurantException extends Exception {

    public RestaurantException(String string) {
        super(string);
    }

    public RestaurantException(Throwable thrwbl) {
        super(thrwbl);
    }

    public RestaurantException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

}
