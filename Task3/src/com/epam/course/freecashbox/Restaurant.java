/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.freecashbox;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 *
 * @author notepad
 */
public class Restaurant {

    private BlockingQueue<CashBox> cashBoxList;
    private Map<Client, CashBox> usedCashBoxes;

    public Restaurant(int amountOfCashBoxes) {
        cashBoxList = new ArrayBlockingQueue<CashBox>(amountOfCashBoxes);
        for (int i = 0; i < amountOfCashBoxes; i++) {
            cashBoxList.add(new CashBox(i));
        }
        usedCashBoxes = new HashMap<Client, CashBox>();
    }

    public void lockCashBox(Client client) {
        usedCashBoxes.put(client, client.getCashBox());
    }

    public boolean unlockCashBox(Client client) {
        CashBox cashBox = usedCashBoxes.get(client);
        try {
            cashBoxList.put(cashBox);
            usedCashBoxes.remove(client);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    public CashBox getCashBox() throws RestaurantException, InterruptedException {
        if (cashBoxList.isEmpty()) {
            throw new RestaurantException("list of cash boxes is empty, fill it");
        } else {
            return cashBoxList.take();
        }
    }

    public BlockingQueue<CashBox> getCashBoxList() {
        return cashBoxList;
    }

}
