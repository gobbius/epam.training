/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.freecashbox;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import org.apache.log4j.Logger;

/**
 *
 * @author notepad
 */
public class Client extends Thread {

    private final static Logger log = Logger.getLogger(Client.class);
    private volatile boolean stopThread = false;
    private Restaurant restaurant;
    private CashBox cashBox;

    public Client(Restaurant restaurant, String name) {
        this.restaurant = restaurant;
        this.setName(name);

    }

    public void MakeAnOrder(CashBox cashBox) throws InterruptedException {//метод позволяющий клиенту сделать заказ в кассе, блокирует кассу во время заказа и отдает блокировку когда заказ сделан
        restaurant.lockCashBox(this);
        Lock cashBoxLock = cashBox.getLock();
        boolean isLocked = false;
        try {
            isLocked = cashBoxLock.tryLock(10, TimeUnit.SECONDS);
            if (isLocked) {
                Thread.sleep(1000);
                log.info("client " + this.getName() + " is making order");
                Client clientToRemove = this.cashBox.getQueue().removeHead();
                log.info("client " + clientToRemove.getName() + " is removed from queue");
            }
        } finally {
            if (isLocked) {
                cashBoxLock.unlock();
                restaurant.unlockCashBox(this);
            }
        }
    }

    private void away() throws InterruptedException, RestaurantException {//клиент не находится в ресторане, он где-то шляется
        log.info(this.getName() + " is away");
        Thread.sleep(1000);
    }

    private void inQueue() throws InterruptedException, RestaurantException {//клиент стоит в очереди в кассу и ждет пока он станет 1ым в очереди, чтобы сделать заказ
        if (this.cashBox == null) {
            this.cashBox = restaurant.getCashBox();
        }
        log.info("client " + this.getName() + " is queue in " + this.cashBox.getId() + " cash box");
        if (!cashBox.getQueue().clients.contains(this)) {
            cashBox.getQueue().addClient(this);
        }
        if (new Random().nextInt(1000) > 500) {
            changeQueue();
        }
        if (!cashBox.getQueue().isEmpty()) {
            if (cashBox.getQueue().HeadOfQueue() == this) {
                this.MakeAnOrder(cashBox);
            } else {
                Thread.sleep(1000);
                
            }
        }
    }

    private void changeQueue() throws RestaurantException, InterruptedException {//клиент неожиданно решил сменить кассу, он можно встать в очередь в любую другую кассу, он выходит из текущей очереди и становится в другую inQueue()
        log.info("client " + this.getName() + " want to change cash box");
        CashBox cashBoxBeforeChange = this.getCashBox();
        boolean changeQ = false;
        boolean result = this.cashBox.getQueue().removeClient(this);
        if (result == true) {
            while (!changeQ) {
                for (CashBox cb : restaurant.getCashBoxList()) {
                    if (this.cashBox.getId() != cb.getId()) {
                        this.cashBox = cb;
                        this.cashBox.getQueue().addClient(this);
                        changeQ = true;
                        log.info("client " + this.getName() + " cash box is changed to " + this.cashBox.getId() + " from " + cashBoxBeforeChange.getId());
                    }
                }
            }
        }
    }

    @Override
    public void run() {
        while (!stopThread) {
            try {
                inQueue();
                away();
            } catch (InterruptedException ex) {

            } catch (RestaurantException ex) {

            }

        }
    }

    public void stopThread() {
        stopThread = true;
    }

    public void setCashBox(CashBox cashBox) {
        this.cashBox = cashBox;
    }

    public CashBox getCashBox() {
        return cashBox;
    }

    @Override
    public String toString() {
        return this.getCashBox() + this.getName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        Client other = (Client) o;
        if (!this.getName().equals(other.getName())) {
            return false;
        }
        return true;
    }

}
