/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.freecashbox;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author notepad
 */
public class ClientsQueue {

    BlockingQueue<Client> clients;

    public ClientsQueue() {
        clients = new ArrayBlockingQueue<Client>(5);
    }

    public void addClient(Client client) throws InterruptedException {
        clients.put(client);
    }

    public boolean removeClient(Client client) {
        return clients.remove(client);
    }

    public Client removeHead() throws InterruptedException {
        return clients.poll(10, TimeUnit.SECONDS);
    }

    public BlockingQueue<Client> getClients() {
        return clients;
    }

    public int getSize() {
        return clients.size();
    }

    public Client HeadOfQueue() {
        return clients.element();
    }

    public boolean isEmpty() {
        return clients.isEmpty();
    }
}
