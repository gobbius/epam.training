/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.freecashbox;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author notepad
 */
public class CashBox {

    private int id;
    private Lock lock;
    private ClientsQueue queue;

    public CashBox(int id) {
        this.id = id;
        queue = new ClientsQueue();
        lock = new ReentrantLock();
    }

    public int getId() {
        return id;
    }

    public Lock getLock() {
        return lock;
    }

    public ClientsQueue getQueue() {
        return queue;
    }

    @Override
    public String toString() {
        return super.toString(); //To change body of generated methods, choose Tools | Templates.
    }

}
