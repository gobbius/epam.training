/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.view;

import com.epam.course.freecashbox.Client;
import com.epam.course.freecashbox.Restaurant;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author notepad
 */
public class Main {

    public static void main(String[] args) {
        int amountOfCashBoxes = 3;
        Restaurant restaurant = new Restaurant(amountOfCashBoxes);
        Client client = new Client(restaurant, "One");
        Client client1 = new Client(restaurant, "Two");
        Client client2 = new Client(restaurant, "Three");
        Client client3 = new Client(restaurant, "Four");
        Client client4 = new Client(restaurant, "Five");
        Client client5 = new Client(restaurant, "Six");
        client.start();
        client1.start();
        client2.start();
        client3.start();
        client4.start();
        client5.start();
        try {
            Thread.sleep(4000);
            client.stopThread();
            client1.stopThread();
            client2.stopThread();
            client3.stopThread();
            client4.stopThread();
            client5.stopThread();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}
