/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.controller;

import com.epam.course.logic.ActionEnum;
import com.epam.course.logic.ActionMap;
import com.epam.course.logic.TarifAction;

/**
 *
 * @author notepad
 */
public class Controller {

    private final ActionMap actionMap = ActionMap.getInstance();

    public void build(ActionEnum elem, Object... args) {
        TarifAction action = actionMap.getProperty(elem);
        action.doAction(args);
    }
}
