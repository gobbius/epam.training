/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.logic;

import com.epam.course.builder.VelcomTarifDirector;
import com.epam.course.entity.Tarif;
import java.util.Collections;
import org.apache.log4j.Logger;

/**
 *
 * @author notepad
 */
public class ActionSortByLicenseFee implements TarifAction {

    private Logger log = Logger.getLogger(ActionSortByLicenseFee.class);

    @Override
    public void doAction(Object... args) {
        VelcomTarifDirector director = new VelcomTarifDirector();
        if (director.getBuilder().getTarifList().isEmpty()) {
            log.error("list of tarifs is empty");
            System.err.println("Before sorting construct atleast one of tarifs");
        } else {
            Collections.sort(director.getBuilder().getTarifList().getListTarif(), new LicenseFeeComparator());
            log.info("Sorting is complete!");
        }
    }

}
