/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.logic;

import com.epam.course.builder.VelcomTarifDirector;
import com.epam.course.entity.Tarif;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author notepad
 */
public class ActionCountClients implements TarifAction {

    private Logger log = Logger.getLogger(ActionCountClients.class);

    @Override
    public void doAction(Object... args) {
        VelcomTarifDirector director = new VelcomTarifDirector();
        if (director.getBuilder().getTarifList().isEmpty()) {
            log.error("list of tarifs is empty");
            System.err.println("Before counting construct atleast one of tarif");
        } else {
            List<Tarif> list = director.getBuilder().getTarifList().getListTarif();
            int amountOfClients = 0;
            for (Tarif t : list) {
                amountOfClients = amountOfClients + t.getCountOfClients();
            }
            log.info("Counting is complete");
        }
    }

}
