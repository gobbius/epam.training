/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.logic;

import com.epam.course.builder.VelcomTarifDirector;
import com.epam.course.entity.Shoes;
import org.apache.log4j.Logger;

/**
 *
 * @author notepad
 */
public class ActionConstructShoes implements TarifAction {

    private Logger log = Logger.getLogger(ActionConstructShoes.class);

    @Override
    public void doAction(Object... args) {
        VelcomTarifDirector director = new VelcomTarifDirector();
        String name = null;
        double licenseFee = 0;
        int countOfClients = 0;
        int amountOfMms = 0;
        for (Object obj : args) {
            if (obj.getClass() == String.class && name == null) {
                name = (String) obj;
            } else if (obj.getClass() == Integer.class && countOfClients == 0) {
                countOfClients = (int) obj;
            } else if (obj.getClass() == Integer.class && countOfClients != 0 && amountOfMms == 0) {
                amountOfMms = (int) obj;
            } else if (obj.getClass() == Double.class) {
                licenseFee = (double) obj;
            }
        }
        director.constructTarifList(new Shoes(name, licenseFee, countOfClients, amountOfMms));
        log.info("Constructing of shoes is complete!");
    }

}
