/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.logic;

import com.epam.course.builder.VelcomTarifDirector;
import com.epam.course.entity.Smart;
import org.apache.log4j.Logger;

/**
 *
 * @author notepad
 */
public class ActionConstructSmart implements TarifAction {

    private Logger log = Logger.getLogger(ActionConstructSmart.class);

    @Override
    public void doAction(Object... args) {
        VelcomTarifDirector director = new VelcomTarifDirector();
        String name = null;
        double licenseFee = 0;
        int countOfClients = 0;
        int minutesInAllWebs = 0;
        for (Object obj : args) {
            if (obj.getClass() == String.class && name == null) {
                name = (String) obj;
            } else if (obj.getClass() == Integer.class && countOfClients == 0) {
                countOfClients = (int) obj;
            } else if (obj.getClass() == Integer.class && countOfClients != 0 && minutesInAllWebs == 0) {
                minutesInAllWebs = (int) obj;
            } else if (obj.getClass() == Double.class) {
                licenseFee = (double) obj;
            }
        }
        director.constructTarifList(new Smart(name, licenseFee, countOfClients, minutesInAllWebs));
        log.info("Constructing of smart is complete!");
    }

}
