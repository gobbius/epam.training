/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.logic;

import com.epam.course.entity.Tarif;
import java.util.Comparator;

/**
 *
 * @author notepad
 */
public class LicenseFeeComparator implements Comparator<Tarif>{

    @Override
    public int compare(Tarif one, Tarif two) {
        return (int) (one.getLicenseFee() - two.getLicenseFee());
    }
    
}
