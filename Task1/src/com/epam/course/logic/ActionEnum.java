/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.logic;

/**
 *
 * @author notepad
 */
public enum ActionEnum {

    SORT,
    FIND,
    FIND_CLIENTS,
    FIND_FEE,
    FIND_NAME,
    COUNT,
    CONSTRUCT_SMART,
    CONSTRUCT_SHOES,
    CONSTRUCT_BUSSINES
}
