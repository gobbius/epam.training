/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.logic;

import com.epam.course.builder.VelcomTarifDirector;
import com.epam.course.entity.Tarif;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author notepad
 */
public class ActionFindTarif implements TarifAction {

    private  Logger log = Logger.getLogger(ActionFindTarif.class);

    @Override
    public void doAction(Object... args) {
        VelcomTarifDirector director = new VelcomTarifDirector();
        if (director.getBuilder().getTarifList().isEmpty()) {
            log.error("list of tarifs is empty");
        } else {
            ActionEnum elem = null;
            double licenseFee = 0;
            int amountOfClients = 0;
            String name = null;
            for (Object obj : args) {
                if (obj.getClass() == String.class && name == null) {
                    name = (String) obj;
                } else if (obj.getClass() == Double.class && licenseFee == 0) {
                    licenseFee = (double) obj;
                } else if (obj.getClass() == Integer.class && amountOfClients == 0) {
                    amountOfClients = (int) obj;
                } else if (obj.getClass() == ActionEnum.class) {
                    elem = (ActionEnum) obj;
                }
            }
            List<Tarif> list = director.getBuilder().getTarifList().getListTarif();
            try {
                switch (elem) {
                    case FIND_CLIENTS: {
                        for (Tarif t : list) {
                            if (t.getCountOfClients() == amountOfClients) {
                                log.info("tarif is founded");
                            }
                        }
                        break;
                    }
                    case FIND_FEE: {
                        for (Tarif t : list) {
                            if (t.getLicenseFee() == licenseFee) {
                                log.info("tarif is founded");
                            }
                        }
                        break;
                    }
                    case FIND_NAME: {
                        for (Tarif t : list) {
                            if (t.getName().equalsIgnoreCase(name)) {
                                log.info("tarif is founded");
                            }
                        }
                        break;
                    }
                    default: {
                        System.err.println("no one case!");
                        break;
                    }
                }
            } catch (NullPointerException e) {
                System.err.println("You must point case for finding");
                log.error("null enum element exception");
            }
        }
    }
}
