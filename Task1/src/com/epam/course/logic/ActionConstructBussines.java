/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.logic;

import com.epam.course.builder.VelcomTarifDirector;
import com.epam.course.entity.BussinesClass;
import org.apache.log4j.Logger;

/**
 *
 * @author notepad
 */
public class ActionConstructBussines implements TarifAction {

    private  Logger log = Logger.getLogger(ActionConstructBussines.class);

    @Override
    public void doAction(Object... args) {
        VelcomTarifDirector director = new VelcomTarifDirector();
        String name = null;
        double licenseFee = 0;
        int countOfClients = 0;
        int freeSms = 0;
        for (Object obj : args) {
            if (obj.getClass() == String.class && name == null) {
                name = (String) obj;
            } else if (obj.getClass() == Integer.class && countOfClients == 0) {
                countOfClients = (int) obj;
            } else if (obj.getClass() == Integer.class && countOfClients != 0 && freeSms == 0) {
                freeSms = (int) obj;
            } else if (obj.getClass() == Double.class) {
                licenseFee = (double) obj;
            }
        }
        director.constructTarifList(new BussinesClass(name, licenseFee, countOfClients, freeSms));
        log.info("Constructing of bussines is complete!");
    }

}
