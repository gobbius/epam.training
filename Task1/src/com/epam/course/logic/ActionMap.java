/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.logic;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author notepad
 */
public class ActionMap {

    private  Map<ActionEnum, TarifAction> tarifMap = new HashMap<ActionEnum, TarifAction>();
    private static ActionMap instance;

    private ActionMap() {
        tarifMap.put(ActionEnum.CONSTRUCT_SMART, new ActionConstructSmart());
        tarifMap.put(ActionEnum.SORT, new ActionSortByLicenseFee());
        tarifMap.put(ActionEnum.COUNT, new ActionCountClients());
        tarifMap.put(ActionEnum.CONSTRUCT_BUSSINES, new ActionConstructBussines());
        tarifMap.put(ActionEnum.CONSTRUCT_SHOES, new ActionConstructShoes());
        tarifMap.put(ActionEnum.FIND, new ActionFindTarif());
    }

    public static ActionMap getInstance() {
        if (instance == null) {
            instance = new ActionMap();
        }
        return instance;
    }

    public TarifAction getProperty(ActionEnum key) {
        return tarifMap.get(key);
    }

}
