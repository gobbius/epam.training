/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.view;

import com.epam.course.controller.Controller;
import com.epam.course.logic.ActionEnum;

/**
 *
 * @author notepad
 */
public class Main {

    public static void main(String[] args) {
        Controller controller = new Controller();
        controller.build(ActionEnum.CONSTRUCT_SMART, "smart", 30000.0, 156, 300);
        controller.build(ActionEnum.CONSTRUCT_BUSSINES, "bussines", 50000.0, 200, 500);
        controller.build(ActionEnum.CONSTRUCT_SHOES, "shoes", 35000.0, 143, 320);
        controller.build(ActionEnum.FIND, ActionEnum.FIND_CLIENTS, 156);
        controller.build(ActionEnum.COUNT);
        controller.build(ActionEnum.SORT);
    }
}
