/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.entity;

/**
 *
 * @author notepad
 */
public abstract class Tarif {

    private String name;
    private double licenseFee;
    private int countOfClients;

    public Tarif(String name, double licenseFee, int countOfClients) {
        this.name = name;
        this.licenseFee = licenseFee;
        this.countOfClients = countOfClients;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLicenseFee(double licenseFee) {
        this.licenseFee = licenseFee;
    }

    public void setCountOfClients(int countOfClients) {
        this.countOfClients = countOfClients;
    }

    public int getCountOfClients() {
        return countOfClients;
    }

    public double getLicenseFee() {
        return licenseFee;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "\n\r" + "Name of Tarif: " + this.name + "\n\rAmount of Clients: " + this.countOfClients + "\n\rLicense fee: " + this.licenseFee;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.countOfClients;
        result = (int) (prime * result + this.licenseFee);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Tarif other = (Tarif) obj;
        if (this.licenseFee != other.licenseFee) {
            return false;
        }
        if (this.countOfClients != other.countOfClients) {
            return false;
        }
        if (this.name != other.name) {
            return false;
        } else {
            return true;
        }
    }
}
