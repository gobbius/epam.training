/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.entity;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author notepad
 */
public class TarifList {

    private List<Tarif> listTarif;
    private static final Logger log = Logger.getLogger(TarifList.class);

    public TarifList() {
        this.listTarif = new ArrayList<Tarif>();
    }

    public void setListTarif(List<Tarif> listTarif) {
        this.listTarif = listTarif;
    }

    public List<Tarif> getListTarif() {
        return listTarif;
    }

    public void add(Tarif entity) {
        listTarif.add(entity);
    }

    public boolean isEmpty() {
        return this.listTarif.isEmpty();
    }
}
