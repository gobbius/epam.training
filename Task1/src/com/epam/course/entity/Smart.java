/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.entity;

/**
 *
 * @author notepad
 */
public class Smart extends Tarif {

    private int minutesInAllWebs;

    public Smart(String name, double licenseFee, int countOfClients, int minutesInAllWebs) {
        super(name, licenseFee, countOfClients);
        this.minutesInAllWebs = minutesInAllWebs;
    }

    public void setMinutesInAllWebs(int minutesInAllWebs) {
        this.minutesInAllWebs = minutesInAllWebs;
    }

    @Override
    public String toString() {
        return super.toString() + "\n\r" + "Amount of minutes in all webs: " + this.minutesInAllWebs;
    }

    @Override
    public boolean equals(Object obj) {
        Smart other = (Smart) obj;
        if (!super.equals(other)) {
            return false;
        }
        if (this.minutesInAllWebs != other.minutesInAllWebs) {
            return false;
        }
        return true;
    }

}
