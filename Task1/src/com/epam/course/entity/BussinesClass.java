/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.entity;

/**
 *
 * @author notepad
 */
public class BussinesClass extends Tarif {

    private int freeSms;

    public BussinesClass(String name, double licenseFee, int countOfClients, int freeSms) {
        super(name, licenseFee, countOfClients);
        this.freeSms = freeSms;
    }

    public void setFreeSms(int freeSms) {
        this.freeSms = freeSms;
    }

    @Override
    public String toString() {
        return super.toString() + "\n\r" + "Amount of free sms: " + this.freeSms;
    }

    @Override
    public boolean equals(Object obj) {
        BussinesClass other = (BussinesClass) obj;
        if (!super.equals(other)) {
            return false;
        }
        if (this.freeSms != other.freeSms) {
            return false;
        }
        return true;
    }
}
