/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.entity;

/**
 *
 * @author notepad
 */
public class Shoes extends Tarif {

    private int countMms;

    public Shoes(String name, double licenseFee, int countOfClients, int countMms) {
        super(name, licenseFee, countOfClients);
        this.countMms = countMms;
    }

    public void setCountMms(int countMms) {
        this.countMms = countMms;
    }

    @Override
    public String toString() {
        return super.toString() + "\n\r" + "Amount of mms: " + this.countMms;
    }

    @Override
    public boolean equals(Object obj) {
        Shoes other = (Shoes) obj;
        if (!super.equals(other));
        if (this.countMms != other.countMms) {
            return false;
        }
        return true;
    }

}
