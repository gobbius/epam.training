/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.builder;

import com.epam.course.entity.Tarif;
import com.epam.course.entity.TarifList;

/**
 *
 * @author notepad
 */
public interface TarifBuilder {

    public TarifList getTarifList();

    public void addToTarifList(Tarif tarif);
}
