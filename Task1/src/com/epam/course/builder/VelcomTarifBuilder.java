/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.builder;

import com.epam.course.entity.Tarif;
import com.epam.course.entity.TarifList;
import org.apache.log4j.Logger;

/**
 *
 * @author notepad
 */
public class VelcomTarifBuilder implements TarifBuilder {

    private  Logger log = Logger.getLogger(VelcomTarifBuilder.class);
    private static TarifList listTarif = new TarifList();

    @Override
    public TarifList getTarifList() {
        return listTarif;
    }

    @Override
    public void addToTarifList(Tarif tarif) {
        listTarif.add(tarif);
    }
}
