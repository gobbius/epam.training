/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.builder;

import com.epam.course.entity.Tarif;

/**
 *
 * @author notepad
 */
public class VelcomTarifDirector {

    private TarifBuilder builder;

    public VelcomTarifDirector() {
        this.builder = new VelcomTarifBuilder();
    }

    public TarifBuilder getBuilder() {
        return builder;
    }

    public void constructTarifList(Tarif tarif) {
        this.builder.addToTarifList(tarif);
    }

}
