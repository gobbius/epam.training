/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.dao;

/**
 *
 * @author notepad
 */
public enum TagName {

    KNIFE, TYPE, ORIGIN, HANDY, LENGTH, WIDTH, MATERIAL, WOOD, PLASTIC, BLOOD_STOKE, VALUE, KNIVES, ANOTHER_TAG;

    public static TagName getTagName(String tagName) {
        switch (tagName) {
            case "knife": {
                return KNIFE;
            }
            case "type": {
                return TYPE;
            }
            case "origin": {
                return ORIGIN;
            }
            case "handy": {
                return HANDY;
            }
            case "length": {
                return LENGTH;
            }
            case "width": {
                return WIDTH;
            }
            case "knives": {
                return KNIVES;
            }
            case "material": {
                return MATERIAL;
            }
            case "wood": {
                return WOOD;
            }
            case "plastic": {
                return PLASTIC;
            }
            case "blood-stoke": {
                return BLOOD_STOKE;
            }
            case "value": {
                return VALUE;
            }
            default: {
                return ANOTHER_TAG;
            }
        }
    }
}
