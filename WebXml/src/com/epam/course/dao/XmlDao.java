/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.dao;

import com.epam.course.entity.Knife;
import java.util.List;

/**
 *
 * @author notepad
 */
public interface XmlDao {

    List<Knife> parse(String input) throws XmlDaoException;
}
