/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.dao.impl;

import com.epam.course.dao.XmlDao;
import com.epam.course.dao.XmlDaoException;
import com.epam.course.entity.Knife;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author notepad
 */
public class DomXmlDao implements XmlDao {

    private final static Logger log = Logger.getLogger(DomXmlDao.class);
    private final static String ID = "id";
    private final static String TYPE = "type";
    private final static String HANDY = "handy";
    private final static String ORIGIN = "origin";
    private final static String LENGTH = "length";
    private final static String WIDTH = "width";
    private final static String MATERIAL = "material";
    private final static String HANDLE = "handle";
    private final static String BLOOD_STOKE = "blood-stoke";
    private final static String VALUE = "value";
    private final static String KNIFE = "knife";

    @Override
    public List<Knife> parse(String input) throws XmlDaoException {
        List<Knife> knives = new ArrayList<Knife>();
        Knife knife = null;
        InputSource inputSource = new InputSource(input);
        DOMParser parser = new DOMParser();
        try {
            parser.parse(inputSource);
            Document document = parser.getDocument();
            Element root = document.getDocumentElement();
            NodeList knivesNodes = root.getElementsByTagName(KNIFE);
            for (int i = 0; i < knivesNodes.getLength(); i++) {
                knife = new Knife();
                Element knifeElement = (Element) knivesNodes.item(i);
                knife.setId(Integer.parseInt(knifeElement.getAttribute(ID)));
                knife.setType(getSingleChild(knifeElement, TYPE).getTextContent().trim());
                knife.setHandy(getSingleChild(knifeElement, HANDY).getTextContent().trim());
                knife.setOrigin(getSingleChild(knifeElement, ORIGIN).getTextContent().trim());
                knife.setLength(Integer.parseInt(getSingleChild(knifeElement, LENGTH).getTextContent().trim()));
                knife.setWidth(Integer.parseInt(getSingleChild(knifeElement, WIDTH).getTextContent().trim()));
                knife.setMaterial(getSingleChild(knifeElement, MATERIAL).getTextContent().trim());
                knife.setHandle(getSingleChild(knifeElement, HANDLE).getTextContent().trim());
                knife.setBloodStoke(Boolean.parseBoolean(getSingleChild(knifeElement, BLOOD_STOKE).getTextContent().trim()));
                knife.setValue(Boolean.parseBoolean(getSingleChild(knifeElement, VALUE).getTextContent().trim()));
                knives.add(knife);
                knife = null;
            }
        } catch (SAXException ex) {
            throw new XmlDaoException(ex);
        } catch (IOException ex) {
            throw new XmlDaoException(ex);
        }
        return knives;
    }

    private static Element getSingleChild(Element element, String childName) {
        NodeList nlist = element.getElementsByTagName(childName);
        Element child = (Element) nlist.item(0);
        return child;
    }
}
