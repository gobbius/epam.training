/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.dao.impl;

import com.epam.course.dao.SaxHandler;
import com.epam.course.dao.XmlDao;
import com.epam.course.dao.XmlDaoException;
import com.epam.course.entity.Knife;
import java.io.IOException;
import java.util.List;
import org.apache.log4j.Logger;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 *
 * @author notepad
 */
public class SaxXmlDao implements XmlDao {

    private final static Logger log = Logger.getLogger(SaxXmlDao.class);

    @Override
    public List<Knife> parse(String input) throws XmlDaoException {
        List<Knife> knives = null;
        SaxHandler handler = null;
        try {
            XMLReader reader = XMLReaderFactory.createXMLReader();
            InputSource inputSource = new InputSource(input);
            handler = new SaxHandler();
            reader.setContentHandler(handler);
            reader.parse(inputSource);
        } catch (IOException ex) {
            throw new XmlDaoException(ex);
        } catch (SAXException ex) {
            throw new XmlDaoException(ex);
        }
        knives = handler.getKnives();
        return knives;
    }

}
