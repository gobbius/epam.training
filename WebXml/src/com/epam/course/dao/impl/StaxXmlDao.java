/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.dao.impl;

import com.epam.course.dao.TagName;
import com.epam.course.dao.XmlDao;
import com.epam.course.dao.XmlDaoException;
import com.epam.course.entity.Knife;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import org.apache.log4j.Logger;

/**
 *
 * @author notepad
 */
public class StaxXmlDao implements XmlDao {

    private final static Logger log = Logger.getLogger(StaxXmlDao.class);

    @Override
    public List<Knife> parse(String input) throws XmlDaoException {
        List<Knife> knives = new ArrayList<Knife>();
        Knife knife = null;
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        try {
            InputStream inputStream = new FileInputStream(input);
            XMLStreamReader reader = inputFactory.createXMLStreamReader(inputStream);
            TagName tagName = null;
            while (reader.hasNext()) {
                int type = reader.next();
                switch (type) {
                    case XMLStreamConstants.START_ELEMENT: {
                        tagName = TagName.getTagName(reader.getLocalName());
                        switch (tagName) {
                            case KNIFE: {
                                knife = new Knife();
                                knife.setId(Integer.parseInt(reader.getAttributeValue(null, "id")));
                                break;
                            }
                            case ANOTHER_TAG: {
                                break;
                            }
                        }
                        break;
                    }
                    case XMLStreamConstants.CHARACTERS: {
                        String text = reader.getText().trim();
                        if (text.isEmpty()) {
                            break;
                        }
                        switch (tagName) {
                            case TYPE: {
                                knife.setType(text);
                                break;
                            }
                            case HANDY: {
                                knife.setHandy(text);
                                break;
                            }
                            case ORIGIN: {
                                knife.setOrigin(text);
                                break;
                            }
                            case LENGTH: {
                                knife.setLength(Integer.parseInt(text));
                                break;
                            }
                            case WIDTH: {
                                knife.setWidth(Integer.parseInt(text));
                                break;
                            }
                            case MATERIAL: {
                                knife.setMaterial(text);
                                break;
                            }
                            case WOOD: {
                                knife.setHandle(text);
                                break;
                            }
                            case PLASTIC: {
                                knife.setHandle(text);
                                break;
                            }
                            case BLOOD_STOKE: {
                                knife.setBloodStoke(Boolean.getBoolean(text));
                                break;
                            }
                            case VALUE: {
                                knife.setValue(Boolean.getBoolean(text));
                                break;
                            }
                            case ANOTHER_TAG: {
                                break;
                            }
                        }
                        break;
                    }
                    case XMLStreamConstants.END_ELEMENT: {
                        tagName = TagName.getTagName(reader.getLocalName());
                        switch (tagName) {
                            case KNIFE: {
                                knives.add(knife);
                                knife = null;
                                break;
                            }
                            case KNIVES: {
                                return knives;
                            }
                            case ANOTHER_TAG: {
                                break;
                            }
                        }
                    }
                }
            }
        } catch (FileNotFoundException ex) {
            throw new XmlDaoException(ex);
        } catch (XMLStreamException ex) {
            throw new XmlDaoException(ex);
        }
        return knives;
    }

}
