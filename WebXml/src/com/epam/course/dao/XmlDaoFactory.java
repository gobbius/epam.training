/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.dao;

import com.epam.course.dao.impl.DomXmlDao;
import com.epam.course.dao.impl.SaxXmlDao;
import com.epam.course.dao.impl.StaxXmlDao;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author notepad
 */
public class XmlDaoFactory {

    private final static XmlDaoFactory instance = new XmlDaoFactory();
    private final static String STAX = "stax";
    private final static String SAX = "sax";
    private final static String DOM = "dom";
    private Map<String, XmlDao> parsers = new HashMap<String, XmlDao>();

    private XmlDaoFactory() {
        parsers.put(STAX, new StaxXmlDao());
        parsers.put(SAX, new SaxXmlDao());
        parsers.put(DOM, new DomXmlDao());
    }

    public static XmlDaoFactory getInstance() {
        return instance;
    }

    public XmlDao getParser(String parserName) {
        return parsers.get(parserName);
    }
}
