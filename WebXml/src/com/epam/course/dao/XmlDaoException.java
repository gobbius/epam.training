/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.dao;

import com.epam.course.exception.ProjectException;

/**
 *
 * @author notepad
 */
public class XmlDaoException extends ProjectException{

    public XmlDaoException(Throwable thrwbl) {
        super(thrwbl);
    }

    public XmlDaoException() {
    }
    
}
