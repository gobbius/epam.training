/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.dao;

import com.epam.course.dao.TagName;
import com.epam.course.entity.Knife;
import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author notepad
 */
public class SaxHandler extends DefaultHandler {

    private Knife knife;
    private List<Knife> knives = new ArrayList<Knife>();
    private StringBuilder text;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes atrbts) throws SAXException {
        text = new StringBuilder();
        TagName tagName = TagName.getTagName(localName);
        switch (tagName) {
            case KNIFE: {
                knife = new Knife();
                knife.setId(Integer.parseInt(atrbts.getValue(0)));
                break;
            }
            case ANOTHER_TAG: {
                break;
            }
        }
    }

    @Override
    public void characters(char[] buffer, int start, int end) throws SAXException {
        text.append(buffer, start, end);

    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        TagName tagName = TagName.getTagName(localName);
        switch (tagName) {
            case KNIFE: {
                knives.add(knife);
                knife = null;
                break;
            }
            case TYPE: {
                break;
            }
            case HANDY: {
                knife.setHandy(text.toString());
                break;
            }
            case ORIGIN: {
                knife.setOrigin(text.toString());
                break;
            }
            case LENGTH: {
                knife.setLength(Integer.parseInt(text.toString()));
                break;
            }
            case WIDTH: {
                knife.setWidth(Integer.parseInt(text.toString()));
                break;
            }
            case MATERIAL: {
                knife.setMaterial(text.toString());
                break;
            }
            case WOOD: {
                knife.setHandle(text.toString());
                break;
            }
            case PLASTIC: {
                knife.setHandle(text.toString());
                break;
            }
            case BLOOD_STOKE: {
                knife.setBloodStoke(Boolean.parseBoolean(text.toString()));
                break;
            }
            case VALUE: {
                knife.setValue(Boolean.parseBoolean(text.toString()));
                break;
            }
            case ANOTHER_TAG: {
                break;
            }
        }
    }

    public List<Knife> getKnives() {
        return knives;
    }

}
