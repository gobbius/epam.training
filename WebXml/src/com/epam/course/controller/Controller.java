package com.epam.course.controller;

import com.epam.course.command.CommandException;
import com.epam.course.command.CommandFactory;
import com.epam.course.command.CommandName;
import com.epam.course.command.ICommand;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

public class Controller extends HttpServlet {

    private final static Logger log = Logger.getLogger(Controller.class);
    private static final long serialVersionUID = 1L;
    private final static String MESSAGE = "message";

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    private void process(HttpServletRequest request, HttpServletResponse response) {
        String page = null;
        ICommand command = CommandFactory.getInstance().getCommand(request.getParameter(CommandName.COMMAND));
        try {
            page = command.parseXml(request);
            RequestDispatcher dispatcher = request.getRequestDispatcher(page);
            dispatcher.forward(request, response);
        } catch (CommandException ex) {
            log.error(ex.getMessage());
            errorMessageDireclyFromResponse(request, response, ex.getMessage());
        } catch (Exception ex) {
            log.error(ex.getMessage());
            errorMessageDireclyFromResponse(request, response, ex.getMessage());
        }
    }

    private void errorMessageDireclyFromResponse(HttpServletRequest request, HttpServletResponse response, String message) {
        try {
            request.setAttribute(MESSAGE, message);
            RequestDispatcher dispatcher = request.getRequestDispatcher(JspPageName.ERROR);
            dispatcher.include(request, response);
        } catch (ServletException ex) {
            log.error(ex.getMessage());
        } catch (IOException ex) {
            log.error(ex.getMessage());
        }
    }
}
