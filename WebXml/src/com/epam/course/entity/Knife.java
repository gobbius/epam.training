/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.entity;

/**
 *
 * @author notepad
 */
public class Knife {

    private int id;
    private String type;
    private String handy;
    private String origin;
    private int length;
    private int width;
    private String material;
    private String handle;
    private boolean bloodStoke;
    private boolean value;

    public Knife() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setBloodStoke(boolean bloodStoke) {
        this.bloodStoke = bloodStoke;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    public void setHandy(String handy) {
        this.handy = handy;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getId() {
        return id;
    }

    public String getHandle() {
        return handle;
    }

    public String getHandy() {
        return handy;
    }

    public int getLength() {
        return length;
    }

    public String getMaterial() {
        return material;
    }

    public String getOrigin() {
        return origin;
    }

    public String getType() {
        return type;
    }

    public int getWidth() {
        return width;
    }

    public boolean isBloodStoke() {
        return bloodStoke;
    }

    public boolean isValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.id + this.type;
    }

}
