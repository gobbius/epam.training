/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.command.impl;

import com.epam.course.command.CommandException;
import com.epam.course.command.ICommand;
import com.epam.course.controller.JspPageName;
import com.epam.course.dao.XmlDao;
import com.epam.course.dao.XmlDaoException;
import com.epam.course.dao.XmlDaoFactory;
import com.epam.course.entity.Knife;
import java.util.List;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author notepad
 */
public class StaxParseCommand implements ICommand {

    private final static Logger log = Logger.getLogger(StaxParseCommand.class);
    private final static String PARSER_NAME = "stax";
    private final static String FILE_NAME = "filename";
    private final static String KNIVES = "knives";

    @Override
    public String parseXml(HttpServletRequest request) throws CommandException {
        List<Knife> knives = null;
        XmlDao parser = XmlDaoFactory.getInstance().getParser(PARSER_NAME);
        try {
            knives = parser.parse(request.getParameter(FILE_NAME));
            request.setAttribute(KNIVES, knives);
        } catch (XmlDaoException ex) {
            throw new CommandException(ex);
        }
        return JspPageName.USER_PAGE;
    }

}
