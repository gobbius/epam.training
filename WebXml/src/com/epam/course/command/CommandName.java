/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.command;

/**
 *
 * @author notepad
 */
public final class CommandName {

    public final static String COMMAND = "command";
    public final static String STAX_COMMAND = "stax_command";
    public final static String SAX_COMMAND = "sax_command";
    public final static String DOM_COMMAND = "dom_command";
}
