/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.command;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author notepad
 */
public interface ICommand {

    String parseXml(HttpServletRequest request) throws CommandException;
}
