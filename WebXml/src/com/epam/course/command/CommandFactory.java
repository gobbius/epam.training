/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.command;

import com.epam.course.command.impl.DomParseCommand;
import com.epam.course.command.impl.SaxParseCommand;
import com.epam.course.command.impl.StaxParseCommand;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author notepad
 */
public class CommandFactory {

    private final static CommandFactory instance = new CommandFactory();
    private Map<String, ICommand> commands = new HashMap<String, ICommand>();

    private CommandFactory() {
        commands.put(CommandName.STAX_COMMAND, new StaxParseCommand());
        commands.put(CommandName.SAX_COMMAND, new SaxParseCommand());
        commands.put(CommandName.DOM_COMMAND, new DomParseCommand());
    }

    public static CommandFactory getInstance() {
        return instance;
    }

    public ICommand getCommand(String commandName) {
        return commands.get(commandName);
    }
}
