<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Knives</title>
    </head>
    <body>
        <table border="1">
            <th>id</th>
            <th>type</th>
            <th>handy</th>
            <th>origin</th>
            <th>length</th>
            <th>width</th>
            <th>material</th>
            <th>handle</th>
            <th>bloodstoke</th>
            <th>value</th>
            <c:forEach var="knife" items="${knives}">
                <tr>
                    <td>${knife.id}</td>
                    <td>${knife.type}</td>
                    <td>${knife.handy}</td>
                    <td>${knife.origin}</td>
                    <td>${knife.length}</td>
                    <td>${knife.width}</td>
                    <td>${knife.material}</td>
                    <td>${knife.handle}</td>
                    <td>${knife.bloodStoke}</td>
                    <td>${knife.value}</td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>