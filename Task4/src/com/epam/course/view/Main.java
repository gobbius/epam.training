/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.course.view;

import java.io.File;
import java.io.IOException;
import javax.xml.validation.Validator;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author notepad
 */
public class Main {

    public static void main(String[] args) throws SAXException, IOException {
        SchemaFactory factory = SchemaFactory
                .newInstance("http://www.w3.org/2001/XMLSchema");
        File schemaLocation = new File("src/Knives.xsd");
        Schema schema = factory.newSchema(schemaLocation);

        Validator validator = schema.newValidator();

        Source source = new StreamSource("src/Knife.xml");
        try {
            validator.validate(source);
            System.out.println(" is valid.");
        } catch (SAXException ex) {
            System.out.println(" is not valid because ");
            System.out.println(ex.getMessage());
        }
    }
}
