<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

    <xsl:template match="/">
        <xsl:apply-templates select="knives"/>
    </xsl:template>

    <xsl:template match="knives">
        <ul>
            <xsl:apply-templates select="knife"/>
        </ul>
    </xsl:template>
    <xsl:template match="knife">
        <ul>
            <xsl:apply-templates select="visual" />
        </ul>
    </xsl:template>
    <xsl:template match="knife">
        <li>
            <xsl:value-of select="type" />
        </li>
        <li>
            <xsl:value-of select="handy" />
        </li>
        <li>
            <xsl:value-of select="origin" />
        </li>
        <li>
           <xsl:value-of select="visual/length" />
        </li>
        <li>
           <xsl:value-of select="visual/width" />
        </li>
        <li>
           <xsl:value-of select="visual/material" />
        </li>
        <li>
           <xsl:value-of select="visual/handle/wood" />
           <xsl:value-of select="visual/handle/plastic" />
        </li>
        <li>
           <xsl:value-of select="visual/blood-stoke" />
        </li>
        <li>
           <xsl:value-of select="visual/value" />
        </li>
        <br/>
        <br/>
    </xsl:template>
</xsl:stylesheet>
